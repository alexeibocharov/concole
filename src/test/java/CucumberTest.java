import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        plugin = {"pretty"},
        glue = {"ru.sbtqa.pprb.techcore.hyperic.stepdefs", "ru.sbtqa.tag.stepdefs.ru"},
        tags = {"@4797"},
        features = {"src/test/resources/features/"}
)
public class CucumberTest {
}
