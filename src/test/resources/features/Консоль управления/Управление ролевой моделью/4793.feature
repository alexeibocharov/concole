# language: ru
Функционал: Управление ролевой моделью

  @4793 @console @regress @controlRoleModel
  Сценарий: 4793 Роль 1 уровня - изъять объект доступа
    * пользователь авторизован под ролью "autotestuser_ga"
    * открывается страница "Dashboard"
    * (переходит во вкладку) "Administration"
    * открывается страница "Administration"
    * (переходит в плагин) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * (нажимает кнопку) "Пользователи"
    * открывается страница "Управление пользователями>>Пользователи"
    * в таблице "Пользователи" проваливается в ячейку "ФИО" в записи, содержащей
      | ФИО | autotestuser autotestuser |
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (нажимает кнопку) "Изменить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь>>Редактирование ролей"
    * выбирает все элементы списка в поле "Назначенные роли"
    * (нажимает кнопку) "Переместить влево"
    * выбирает элемент списка "Прикладной администратор" в поле "Доступные роли"
    * (нажимает кнопку) "Переместить вправо"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (переходит по хлебным крошкам на страницу) "Пользователи"
    * открывается страница "Управление пользователями>>Пользователи"
    * в таблице "Пользователи" проваливается в ячейку в поле Роль с параметрами
      | ФИО  | autotestuser autotestuser  |
      | Роль | ПА                           |
    * открывается страница "Управление пользователями>>Назначение групп модулей"
    * выбирает все элементы списка в поле "Назначенные группы модулей"
    * (нажимает кнопку) "Переместить влево"
    * выбирает элемент списка "AUTOtest_client" в поле "Доступные группы модулей"
    * выбирает элемент списка "AUTOtest_client2" в поле "Доступные группы модулей"
    * (нажимает кнопку) "Переместить вправо"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление пользователями>>Пользователи"
    * в таблице "Пользователи" проваливается в ячейку в поле Роль с параметрами
      | ФИО  | autotestuser autotestuser |
      | Роль | ПА                           |
    * открывается страница "Управление пользователями>>Назначение групп модулей"
    * выбирает элемент списка "AUTOtest_client" в поле "Назначенные группы модулей"
    * (нажимает кнопку) "Переместить влево"
    * (нажимает кнопку) "Отменить"
    * открывается страница "Управление пользователями>>Пользователи"
    * в таблице "Пользователи" проваливается в ячейку в поле Роль с параметрами
      | ФИО  | autotestuser autotestuser |
      | Роль | ПА                           |
    * открывается страница "Управление пользователями>>Назначение групп модулей"
    * в поле "Назначенные группы модулей" содержится элемент "AUTOtest_client"
    * выбирает элемент списка "AUTOtest_client" в поле "Назначенные группы модулей"
    * (нажимает кнопку) "Переместить влево"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление пользователями>>Пользователи"
    * в таблице "Пользователи" проваливается в ячейку в поле Роль с параметрами
      | ФИО  | autotestuser autotestuser |
      | Роль | ПА                           |
    * открывается страница "Управление пользователями>>Назначение групп модулей"
    * в поле "Назначенные группы модулей" не содержится элемент "AUTOtest_client"
    * пользователь (выходит из системы)
    * находится на странице "Страница авторизации"
