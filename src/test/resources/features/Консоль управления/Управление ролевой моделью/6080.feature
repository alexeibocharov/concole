# language: ru
Функционал: Управление ролевой моделью

  @6080 @console @regress @controlRoleModel
  Сценарий: 6080 Роль 1 уровня - заблокировать пользователя.
    * пользователь авторизован под ролью "autotestuser_ga"
    * открывается страница "Dashboard"
    * (переходит во вкладку) "Administration"
    * открывается страница "Administration"
    * (переходит в плагин) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * (нажимает кнопку) "Пользователи"
    * открывается страница "Управление пользователями>>Пользователи"
    * выбирает запись в таблице "Пользователи"
      | ФИО | autotestuser autotestuser |
    * (нажимает кнопку) "Заблокировать"
    * (отклоняет уведомление) "Выбранные пользователи будут заблокированы."
    * в таблице "Пользователи" присутствует запись
      | ФИО    | autotestuser autotestuser |
      | Статус | активен                      |
    * (нажимает кнопку) "Заблокировать"
    * (принимает уведомление) "Выбранные пользователи будут заблокированы."
    * в таблице "Пользователи" присутствует запись
      | ФИО    | autotestuser autotestuser |
      | Статус | заблокирован                 |
    # повторная блокировка уже заблокированного пользователя
    * (нажимает кнопку) "Заблокировать"
    * (принимает уведомление) "Выбранные пользователи будут заблокированы."
    * в таблице "Пользователи" присутствует запись
      | ФИО    | autotestuser autotestuser |
      | Статус | заблокирован                 |
    #-возвращаем как было
    * (нажимает кнопку) "Активизировать"
    * (принимает уведомление) "Выбранные пользователи будут активизированы."
    * в таблице "Пользователи" присутствует запись
      | ФИО    | autotestuser autotestuser |
      | Статус | активен                      |
    #-вернули как было
    * пользователь (выходит из системы)
    * находится на странице "Страница авторизации"
