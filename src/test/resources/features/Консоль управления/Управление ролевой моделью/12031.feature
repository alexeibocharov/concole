# language: ru

Функционал: Управление ролевой моделью

  @12031 @console @regress @controlRoleModel
  Сценарий: 12031 PRB-21690 - хлебные крошки на форме назначения прав пользователю
    * пользователь авторизован под ролью "ga"
    * открывается страница "Dashboard"
    * (переходит во вкладку) "Administration"
    * открывается страница "Administration"
    * (переходит в плагин) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * в таблице "Пользователи" проваливается в ячейку "ФИО" в записи, содержащей
      | ФИО | autotestuser autotestuser |
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (нажимает кнопку) "Изменить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь>>Редактирование ролей"
    * выбирает все элементы списка в поле "Назначенные роли"
    * (нажимает кнопку) "Переместить влево"
    * выбирает элемент списка "Прикладной администратор" в поле "Доступные роли"
    * (нажимает кнопку) "Переместить вправо"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (переходит по хлебным крошкам на страницу) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * в таблице "Пользователи" проваливается в ячейку "ФИО" в записи, содержащей
      | ФИО | autotestuser autotestuser |
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (нажимает кнопку) "Изменить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь>>Редактирование ролей"
    * (переходит по хлебным крошкам на страницу) "Пользователь"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (нажимает кнопку) "Изменить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь>>Редактирование ролей"
    * (переходит по хлебным крошкам на страницу) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * в таблице "Пользователи" проваливается в ячейку в поле Роль с параметрами
      | ФИО  | autotestuser autotestuser |
      | Роль | ПА                           |
    * открывается страница "Управление пользователями>>Назначение групп модулей"
    * (переходит по хлебным крошкам на страницу) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * пользователь (выходит из системы)
    * находится на странице "Страница авторизации"

