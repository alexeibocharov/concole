# language: ru

Функционал: Управление ролевой моделью

  @6151 @console @regress @controlRoleModel
  Сценарий: 6151 Ревизор - изменение роли пользователя

    * пользователь авторизован под ролью "ga"
    * открывается страница "Dashboard"

    # делаем пользователю права Ревизор

    * (переходит во вкладку) "Administration"
    * открывается страница "Administration"
    * (переходит в плагин) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * (нажимает кнопку) "Пользователи"
    * открывается страница "Управление пользователями>>Пользователи"
    * в таблице "Пользователи" устанавливает количество строк "500"
    * в таблице "Пользователи" проваливается в ячейку "ФИО" в записи, содержащей
      | ФИО | autotestuser autotestuser |
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (нажимает кнопку) "Изменить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь>>Редактирование ролей"
    * выбирает все элементы списка в поле "Назначенные роли"
    * (нажимает кнопку) "Переместить влево"
    * выбирает элемент списка "Ревизор" в поле "Доступные роли"
    * (нажимает кнопку) "Переместить вправо"
    * (нажимает кнопку) "Сохранить"

    # сам тест

    * пользователь (выходит из системы)
    * находится на странице "Страница авторизации"
    * пользователь авторизован под ролью "autotestuser"
    * открывается страница "Dashboard"
    * (переходит во вкладку) "Administration"
    * открывается страница "Administration"
    * (переходит в плагин) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * (нажимает кнопку) "Пользователи"
    * открывается страница "Управление пользователями>>Пользователи"
    * в таблице "Пользователи" устанавливает количество строк "500"
    * в таблице "Пользователи" проваливается в ячейку "ФИО" в записи, содержащей
      | ФИО | autotestuser_ga autotestuser_ga |
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (нажимает кнопку) "Изменить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь>>Редактирование ролей"
    * выбирает все элементы списка в поле "Назначенные роли"
    * (нажимает кнопку) "Переместить влево"
    * выбирает элемент списка "Администратор конфигураций" в поле "Доступные роли"
    * (нажимает кнопку) "Переместить вправо"
    * (нажимает кнопку) "Сохранить"
    * (появляется диалог с сообщением) "Ошибка авторизации: Недостаточно полномочий для выполнения операции"
    * (нажимает кнопку) "OK"
