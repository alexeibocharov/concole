# language: ru

Функционал: Консоль управления

  Предыстория:
    * CLI Console: выполняет сценарий "createModulesGroup"
      | Наименование группы | Autotest9062ClientGroup |
      | Тип группы          | 1                       |
      | Порт                | 9062                    |
      | Код группы          | Autotest9062ClientCode  |

  @9062 @regress @console
  Сценарий: 9062 Добавление-Исключение узлов клиентской группе модулей
    * пользователь авторизован
    * открывается страница "Dashboard"
    * (переходит по меню) "Resources->КБЛ"
    * открывается страница "Модуль управления контейнером ППРБ. Группы модулей"
    * (выбирает вкладку) "Настройка модулей"
    * открывается страница "Модуль управления контейнером ППРБ. Настройка модулей"
    * (нажимает кнопку) "Топология клиентских узлов"
    * открывается страница "Настройка модулей -> Топология клиентских узлов"
    * в таблице "Группы модулей, размещаемых на клиентских узлах" проваливается в ячейку "Наименование" в записи, содержащей
      | Наименование | Autotest9062ClientGroup |
    * открывается страница "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа"
    * запоминает содержимое таблицы "Узлы группы модулей"
    * (нажимает кнопку) "Добавить/исключить узлы"
    * открывается страница "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа -> Добавить узел"
    * количество узлов в поле "Узлы группы модулей" соответствует количеству в таблице "Узлы группы модулей"
    * выбирает первый элемент списка в поле "Узлы для выбора"
    * (нажимает кнопку) "Добавить узлы"
    * поле "Узлы группы модулей" содержит перемещенные элементы
    * выбирает все элементы списка в поле "Узлы группы модулей"
    * (нажимает кнопку) "Исключить узлы"
    * поле "Узлы группы модулей" пустое
    * (нажимает кнопку) "Отменить"
    * открывается страница "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа"
    * проверяет что таблица "Узлы группы модулей" не изменилась
    * (нажимает кнопку) "Добавить/исключить узлы"
    * открывается страница "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа -> Добавить узел"
    * выбирает первый элемент списка в поле "Узлы для выбора"
    * (нажимает кнопку) "Добавить узлы"
    * поле "Узлы группы модулей" содержит перемещенные элементы
    * запоминает узлы в поле "Узлы группы модулей"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа"
    * проверяет что таблица "Узлы группы модулей" содержит добавленные узлы
    * проверяет что "поля пустые" "Установлено приложение"
    * (нажимает кнопку) "Добавить/исключить узлы"
    * открывается страница "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа -> Добавить узел"
    * выбирает все элементы списка в поле "Узлы группы модулей"
    * (нажимает кнопку) "Исключить узлы"
    * поле "Узлы группы модулей" пустое
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа"

    * пользователь (выходит из системы)
    * находится на странице "Страница авторизации"
