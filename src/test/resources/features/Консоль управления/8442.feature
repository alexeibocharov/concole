# language: ru

Функционал: Консоль управления
  Предыстория:
    * CLI Console: выполняет сценарий
      | command | createModule |
    * CLI Console: выполняет сценарий
      | command | createGroupModule |
    * CLI Console: выполняет сценарий
      | command | addModuleToGroup |
    * пользователь авторизован под ролью "ga"
    * открывается страница "Dashboard"
    * (переходит во вкладку) "Administration"
    * открывается страница "Administration"
    * (переходит в плагин) "Управление пользователями"
    * открывается страница "Управление пользователями"
    * в таблице "Пользователи" проваливается в ячейку "ФИО" в записи, содержащей
      | ФИО | autotestuser autotestuser |
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * (нажимает кнопку) "Изменить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь>>Редактирование ролей"
    * выбирает все элементы списка в поле "Назначенные роли"
    * (нажимает кнопку) "Переместить влево"
    * выбирает элемент списка "Администратор Грида" в поле "Доступные роли"
    * (нажимает кнопку) "Переместить вправо"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление пользователями>>Пользователи>>Пользователь"
    * пользователь (выходит из системы)
    * находится на странице "Страница авторизации"

  @8442
  Сценарий: 8442 Администратор грида. Загрузка КБЛ и модулей на узлы, назначенные группе
    * CLI Console: выполняет сценарий
      | name | createModule |
      | code |              |
    * пользователь авторизован с учетной записью "wolf"


