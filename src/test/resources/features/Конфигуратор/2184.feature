# language: ru

Функционал: Конфигуратор

  @2184 @regress @configurator
  Сценарий: 2184 Загрузка валидного xml файла с новой версией
    * пользователь авторизован
    * открывается страница "Dashboard"
    * (переходит по меню) "Resources->Конфигуратор"
    * открывается страница "Конфигуратор ППРБ"
    * в таблице "Артефакты" устанавливает количество строк "100"
    * анализ xml: информация о модели конфигурации
      | Имя файла | 2184.xml |
    * (нажимает кнопку) "Импорт модели"
    * (появляется диалог Импорт модели с сообщением) "Необходимо выбрать файл (*.xml), содержащий модель конфигурации."
    * пользователь в блоке "Диалог" (загружает модель конфигурации)
    * появляется диалоговое окно браузера: "Импорт модели выполнен успешно!"
    * находится на странице "Конфигуратор ППРБ"
    * переходит в список версий артефакта
    * открывается страница "Список версий артефакта"
    * пользователь (выходит из системы)
    * находится на странице "Страница авторизации"