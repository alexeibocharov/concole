# language: ru

Функционал: Конфигуратор

  @14354 @shortregress @regress
  Сценарий: 14354 Добавление клиентской группы модулей

    * пользователь авторизован
    * открывается страница "Dashboard"
    * (переходит по меню) "Resources->КБЛ"
    * открывается страница "Модуль управления контейнером ППРБ. Runtime"
    * (выбирает вкладку) "Управление версиями"
    * открывается страница "Модуль управления контейнером ППРБ. Управление версиями"
    * (выбирает вкладку) "Настройка модулей"
    * открывается страница "Модуль управления контейнером ППРБ. Настройка модулей"
    * (нажимает кнопку) "Группы модулей"
    * открывается страница "Настройка модулей -> Группы модулей"
    * (нажимает кнопку) "Добавить"
    * открывается страница "Настройка модулей -> Группы модулей -> Добавить"
    * (заполняет поле) "Наименование группы" "testgroup14354"
    * (заполняет поле) "Код группы" "testgroup12062"
    * (заполняет поле) "Порт" "1235"
    * выбирает элемент списка "Клиентская" в поле "Тип"
    * (заполняет поле) "Каталог для установки группы на серверах" "testgroup14354"
    * (нажимает кнопку) "Сохранить"

    # удаление тестовых данных

    * открывается страница "Настройка модулей -> Группы модулей"
    * в таблице "Группы модулей" присутствует запись
      | Наименование | testgroup14354 |
    * выбирает запись в таблице "Группы модулей"
      | Наименование | testgroup14354 |
    * (нажимает кнопку) "Удалить"
    * (нажимает кнопку) "OK"
