#language: ru

Функционал: плагин Управление версиями

  @19426 @console @regress @shortregress
  Сценарий: 19426 Установка устаревшей версии КБЛ признака по умочанию
    * пользователь авторизован
    * открывается страница "Dashboard"
    * (переходит по меню) "Resources->Управление КБЛ"
    * открывается страница "Модуль управления контейнером ППРБ. Runtime"
    * (выбирает вкладку) "Управление версиями"
    * открывается страница "Модуль управления контейнером ППРБ. Управление версиями"
    * (нажимает кнопку) "Версии контейнера"
    * открывается страница "Управление версиями >> Версии контейнера"
    * в таблице "Счетчик Контейнер бизнес-логики" устанавливает количество строк "500"

    * (нажимает кнопку) "Добавить"
    * открывается страница "Управление версиями >> Версии контейнера >> Добавить"
    * (заполняет поле) "Версия" "Autotest19426"
    * выбирает элемент списка "scp://hyperic:hyperic@10.44.37.146/u01/pprb/env-src/" в поле "Репозиторий"
    * (заполняет поле) "Файл" "Autotest19426.zip"
    * выбирает элемент списка "анлим" в поле "Версия лицензии GridGain"
    * (нажимает кнопку) "Сохранить"

    * открывается страница "Управление версиями >> Версии контейнера"
    * выбирает запись в таблице "Таблица Контейнер бизнес-логики"
      | Версия | Autotest19426 |

    * (нажимает кнопку) "Признак Устаревшая"
    * (появляется диалог с сообщением) "Признак"
    * (появляется диалог с сообщением) "Устаревшая"
    * (появляется диалог с сообщением) "установлен"
    * пользователь в блоке "Диалог" (принимает его)

    * в таблице "Таблица Контейнер бизнес-логики" присутствует запись
      | Версия     | Autotest19426 |
      | Устаревшая | Да            |

    * проверка, что запись в таблице "Таблица Контейнер бизнес-логики" без признака по умолчанию
      | Версия | Autotest19426 |

    * (нажимает кнопку) "Выбрать по умолчанию"
    * (появляется диалог с сообщением) "Устаревшая версия не может быть установлена в качестве версии"
    * (появляется диалог с сообщением) "по умолчанию"
    * пользователь в блоке "Диалог" (принимает его)
    * проверка, что запись в таблице "Таблица Контейнер бизнес-логики" без признака по умолчанию
      | Версия | Autotest19426 |

    * (нажимает кнопку) "Удалить"
    * (появляется диалог с сообщением) "Вы действительно хотите удалить версию(и) контейнера?"
    * пользователь в блоке "Диалог" (принимает его)
    * не находит запись в таблице "Таблица Контейнер бизнес-логики"
      | Версия | Autotest19426 |