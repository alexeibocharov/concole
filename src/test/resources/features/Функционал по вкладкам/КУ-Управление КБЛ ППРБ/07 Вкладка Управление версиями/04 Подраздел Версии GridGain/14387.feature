#language: ru

Функционал: плагин Управление версиями

  @14387 @console @regress
  Сценарий: 14387 Изменение версии GridGain
    * пользователь авторизован
    * открывается страница "Dashboard"
    * (переходит по меню) "Resources->Управление КБЛ"
    * открывается страница "Модуль управления контейнером ППРБ. Runtime"
    * (выбирает вкладку) "Управление версиями"
    * открывается страница "Модуль управления контейнером ППРБ. Управление версиями"
    * (нажимает кнопку) "Версии GridGain"
    * открывается страница "Управление версиями >> Версии GridGain"

    * в таблице "Счетчик Версии GridGain" устанавливает количество строк "500"

    * (нажимает кнопку) "Добавить"
    * открывается страница "Управление версиями >> Версии GridGain >> Добавить"
    * (заполняет поле) "Наименование версии GridGain" "Autotest14387"
    * (заполняет поле) "Код версии GridGain" "Autotest14387"
    * (заполняет поле) "zip-файл GridGain" "Autotest14387"
    * выбирает элемент списка "анлим" в поле "Версия лицензии GridGain"
    * (нажимает кнопку) "Сохранить"

    * открывается страница "Управление версиями >> Версии GridGain"
    * находит запись в таблице "Таблица Версии GridGain"
      | Версия GridGain | Autotest14387 |
    * выбирает запись в таблице "Таблица Версии GridGain"
      | Версия GridGain | Autotest14387 |

    * (нажимает кнопку) "Изменить"
    * открывается страница "Управление версиями >> Версии GridGain >> Добавить"
    * (заполняет поле) "Наименование версии GridGain" "Autotest14387change"
    * (заполняет поле) "Код версии GridGain" "Autotest14387change"
    * (заполняет поле) "zip-файл GridGain" "Autotest14387change"
    * выбирает элемент списка "анлим" в поле "Версия лицензии GridGain"
    * (нажимает кнопку) "Сохранить"

    * открывается страница "Управление версиями >> Версии GridGain"
    * находит запись в таблице "Таблица Версии GridGain"
      | Версия GridGain | Autotest14387change |
    * выбирает запись в таблице "Таблица Версии GridGain"
      | Версия GridGain | Autotest14387change |

    * (нажимает кнопку) "Удалить"
    * (появляется диалог с сообщением) "Вы действительно хотите удалить версию(и) GridGain?"
    * пользователь в блоке "Диалог" (принимает диалог)
    * (появляется диалог с сообщением) "Версия(и) GridGain успешно удалена"
    * пользователь в блоке "Диалог" (принимает его)
    * не находит запись в таблице "Таблица Версии GridGain"
      | Версия GridGain | Autotest14387change |