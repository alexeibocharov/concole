#language: ru

Функционал: плагин Управление версиями

  @14457 @console @regress @shortregress
  Сценарий: 14457 Установка версии GridGain признака по умолчанию
    * пользователь авторизован
    * открывается страница "Dashboard"
    * (переходит по меню) "Resources->Управление КБЛ"
    * открывается страница "Модуль управления контейнером ППРБ. Runtime"
    * (выбирает вкладку) "Управление версиями"
    * открывается страница "Модуль управления контейнером ППРБ. Управление версиями"
    * (нажимает кнопку) "Версии GridGain"
    * открывается страница "Управление версиями >> Версии GridGain"
    * в таблице "Счетчик Версии GridGain" устанавливает количество строк "500"

    * (нажимает кнопку) "Добавить"
    * открывается страница "Управление версиями >> Версии GridGain >> Добавить"
    * (заполняет поле) "Наименование версии GridGain" "Autotest14457"
    * (заполняет поле) "Код версии GridGain" "Autotest14457"
    * (заполняет поле) "zip-файл GridGain" "Autotest14457"
    * выбирает элемент списка "анлим" в поле "Версия лицензии GridGain"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление версиями >> Версии GridGain"

    * выбирает запись в таблице "Таблица Версии GridGain"
      | Версия GridGain | Autotest14457 |
    * проверка, что запись в таблице "Таблица Версии GridGain" без признака по умолчанию
      | Версия GridGain | Autotest14457 |
    * (нажимает кнопку) "По умолчанию"
    * (появляется диалог с сообщением) "Версия GridGain установлена по умолчанию"
    * пользователь в блоке "Диалог" (принимает его)

    * открывается страница "Управление версиями >> Версии GridGain"
    * проверка, что запись в таблице "Таблица Версии GridGain" с признаком по умолчанию
    | Версия GridGain | Autotest14457 |

    * (нажимает кнопку) "Удалить"
    * (появляется диалог с сообщением) "Вы действительно хотите удалить версию(и) GridGain?"
    * пользователь в блоке "Диалог" (принимает диалог)
    * (появляется диалог с сообщением) "Версия(и) GridGain успешно удалена"
    * пользователь в блоке "Диалог" (принимает его)
    * не находит запись в таблице "Таблица Версии GridGain"
      | Версия GridGain | Autotest14457 |