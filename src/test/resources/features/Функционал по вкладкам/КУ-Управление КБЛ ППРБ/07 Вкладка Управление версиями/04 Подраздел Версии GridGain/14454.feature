  #language: ru

  Функционал: плагин Управление версиями

    @14454 @console @regress
    Сценарий: 14454 Удаление нескольких версий GridGain одновременно
      * пользователь авторизован
      * открывается страница "Dashboard"
      * (переходит по меню) "Resources->Управление КБЛ"
      * открывается страница "Модуль управления контейнером ППРБ. Runtime"
      * (выбирает вкладку) "Управление версиями"
      * открывается страница "Модуль управления контейнером ППРБ. Управление версиями"
      * (нажимает кнопку) "Версии GridGain"
      * открывается страница "Управление версиями >> Версии GridGain"
      * в таблице "Счетчик Версии GridGain" устанавливает количество строк "500"

      * (нажимает кнопку) "Добавить"
      * открывается страница "Управление версиями >> Версии GridGain >> Добавить"
      * (заполняет поле) "Наименование версии GridGain" "Autotest14454_1"
      * (заполняет поле) "Код версии GridGain" "Autotest14454_1"
      * (заполняет поле) "zip-файл GridGain" "Autotest14454_1"
      * выбирает элемент списка "анлим" в поле "Версия лицензии GridGain"
      * (нажимает кнопку) "Сохранить"
      * открывается страница "Управление версиями >> Версии GridGain"
      * (нажимает кнопку) "Добавить"
      * открывается страница "Управление версиями >> Версии GridGain >> Добавить"
      * (заполняет поле) "Наименование версии GridGain" "Autotest14454_2"
      * (заполняет поле) "Код версии GridGain" "Autotest14454_2"
      * (заполняет поле) "zip-файл GridGain" "Autotest14454_2"
      * выбирает элемент списка "анлим" в поле "Версия лицензии GridGain"
      * (нажимает кнопку) "Сохранить"

      * открывается страница "Управление версиями >> Версии GridGain"
      * выбирает запись в таблице "Таблица Версии GridGain"
        | Версия GridGain | Autotest14454_1 |
        | Версия GridGain | Autotest14454_2 |

      * (нажимает кнопку) "Удалить"
      * (появляется диалог с сообщением) "Вы действительно хотите удалить версию(и) GridGain?"
      * пользователь в блоке "Диалог" (принимает диалог)
      * (появляется диалог с сообщением) "Версия(и) GridGain успешно удалена"
      * пользователь в блоке "Диалог" (принимает его)
      * не находит запись в таблице "Таблица Версии GridGain"
        | Версия GridGain | Autotest14454_1 |
                                          | Версия GridGain | Autotest14454_2 |