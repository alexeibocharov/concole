# language: ru

Функционал: Конфигуратор

  @14422 @regress
  Сценарий: 14422 Удаление нескольких версий модуля одновременно
      # предусловия

    * пользователь авторизован
    * пользователь сохраняет уникальное значение переменной "modulewithflag"
    * открывается страница "Dashboard"
    * (переходит по меню) "Resources->КБЛ"
    * открывается страница "Модуль управления контейнером ППРБ. Runtime"
    * (выбирает вкладку) "Управление версиями"
    * открывается страница "Модуль управления контейнером ППРБ. Управление версиями"
    * (нажимает кнопку) "Версии модулей"
    * открывается страница "Управление версиями >> Список модулей"

    # создаём модуль

    * (нажимает кнопку) "Добавить"
    * открывается страница "Управление версиями >> Список модулей >> Добавить"
    * пользователь заполняет поле "Наименование модуля" значением "modulewithflag"
    * пользователь заполняет поле "Код модуля" значением "modulewithflag"
    * (нажимает кнопку) "Требуется версия модели"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление версиями >> Список модулей"

    # Добавляем версию в модуль

    * в таблице "Модули" проваливается в ячейку "Модуль" в записи, содержащей
      | Модуль | modulewithflag |
    * открывается страница "Управление версиями >> Список модулей >> Версии модуля"
    * (нажимает кнопку) "Добавить"
    * открывается страница "Управление версиями >> Список модулей >> Версии модуля >> Добавление модуля"
    * (заполняет поле) "Наименование версии" "name14422"
    * (заполняет поле) "Файл" "filefortest14422.doc"
    * (заполняет поле) "Контрольная сумма" "555"
    * (выбирает) "Алгоритм" "MD5"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление версиями >> Список модулей >> Версии модуля"
    * пользователь находит запись в таблице "Список версий"
      | Версия | name14422 |

    # Добавляем вторую версию в модуль

    * (нажимает кнопку) "Добавить"
    * открывается страница "Управление версиями >> Список модулей >> Версии модуля >> Добавление модуля"
    * (заполняет поле) "Наименование версии" "secondname14422"
    * (заполняет поле) "Файл" "secondfilefortest14422.doc"
    * (заполняет поле) "Контрольная сумма" "555"
    * (выбирает) "Алгоритм" "MD5"
    * (нажимает кнопку) "Сохранить"
    * открывается страница "Управление версиями >> Список модулей >> Версии модуля"
    * пользователь находит запись в таблице "Список версий"
      | Версия | secondname14422 |

    # Удаление 2 версий одновременно

    * пользователь выбирает запись в таблице "Список версий"
      | Версия | name14422 |
    * пользователь выбирает запись в таблице "Список версий"
      | Версия | secondname14422 |
    * (нажимает кнопку) "Удалить"
    * (нажимает кнопку) "OK"
    * (нажимает кнопку) "Назад в Список модулей"

     # Удаляем тестовые данные

    * открывается страница "Управление версиями >> Список модулей"
    * выбирает запись в таблице "Модули"
      | Модуль | modulewithflag |
    * (нажимает кнопку) "Удалить"
    * (нажимает кнопку) "OK"