package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;

/**
 * Created by sbt-kolesnichenko-af on 23.11.2017.
 */
@PageEntry(title = "Настройка модулей -> Топология клиентских узлов")
public class TopologyClientNodes extends AnyPage {

    @ElementTitle(value = "Группы модулей, размещаемых на клиентских узлах")
    @FindBy(xpath = "//*[@id='ClientNodesTopologyTableContainer']")
    HypericTable clientNodesTopologyTableTable;

    public TopologyClientNodes() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ClientNodesTopologyTableContainer")));
    }
}
