package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;


import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Версии контейнера")
public class ContainerVersionPage extends PPRBContainersControlModule {

    final String containerVersionPageXpath = "//*[@id='Top-KBLTableContainer']";

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = containerVersionPageXpath + "//button[descendant::text() = 'Добавить']")
    Button add;

    @ElementTitle(value = "Изменить")
    @FindBy(xpath = containerVersionPageXpath + "//button[descendant::text() = 'Изменить']")
    Button edit;

    @ElementTitle(value = "Удалить")
    @FindBy(xpath = containerVersionPageXpath +"//button[descendant::text() = 'Удалить']")
    Button delete;

    @ElementTitle(value = "Выбрать по умолчанию")
    @FindBy(xpath = containerVersionPageXpath + "//button[descendant::text() = 'Выбрать \"по умолчанию\"']")
    Button getByDefault;

    @ElementTitle(value = "Признак Устаревшая")
    @FindBy(xpath = containerVersionPageXpath + "//button[descendant::text() = 'Признак \"Устаревшая\"']")
    Button deprecate;

    @ElementTitle(value = "Фильтр версии контейнера")
    @FindBy(xpath = containerVersionPageXpath + "//input[@id='filterKBLVersions']")
    TextInput filter;

    @ElementTitle(value = "Счетчик Контейнер бизнес-логики")
    @FindBy(xpath = "//*[@id='KBLTableContainer']")
    HypericTable KBLTableContainerCounter;

    @ElementTitle(value = "Таблица Контейнер бизнес-логики")
    @FindBy(xpath = "//*[@id='KBLTableContainer']//table")
    HypericTable KBLTableContainer;

    public ContainerVersionPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(containerVersionPageXpath)));
    }
}
