package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Ячейки")
public class CellTab extends PPRBContainersControlModule {

    @ElementTitle(value = "Установить на ячейку")
    @FindBy(id = "setupCellSpan")
    Button selectCell;

    @ElementTitle(value = "Удалить с ячейки")
    @FindBy(id = "uninstallCellSpan")
    Button deleteFromCell;

    @ElementTitle(value = "Запустить")
    @FindBy(id = "startCellsSpan")
    Button execute;

    @ElementTitle(value = "Остановить")
    @FindBy(id = "stopCellSpan")
    Button stop;

    @ElementTitle("Ячейки")
    @FindBy(id = "tabCellsTableContainer")
    HypericTable cellsGroupsTab;

    public CellTab() {
        assertTrue(tabList.getActiveTab().getText().equals("Ячейки"));
    }
}
