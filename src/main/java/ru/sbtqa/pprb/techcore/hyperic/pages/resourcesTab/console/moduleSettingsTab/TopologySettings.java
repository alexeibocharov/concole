package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Настройка модулей -> Группы модулей")
public class TopologySettings extends ModuleSettingsTab {

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = "//button[@title = 'Создание группы модулей']")
    Button addGroup;

    @ElementTitle(value = "Изменить")
    @FindBy(xpath = "//td[@id = 'ShowEditTopologyPane']/button")
    Button btnEditGroup;

    @ElementTitle(value = "Удалить")
    @FindBy(xpath = "//*[@id='deleteGroupConfirmed']/span")
    Button btnDeleteGroup;

    @ElementTitle(value = "Группы модулей")
    @FindBy(id = "TopologySettingsTableContainer")
    HypericTable topologySettingsTableTab;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'OK' or text() = 'ОК']")
    Button Ok;

    @ElementTitle(value = "Очистить все фильтры")
    @FindBy(id ="resetFilters")
    Button reset;

    @ElementTitle(value = "Назад в Настройку модулей")
    @FindBy(xpath ="//*[@id='Top-TopologySettingsTableContainer']/div[5]/a/span")
    Button back;

    public TopologySettings() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@title = 'Создание группы модулей']")));
    }
}
