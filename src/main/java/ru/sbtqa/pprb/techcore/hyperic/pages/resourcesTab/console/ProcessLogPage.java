package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Link;

/**
 * Created by sbt-kolesnichenko-af on 13.10.2017.
 */
@PageEntry(title = "Модуль управления контейнером ППРБ. Журнал процесса")
public class ProcessLogPage extends AnyPage {

    @ElementTitle(value = "Скрыть")
    @FindBy(xpath = "//a/span[text()='Скрыть']")
    public Link btnHideProcessLog;

    public ProcessLogPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a/span[text()='Скрыть']")));
    }
}
