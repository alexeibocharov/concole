package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.allurehelper.ParamsHelper;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.datajack.TestDataObject;
import ru.sbtqa.tag.datajack.exceptions.DataException;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper.VMOPTIONS_PATH;
import static ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper.VMOPTIONS_OBJECT;

/**
 * Created by sbt-kolesnichenko-af on 21.10.2017.
 */
@PageEntry(title = "Редактирование версии")
public class EditVersionPage extends AnyPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditVersionPage.class);
    @ElementTitle(value = "Опции VM")
    @FindBy(xpath = "//div[@id='vmForm']/textarea[@id='vmoptions']")
    public TextInput inputVMOptions;

    @ElementTitle(value = "Отмена")
    @FindBy(xpath = "//a[@id='cancelVMLink']/span[@id='vmFormBtnCancel']")
    public Button btnCancel;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//a[@id='saveVMLink']/span[@id='vmFormBtnSave']")
    public Button btnSave;

    public EditVersionPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cancelVMLink")));
        LOGGER.info("Page is initialized: " + this.getClass().getSimpleName());
    }

    public static String getVMOption(String optionName) {
        String data = "";
        try {
            TestDataObject tdo = Stash.getValue(VMOPTIONS_OBJECT);
            data = tdo.get("VMOptions." + optionName).getValue();
        } catch (DataException e) {
            LOGGER.info("Please, check the file with VM Options by path \"" + VMOPTIONS_PATH + "\"");
            e.printStackTrace();
        }
        return data;
    }

    public void fillVMOptions(String option) {
        inputVMOptions.clear();
        String data = EditVersionPage.getVMOption(option);
        ParamsHelper.addParam("Длина Опций VM: ", String.valueOf(data.length()));
        fillField(inputVMOptions, data);
    }

}
