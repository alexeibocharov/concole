package ru.sbtqa.pprb.techcore.hyperic.utils;

import autoitx4java.AutoItX;
import com.jacob.com.LibraryLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.tag.qautils.errors.AutotestError;

import java.io.File;
import java.nio.file.FileSystems;

/**
 * Created by sbt-kolesnichenko-af on 30.10.2017.
 */
public class AutoItHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(AutoItHelper.class);
    private String wndHandle;

    public AutoItHelper(String wndHandle) {
        this.wndHandle = wndHandle;
    }

    public AutoItHelper() {
    }

    public static File getLibAutoIT() {
        File file;
        if (System.getProperty("sun.arch.data.model").indexOf("64") != -1) {
            file = new File("lib", "jacob-1.18-x64.dll");
        } else {
            file = new File("lib", "jacob-1.18-x86.dll");
        }
        return file;
    }

    public static void activateAutoIt() {
        File file = getLibAutoIT();
        System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());
    }

    public String getTextFromFile() {
        AutoItHelper.activateAutoIt();
        AutoItX autoIt = new AutoItX();
        autoIt.winWait(wndHandle, "", 10);
        return autoIt.controlGetText(wndHandle, "", "Edit1");
    }

    public void closeFile(String wndHandle) {
        AutoItHelper.activateAutoIt();
        AutoItX autoIt = new AutoItX();
        autoIt.winWait(wndHandle, "", 10);
        autoIt.winClose(wndHandle);
    }

    public static void chooseFile(String title, String titleRus, String pathToFile) throws InterruptedException {
        AutoItHelper.activateAutoIt();
        AutoItX autoItX = new AutoItX();
        if (!autoItX.winWait(title, "", 10)) {
            if (!autoItX.winWait(titleRus, "", 1)) {
                throw new AutotestError("Не найдено окно - " + title + " или " + titleRus);
            } else {
                title = titleRus;
            }
        }
        autoItX.controlFocus(title, "", "Edit1");
        Thread.sleep(1000);
        String correctedPathToFile = checkAbsolutePath(pathToFile);
        autoItX.controlSend(title, "", "[CLASS:Edit; INSTANCE:1]", correctedPathToFile);
        autoItX.controlClick(title, "", "[CLASS:Button; INSTANCE:1]");
        Thread.sleep(1000);
    }

    private static String checkAbsolutePath(String path) {
        String correctedPath = path;
        if (path.contains("|")) {
            correctedPath = correctedPath.replace("|", FileSystems.getDefault().getSeparator());
        }
        if (path.contains(";")) {
            correctedPath = correctedPath.replace(";", ":");
        }
        return correctedPath;
    }
}
