package ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.roleModelControl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.internal.MouseAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.AdministrationPage;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

/**
 * Created by sbt-kolesnichenko-af on 24.11.2017.
 */
@PageEntry(title = "Управление пользователями>>Пользователи")
public class RoleModelControlPage extends AdministrationPage {

    @ElementTitle(value = "Пользователи")
    @FindBy(id = "ExistingUsersTableContainer")
    protected HypericTable tblUsers;

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = "//div[@id='Top-ExistingUsersTableContainer']//a[child::span[text()='Добавить']]")
    public Button btnAdd;

    @ElementTitle(value = "Заблокировать")
    @FindBy(xpath = "//div[@id='Top-ExistingUsersTableContainer']//a[child::span[text()='Заблокировать']]")
    public Button btnDisableUser;

    @ElementTitle(value = "Активизировать")
    @FindBy(xpath = "//div[@id='Top-ExistingUsersTableContainer']//a[child::span[text()='Активизировать']]")
    public Button btnenableUser;

    @ElementTitle(value="Справка")
    @FindBy(xpath="//div[@id='Top-ExistingUsersTableContainer']//a[@title='Вывод справки для данной формы']")
    public Button help;

    @ActionTitle(value = "переходит по хлебным крошкам на страницу")
    public void goByBreadTo(String title) {
        String xPath = "//div[@class='bread']//a[contains(., '" + title + "')]";
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath)));
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public RoleModelControlPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@title='Добавить нового пользователя в ролевую модель']")));
    }

}
