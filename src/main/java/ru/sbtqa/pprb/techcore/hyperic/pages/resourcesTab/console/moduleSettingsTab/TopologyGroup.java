package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.annotations.ValidationRule;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextBlock;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Настройка модулей -> Группы модулей -> Модули группы")
public class TopologyGroup extends ModuleSettingsTab {

    @ElementTitle(value = "имя группы равно")
    @FindBy(xpath = ".//*[@id='TopologyGroupsTableContainer-groupName']")
    TextBlock groupName;

    @ElementTitle(value = "Добавить модуль в группу")
    @FindBy(xpath = "//*[@id='addModulesToGroupOnTopologyConfirmed']")
    Button addGroup;

    @ElementTitle(value = "Исключить модуль из группы")
    @FindBy(xpath = "//button[@title = 'Удаление выбранных модулей из группы']")
    Button deleteModule;

    @ElementTitle(value = "Группы модулей - группа")
    @FindBy(xpath = "//*[@id='TopologyGroupsTableContainer']")
    HypericTable topologySettingsTable;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'ОК']")
    Button Ok;
    //div[2]/div/div[2]/button[1]

    @ElementTitle(value = "Назад в настройки групп")
    @FindBy(xpath ="//*[@id='Top-TopologyGroupsTableContainer']/div[4]/a/span")
    Button BackToGroupModules;


    public TopologyGroup() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='addModulesToGroupOnTopologyConfirmed']")));
    }

    @ValidationRule(title = "имя группы равно")
    public void checkGroupName(String expected) {
        assertTrue(groupName.getText().contains(expected));
    }

}
