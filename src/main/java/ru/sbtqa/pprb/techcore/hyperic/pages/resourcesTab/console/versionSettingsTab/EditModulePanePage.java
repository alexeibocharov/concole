package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Список модулей >> Изменить")
public class EditModulePanePage extends AnyPage {

    final String editModulePanePageXpath = "//div[@id='editModulePane']";

    @ElementTitle(value = "Наименование модуля")
    @FindBy(xpath = editModulePanePageXpath + "//input[@id='module_name_edit']")
    TextInput module_name_edit;

    @ElementTitle(value = "Код модуля")
    @FindBy(xpath = editModulePanePageXpath + "//input[@id='module_code_edit']")
    TextInput module_code_edit;

    @ElementTitle(value = "Требуется версия модели")
    @FindBy(xpath = editModulePanePageXpath + "//input[@id='module_isModelRequired']")
    CheckBox module_isModelRequired;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = editModulePanePageXpath + "//button[descendant::text() = 'Сохранить']")
    Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = editModulePanePageXpath + "//button[descendant::text() = 'Отменить']")
    Button cancel;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'OK']")
    Button Ok;

    public EditModulePanePage() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(editModulePanePageXpath)));
    }
}
