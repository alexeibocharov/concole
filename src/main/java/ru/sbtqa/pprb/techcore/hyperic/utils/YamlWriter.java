package ru.sbtqa.pprb.techcore.hyperic.utils;

import org.yaml.snakeyaml.Yaml;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by sbt-kolesnichenko-af on 20.12.2017.
 */
public class YamlWriter {

    public static void write(Object data, String path) throws IOException {
        Yaml yaml = new Yaml();
        FileWriter writer = new FileWriter(path);
        yaml.dump(data, writer);
    }

}
