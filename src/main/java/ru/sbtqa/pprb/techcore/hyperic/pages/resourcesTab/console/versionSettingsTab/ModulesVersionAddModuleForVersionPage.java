package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Список модулей >> Версии модуля >> Добавление модуля")
public class ModulesVersionAddModuleForVersionPage extends AnyPage {

    final private String  ModulesVersionAddModuleForVersionPageXpath = "//*[@id='editVersionPane']";

    @ElementTitle(value = "Наименование версии")
    @FindBy(id = "version_name_edit")
    TextInput nameversion;

    @ElementTitle(value = "Файл")
    @FindBy(id = "version_file_edit")
    Select file;

    @ElementTitle(value = "Контрольная сумма")
    @FindBy(id = "check_sum_module")
    TextInput summ;

    @ElementTitle(value = "Файл")
    @FindBy(id = "kbl_file_edit")
    TextInput kbl_file_edit;

    @ElementTitle(value = "Контрольная сумма")
    @FindBy(id = "check_sum_kbl")
    TextInput check_sum_kbl;

    @ElementTitle(value = "Алгоритм")
    @FindBy(id = "check_sum_alg_module")
    Select check_sum_alg_edit;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = ModulesVersionAddModuleForVersionPageXpath + "//button[descendant::text() = 'Сохранить']")
    Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = ModulesVersionAddModuleForVersionPageXpath + "//button[descendant::text() = 'Отменить']")
    Button cancel;

    public ModulesVersionAddModuleForVersionPage() {
        wait.until(ExpectedConditions.elementToBeClickable(save));
    }
}
