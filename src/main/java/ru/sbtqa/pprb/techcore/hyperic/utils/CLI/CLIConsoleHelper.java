package ru.sbtqa.pprb.techcore.hyperic.utils.CLI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper;
import ru.sbtqa.pprb.techcore.hyperic.utils.ManagerTemplate;
import ru.sbtqa.tag.datajack.Stash;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

/**
 * Created by sbt-kolesnichenko-af on 29.12.2017.
 */
public class CLIConsoleHelper extends CLIHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(CLIConsoleHelper.class);
    public static String utilite;
    public static String configFile;
    public static String urlHyperic;
    public static String loginHyperic;
    public static String passwordHyperic;
    public static String pathToScenario;
    private String scenarioName;

    static {
        utilite = KeysHelper.UTILITES_FOLDER_PATH + KeysHelper.CONSOLE_UTILITE_VERSION + ".jar";
        configFile = KeysHelper.CONFIG_FOLDER_PATH + "configurator_" + standName.toLowerCase() + ".properties";
        urlHyperic = Stash.getValue("urlHyperic");
        loginHyperic = Stash.getValue("loginHyperic");
        passwordHyperic = Stash.getValue("passwordHyperic");
        pathToScenario = KeysHelper.CONSOLE_SCENARIOS_TEMPLATE_PATH;
    }

    public CLIConsoleHelper(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    public void createScenario(Map<String, String> scenarioData) {
        ManagerTemplate managerTemplate = new ManagerTemplate(pathToScenario, scenarioName);
        managerTemplate.createFileFromTemplate(scenarioData);
        LOGGER.info("File \"" + scenarioName + ".yml\" is created");
    }

    public void removeScenario() {
        Path file = FileSystems.getDefault().getPath(pathToScenario, scenarioName + ".yml");
        try {
            Files.deleteIfExists(file);
            LOGGER.info("File \"" + scenarioName + ".yml\" is removed");
        } catch (IOException e) {
            LOGGER.info("File \"" + scenarioName + ".yml\" is not found");
            e.printStackTrace();
        }
    }

    public void runScenario() throws IOException {
        String fullPathToScenario = pathToScenario + scenarioName + ".yml";
        String argLine = String.format("java -jar %1$s -h %2$s -f %3$s -l %4$s -p %5$s",
                utilite, urlHyperic, fullPathToScenario, loginHyperic, passwordHyperic);
        LOGGER.info("ArgLine - " + argLine);
        runProcess(argLine, scenarioName);
    }
}
