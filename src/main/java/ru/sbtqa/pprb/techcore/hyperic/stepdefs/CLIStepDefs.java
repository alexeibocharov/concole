package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.DataTable;
import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Тогда;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.utils.CLI.CLIConfiguratorHelper;
import ru.sbtqa.pprb.techcore.hyperic.utils.CLI.CLIConsoleHelper;
import ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper;
import ru.sbtqa.tag.datajack.Stash;

import java.io.IOException;
import java.util.Map;

public class CLIStepDefs {

    private static final Logger LOGGER = LoggerFactory.getLogger(CLIStepDefs.class);

    @Тогда("^CLI: удаляет версию$")
    public void cliRemoveArtifactVersion(DataTable dataTable) throws IOException, InterruptedException {
        Map<String, String> params = dataTable.asMap(String.class, String.class);
        CLIConfiguratorHelper cliConfiguratorHelper = new CLIConfiguratorHelper();
        cliConfiguratorHelper.removeArtifactVersion(
                Stash.getValue("artifactId"),
                params.get("Version"),
                params.get("Module"),
                params.get("Node"));
    }

    @Тогда("^CLI: загружает модель конфигурации \"(.*?)\"$")
    public void cliLoadModelConfiguration(String fileName) throws IOException, InterruptedException {
        CLIConfiguratorHelper cliConfiguratorHelper = new CLIConfiguratorHelper();
        String configModel = KeysHelper.MODEL_CONFIGURATIONS_PATH + fileName;
        cliConfiguratorHelper.loadModelConfiguration(configModel);
    }

    @Тогда("^CLI: создает модель конфигурации с перекрытиями настроек$")
    public void createModelConfigurationWithOverlap(DataTable dataTable) throws IOException, InterruptedException {
        Map<String, String> params = dataTable.asMap(String.class, String.class);
        CLIConfiguratorHelper cliConfiguratorHelper = new CLIConfiguratorHelper();
        cliConfiguratorHelper.createModelConfigurationWithOverlap(
                Stash.getValue("artifactId"),
                params.get("Version"),
                params.get("Module"),
                params.get("Node"));
    }

    @Дано("^CLI Console: выполняет сценарий \"(.*?)\"$")
    public void createModule(String scenarioName, DataTable dataTable) throws IOException, InterruptedException {
        Map<String, String> scenarioData = dataTable.asMap(String.class, String.class);
        CLIConsoleHelper cliConsoleHelper = new CLIConsoleHelper(scenarioName);
        cliConsoleHelper.createScenario(scenarioData);
        cliConsoleHelper.runScenario();
        cliConsoleHelper.removeScenario();
    }
}
