package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.RuntimeTabs;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Runtime. Версии GG на серверах. Неиспользуемые версии")
public class RuntimeTabVersionNotUsedVersion extends PPRBContainersControlModule {

    @ElementTitle(value = "Переустановить на отмеченных серверах")
    @FindBy(id = "reInstallGGOnServersPane")
    public Button reinstallOnSelectedServe;

    @ElementTitle(value = "Удалить с отмеченных серверов")
    @FindBy(id = "deleteGGOnServers")
    public Button compleatInstall;


    @ElementTitle(value = "Фильтр очистить все")
    @FindBy(xpath = "//*[@id='unusedGGVersionsButtons']/div[3]/div/div[2]")
    public Button filterClearAll;

    @ElementTitle(value = "Неиспользуемые версии")
    @FindBy(id = "UnusedGridGainVersionsContainer")
    HypericTable notUsedVersonsTab;

    public RuntimeTabVersionNotUsedVersion() {
        assertTrue(tabList.getActiveTab().getText().equals("Runtime"));
        wait.until(ExpectedConditions.elementToBeClickable(filterClearAll));
    }
}
