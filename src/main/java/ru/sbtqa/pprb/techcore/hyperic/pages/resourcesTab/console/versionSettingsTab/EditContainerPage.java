package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Версии контейнера >> Изменить")
public class EditContainerPage extends AnyPage {

    final String editContainerPaneXpath = "//*[@id='editKBLPane']";

    @ElementTitle(value = "Версия")
    @FindBy(xpath = "//input[@id='kbl_version_edit']")
    TextInput version;

    @ElementTitle(value = "Репозиторий")
    @FindBy(id = "kbl_repo_edit")
    Select kbl_repo_edit;

    @ElementTitle(value = "Путь к файлу в репозитории")
    @FindBy(xpath = "//input[@id='kbl_file_path_edit']")
    TextInput kbl_file_path_edit;

    @ElementTitle(value = "Файл")
    @FindBy(xpath = "//input[@id='kbl_file_edit']")
    TextInput kbl_file_edit;

    @ElementTitle(value = "Контрольная сумма")
    @FindBy(xpath = "//input[@id='check_sum_kbl']")
    TextInput check_sum_kbl;

    @ElementTitle(value = "Алгоритм")
    @FindBy(id = "check_sum_alg_edit")
    Select check_sum_alg_edit;

    @ElementTitle(value = "Версия лицензии GridGain")
    @FindBy(id = "kbl_license_edit")
    Select kbl_license_edit;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = editContainerPaneXpath + "//button[descendant::text() = 'Сохранить']")
    Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = editContainerPaneXpath + "//button[descendant::text() = 'Отменить']")
    Button cancel;

    public EditContainerPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(editContainerPaneXpath)));
    }
}
