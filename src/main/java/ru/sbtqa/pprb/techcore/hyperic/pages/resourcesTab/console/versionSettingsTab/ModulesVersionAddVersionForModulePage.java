package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Список модулей >> Версии модуля")
public class ModulesVersionAddVersionForModulePage extends PPRBContainersControlModule {

    final String modulesSettingsPageXpath = "//*[@id='Top-ModulesVersionsTableContainer']";

    @ElementTitle(value = "Строка поиска")
    @FindBy(id = "ModulesVersionsTableFilter")
    TextInput search;

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = modulesSettingsPageXpath + "//button[descendant::text() = 'Добавить']")
    Button add;

    @ElementTitle(value = "Изменить")
    @FindBy(xpath = modulesSettingsPageXpath + "//button[descendant::text() = 'Изменить']")
    Button edit;

    @ElementTitle(value = "Удалить")
    @FindBy(xpath = modulesSettingsPageXpath + "//button[descendant::text() = 'Удалить']")
    Button delete;

    @ElementTitle(value = "Выбрать по умолчанию")
    @FindBy(xpath = "//*[@id='setVersionAsDefaultConfirmed']/span")
    Button defaultchose;

    @ElementTitle(value = "Признак Устаревшая")
    @FindBy(xpath = "//*[@id='changeDeprecatedPropVersionConfirmed']/span")
    Button old;

    @ElementTitle(value = "Фильтр модулей")
    @FindBy(xpath = modulesSettingsPageXpath + "//input[@id='ModulesSettingsTableFilter']")
    TextInput filter;

    @ElementTitle(value = "Табличный фильтр устаревшая")
    @FindBy(xpath = "//*[@id='ModulesVersionsTableContainer']/div/table/thead/tr/th[7]/div/div[2]")
    Button oldVersion;

    @ElementTitle(value = "Да")
    @FindBy(id = "filter-val-1")
    CheckBox yes;

    @ElementTitle(value = "Нет")
    @FindBy(id = "filter-val-0")
    CheckBox no;


    @ElementTitle(value = "Табличный фильтр версий")
    @FindBy(xpath = "//*[@id='ModulesVersionsTableContainer']/div/table/thead/tr/th[2]/div/div[2]")
    Button filterVersion;

    @ElementTitle(value = "Список версий")
    @FindBy(id = "ModulesVersionsTableContainer")
    HypericTable modelVersionsTableContainer;

    @ElementTitle(value = "Значение фильтра версии")
    @FindBy(xpath = "//input[@class='filterVal']")
    TextInput filterVersionVal;

    @ElementTitle(value = "Применить")
    @FindBy(xpath =".//button[text() = 'Применить']")
    Button aplly;

    @ElementTitle(value = "Сбросить")
    @FindBy(xpath =".//button[text() = 'Сбросить']")
    Button cancel;

    @ElementTitle(value = "Закрыть")
    @FindBy(xpath =".//button[text() = 'Закрыть']")
    Button close;

    @ElementTitle(value = "Назад в Список модулей")
    @FindBy(xpath = modulesSettingsPageXpath + "/div[5]/a/span")
    Button back;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'OK' or text() = 'ОК']")
    Button ok;

    public ModulesVersionAddVersionForModulePage() {
        wait.until(ExpectedConditions.elementToBeClickable(back));
    }
}
