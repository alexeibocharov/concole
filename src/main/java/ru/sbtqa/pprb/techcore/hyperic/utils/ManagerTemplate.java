package ru.sbtqa.pprb.techcore.hyperic.utils;


import ru.sbtqa.tag.allurehelper.ParamsHelper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

/**
 * Created by sbt-kolesnichenko-af on 20.12.2017.
 */
public class ManagerTemplate {
    private String templateName;
    private String pathToTemplate;

    public ManagerTemplate(String pathToTemplate, String templateName) {
        this.pathToTemplate = pathToTemplate;
        this.templateName = templateName;
    }

    public void createFileFromTemplate(Map<String, String> mapOfParams) {
        String scenario = replaceAll(mapOfParams);
        Charset charset = Charset.forName("UTF-8");
        Path file = FileSystems.getDefault().getPath(pathToTemplate, templateName + ".yml");
        try (BufferedWriter writer = Files.newBufferedWriter(file, charset)) {
            writer.write(scenario, 0, scenario.length());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String replaceAll(Map<String, String> mapOfParams) {
        String file = readTemplate();
        for (Map.Entry<String, String> pair: mapOfParams.entrySet()) {
            String param = pair.getKey();
            String value = pair.getValue();
            if (file.contains(param)) {
                file = file.replace("%" + param + "%", value);
                ParamsHelper.addParam("Replaced <" + param + ">", "value - <" + value + ">");
            }
        }
        return file;
    }

    private String readTemplate() {
        Path file = FileSystems.getDefault().getPath(pathToTemplate, templateName);
        Charset charset = Charset.forName("UTF-8");
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = Files.newBufferedReader(file, charset)) {
            String line;
            while (null != (line = br.readLine())) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
