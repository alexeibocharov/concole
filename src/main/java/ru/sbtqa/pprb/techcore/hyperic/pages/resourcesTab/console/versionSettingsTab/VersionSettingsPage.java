package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Модуль управления контейнером ППРБ. Управление версиями")
public class VersionSettingsPage extends PPRBContainersControlModule {

    final String versionSettingsTabPageXpath = "//div[@id='TabVersionSettings']";

    @ElementTitle(value = "Версии контейнера")
    @FindBy(xpath = "//button[descendant::text() = 'Версии контейнера']")
    Button containerVersions;

    @ElementTitle(value = "Версии модулей")
    @FindBy(xpath = "//button[descendant::text() = 'Версии модулей']")
    Button modulesVersions;

    @ElementTitle(value = "Версии моделей данных")
    @FindBy(xpath = "//button[descendant::text() = 'Версии моделей данных']")
    Button versionsModulesDate;

    @ElementTitle(value = "Версии GridGain")
    @FindBy(xpath = "//button[descendant::text() = 'Версии GridGain']")
    Button gridgainVersions;

    @ElementTitle(value = "Справка")
    @FindBy(xpath = versionSettingsTabPageXpath + "//button[descendant::text() = 'Справка']")
    Button help;

    public VersionSettingsPage() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(versionSettingsTabPageXpath)));
    }
}
