package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка модулей")
public class ModuleSettingsTab extends PPRBContainersControlModule {

//================================== Меню ===================================

    @ElementTitle(value = "Общие настройки")
    @FindBy(xpath = "//button[descendant::text() = 'Общие настройки']")
    Button generalSettings;

    @ElementTitle(value = "Группы модулей")
    @FindBy(xpath = "//button[descendant::text() = 'Группы модулей']")
    Button moduleGroups;

    @ElementTitle(value = "Топология клиентских групп")
    @FindBy(xpath = "//button[descendant::text() = 'Топология клиентских групп']")
    Button btnClientNodesTopology;

    @ElementTitle(value = "Топология серверных групп")
    @FindBy(xpath = "//button[descendant::text() = 'Топология серверных групп']")
    Button btnClientGroupTopology;

    @ElementTitle(value = "Справка")
    @FindBy(xpath = "//*[@id='TabModuleSettings_left_menu']/button[4]/span")
    Button btnHelp;

//===========================================================================

    public ModuleSettingsTab() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='TabModuleSettings']")));
    }
}


