package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.RuntimeTabs;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Runtime. Версии GG на серверах. Установки версий")
public class RuntimeTabVersionGGInstallVersions extends PPRBContainersControlModule {

    @ElementTitle(value = "Новая установка")
    @FindBy(id = "newGridVersionReplicationButton")
    public Button newInstall;

    @ElementTitle(value = "Завершить установку")
    @FindBy(id = "completeGridVersionReplicationButton")
    public Button compleatInstall;

    @ElementTitle(value = "Назад в Версии GG на серверах")
    @FindBy(xpath = "//*[@id='serverGridGainVersionReplications']/a/span")
    public Button back;

    @ElementTitle(value = "Установка версий")
    @FindBy(id = "ServerGridGainVersionReplications-Container")
    HypericTable installVersionsTab;

    public RuntimeTabVersionGGInstallVersions() {
        assertTrue(tabList.getActiveTab().getText().equals("Runtime"));
        wait.until(ExpectedConditions.elementToBeClickable(newInstall));
    }
}
