package ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.roleModelControl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.internal.MouseAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.AdministrationPage;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

/*
 * Created by SBT-Azarkov-EV on 21.04.2018.
 */

@PageEntry(title = "Управление пользователями")
public class RoleModelMainPage extends AdministrationPage {
    public RoleModelMainPage(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='migContainer']//td[contains(text(), 'Управление пользователями')]")));
    }

    @ElementTitle("Пользователи")
    @FindBy(xpath="//div[@id='Top-MainUsersTableContainer']//a[@title='Пользователи']")
    public Button Users;

}
