package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.annotations.ValidationRule;
import ru.sbtqa.tag.pagefactory.exceptions.PageInitializationException;
import ru.yandex.qatools.htmlelements.element.Button;

import java.util.List;
import java.util.Map;

/**
 * Created by sbt-kolesnichenko-af on 23.11.2017.
 */
@PageEntry(title = "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа")
public class TopologyClientNodesGroup extends AnyPage {

    @ElementTitle(value = "Добавить/исключить узлы")
    @FindBy(xpath = "*//button[child::span[contains(., 'Добавить/исключить узлы')]]")
    protected Button btnAddDeleteNodes;

    @ElementTitle(value = "Узлы группы модулей")
    @FindBy(id = "ClientNodesTopologyGroupTableContainer")
    public HypericTable tblNodesOfGroupsModules;

    public TopologyClientNodesGroup() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Top-ClientNodesTopologyGroupTableContainer")));
        wait.until(ExpectedConditions.visibilityOf(btnAddDeleteNodes));
    }

    @ValidationRule(title = "поля пустые")
    public void fieldsAreEmpty(String field) throws PageInitializationException {
        List<Map<String, WebElement>> listRaws =  tblNodesOfGroupsModules.getRowsMappedToHeadings();
        for (int i = 0; i < listRaws.size(); i++) {
            String errMessage = "Field <" + field + "> in " + i + " raw is not empty!";
            Assert.assertTrue(errMessage, listRaws.get(i).get(field).getText().isEmpty());
        }
    }
}
