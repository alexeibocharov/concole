package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Группы модулей")
public class GroupsTab extends PPRBContainersControlModule {

    @ElementTitle(value = "Установить")
    @FindBy(xpath = "//*[@id='leftGroupButtons']/button[1]/span")
    Button install;

    @ElementTitle(value = "Обновить модули")
    @FindBy(xpath = "//*[@id='leftGroupButtons']/button[2]/span")
    Button updateModules;

    @ElementTitle(value = "Обновить контейнер")
    @FindBy(xpath = "//*[@id='leftGroupButtons']/button[3]/span")
    Button updateConteiner;

    @ElementTitle(value = "Загрузить")
    @FindBy(xpath = "//*[@id='leftGroupButtons']/button[4]/span")
    Button upload;

    @ElementTitle(value = "Удалить")
    @FindBy(xpath = "//*[@id='leftGroupButtons']/button[5]/span")
    Button delete;

    @ElementTitle(value = "Запустить")
    @FindBy(xpath = "//*[@id='rightGroupButtons']/button[1]/span")
    Button run;

    @ElementTitle(value = "Остановить")
    @FindBy(xpath = "//*[@id='rightGroupButtons']/button[2]/span")
    Button stop;

    @ElementTitle(value = "Наименование")
    @FindBy(xpath = "//*[@id='nameFilterGroupTab']")
    TextInput nameFilter;

    @ElementTitle(value = "Код")
    @FindBy(xpath = "//*[@id='codeFilterGroupTab']")
    TextInput codeFilter;

    @ElementTitle(value = "Тип")
    @FindBy(xpath = "//*[@id='typeFilterGroupTab']")
    Select typeFilter;

    @ElementTitle(value = "Применить фильтр")
    @FindBy(xpath = "//*[@id='filterButtonGroupTab']")
    Button filterButton;

    @ElementTitle(value = "Сбросить фильтр")
    @FindBy(xpath = "//*[@id='filterButtonGroupTabReset']")
    Button filterButtonReset;

    @ElementTitle("Группы модулей")
    @FindBy(id = "tabGroupsTableContainer")
    HypericTable groups;

    public GroupsTab() {
        assertTrue(tabList.getActiveTab().getText().equals("Группы модулей"));
        wait.until(ExpectedConditions.elementToBeClickable(install));
    }
}
