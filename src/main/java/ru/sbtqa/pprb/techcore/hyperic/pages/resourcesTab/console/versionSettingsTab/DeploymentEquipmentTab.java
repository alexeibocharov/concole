package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.support.FindBy;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка инфраструктуры. Зоны обслуживания")
public class DeploymentEquipmentTab extends PPRBContainersControlModule {

    @ElementTitle(value = "Серверы")
    @FindBy(xpath = "//*[@id='tab3_left_menu_hardware']/div[2]/button[1]/span")
    Button btnServers;

    @ElementTitle(value = "Стойки")
    @FindBy(xpath = "//*[@id='tab3_left_menu_hardware']/div[2]/button[2]/span")
    Button btnRack;

    @ElementTitle(value = "Машинные залы")
    @FindBy(xpath = "//*[@id='tab3_left_menu_hardware']/div[2]/button[3]/span")
    Button btnMashineHall;

    @ElementTitle(value = "Дата-центры")
    @FindBy(xpath = "//*[@id='tab3_left_menu_hardware']/div[2]/button[4]/span")
    Button btnDataCenter;

    public DeploymentEquipmentTab() {
        assertTrue(tabList.getActiveTab().getText().equals("Настройка инфраструктуры"));
    }
}
