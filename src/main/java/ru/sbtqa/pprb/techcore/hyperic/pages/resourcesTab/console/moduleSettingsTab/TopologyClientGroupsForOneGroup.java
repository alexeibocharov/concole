package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка модулей. Топология клиентских групп. Серверы группы модулей")
public class TopologyClientGroupsForOneGroup extends ModuleSettingsTab {

    @ElementTitle(value = "Добавить/исключить узлы")
    @FindBy(xpath = "//*[@id='showModifyNodesInClientGroupBtn']/span")
    Button changeGroupPort;

    @ElementTitle(value = "Серверы группы модулей")
    @FindBy(id = "GroupNodesTopologyTableContainer")
    HypericTable listCellsTableContainerTab;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'OK' or text() = 'ОК']")
    Button Ok;

    @ElementTitle(value = "Назад в Топология клиентских групп")
    @FindBy(id ="groupTopologyBreadCrumbBottom")
    Button back;

    public TopologyClientGroupsForOneGroup() {
        wait.until(ExpectedConditions.elementToBeClickable(back));
    }
}
