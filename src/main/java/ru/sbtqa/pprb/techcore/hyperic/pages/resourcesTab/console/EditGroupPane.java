package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Редактировать группу модулей")
public class EditGroupPane extends AnyPage {

    @ElementTitle(value = "Наименование группы")
    @FindBy(id = "group_name_edit")
    TextInput groupNameEdit;

    @ElementTitle(value = "Код группы")
    @FindBy(id = "group_code_edit")
    TextInput groupCodeEdit;

    @ElementTitle(value = "Порт")
    @FindBy(id = "group_port_edit")
    TextInput groupPortEdit;
    
    @ElementTitle(value = "Тип")
    @FindBy(id = "group_type_edit")
    Select type;

    @ElementTitle(value = "Путь к Java")
    @FindBy(id = "group_java_home_edit")
    TextInput groupJavaHomeEdit;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//*[@id='editGroupPane']//button[descendant::text() = 'Сохранить']")
    Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = "//*[@id='editGroupPane']//button[descendant::text() = 'Отменить']")
    Button btnCancel;

    public EditGroupPane() {
        wait.until(ExpectedConditions.elementToBeClickable(groupNameEdit));
    }
}
