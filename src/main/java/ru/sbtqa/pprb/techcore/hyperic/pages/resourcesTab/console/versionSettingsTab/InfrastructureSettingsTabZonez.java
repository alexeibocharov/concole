package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.support.FindBy;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка инфраструктуры. Зоны обслуживания")
public class InfrastructureSettingsTabZonez extends PPRBContainersControlModule {

    @ElementTitle(value = "Таблица зоны обслуживания")
    @FindBy(id = "ServiceZonesSettingsTableContainer")
    HypericTable serviseZoneTableContainer;

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = "//*[@id='Top-ServiceZonesSettingsTableContainer']/div[2]/div[1]/button[1]/span")
    public Button btnAdd;

    @ElementTitle(value = "Фильтр по имени зоны")
    @FindBy(xpath = "//*[@id='ServiceZonesSettingsTableContainer']/div/table/thead/tr/th[2]/div/div[2]")
    public Button FilterNameZone;

    @ElementTitle(value = "Удалить")
    @FindBy(xpath = "//*[@id='deleteServiceZonesConfirmed']/span")
    public Button delete;

    @ElementTitle(value = "OK")
    @FindBy(xpath = ".//button[text() = 'OK' or text() = 'ОК']")
    public Button ok;

    @ElementTitle(value = "Значение фильтра имя зоны обслуживания")
    @FindBy(xpath = "//input[@class='filterVal']")
    TextInput filterVersionVal;

    @ElementTitle(value = "Применить")
    @FindBy(xpath =".//button[text() = 'Применить']")
    Button aplly;

    public InfrastructureSettingsTabZonez() {
        assertTrue(tabList.getActiveTab().getText().equals("Настройка инфраструктуры"));
    }
}
