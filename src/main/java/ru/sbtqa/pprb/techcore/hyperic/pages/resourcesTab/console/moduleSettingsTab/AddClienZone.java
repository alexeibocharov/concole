package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 *
 */
@PageEntry(title = "Настройка инфраструктуры >> Зоны обслуживания")
public class AddClienZone extends AnyPage {

    @ElementTitle(value = "Наименование зоны обслуживания")
    @FindBy(id = "zone_name_edit")
    public TextInput nameOfZone;

    @ElementTitle(value = "Код зоны обслуживания")
    @FindBy(id = "zone_code_edit")
    public TextInput codeOfZone;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//*[@id='editServiceZonePane']/table/tbody/tr[3]/td[2]/table/tbody/tr/td[1]/button/span")
    public Button save;

}
