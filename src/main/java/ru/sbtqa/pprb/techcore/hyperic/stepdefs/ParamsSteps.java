package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.java.ru.Тогда;
import org.apache.xmlbeans.impl.tool.Extension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.utils.Params;

public class ParamsSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParamsSteps.class);

    @Тогда("^пользователь сохраняет уникальное значение переменной \"(.*?)\"$")
    public void generateUnicalVariable(String value) {
        Params.generateUnicalVariable(value);
        LOGGER.info("пользователь сохраняет уникальное значение переменной " + value);
    }

    @Тогда("^пользователь сохраняет уникальное значение числовой переменной \"(.*?)\"$")
    public void generateUnicalIntVariable(String value) {
        Params.generateUnicalintVariable(value);
        LOGGER.info("пользователь сохраняет уникальное значение числовой переменной " + value);
    }
}
