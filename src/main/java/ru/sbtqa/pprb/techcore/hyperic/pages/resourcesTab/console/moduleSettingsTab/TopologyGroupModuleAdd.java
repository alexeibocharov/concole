package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Настройка модулей -> Группы модулей -> Модули группы -> Добавить модуль")
public class TopologyGroupModuleAdd extends ModuleSettingsTab {

    @ElementTitle(value = "Добавить модуль")
    @FindBy(xpath = "//*[@id='TopologyGroupModuleAddTableContainer']")
    HypericTable topologySettingsTable;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//button[@title = 'Добавление выделенных модулей в группу']")
    Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = "//button[@title = 'Закрытие формы без сохранения']")
    Button cancel;

    public TopologyGroupModuleAdd() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='TopologyGroupModuleAddTableContainer']")));
    }

}
