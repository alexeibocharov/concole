package ru.sbtqa.pprb.techcore.hyperic.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.qautils.errors.AutotestError;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sbt-kolesnichenko-af on 20.10.2017.
 */
public class CommonMethods {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonMethods.class);

    public static void doubleClickOnWebelement(WebElement webElement) {
        try {
            Actions actions = new Actions(PageFactory.getWebDriver());
            actions.doubleClick(webElement).perform();
        } catch (java.util.NoSuchElementException e) {
            LOGGER.warn("Не найден веб-элемент на странице " + webElement.getText() + ". ", e);
        }
    }

    public static String getTextUsingAutoIt() {
        AutoItHelper autoItHelper = new AutoItHelper("[CLASS:Notepad]");
        return autoItHelper.getTextFromFile();
    }

    public static void closeFile(String wndHandle) {
        AutoItHelper autoItHelper = new AutoItHelper(wndHandle);
        autoItHelper.closeFile(wndHandle);
    }

    public static void chooseFile(String path) throws InterruptedException {
        AutoItHelper autoItHelper = new AutoItHelper();
        autoItHelper.chooseFile("Choose File to Upload", "Выбор выкладываемого файла", path);
    }

    public static String getTextUsingJava(String pathToFile) {
        StringBuilder stringBuilder = new StringBuilder();
        Path file = Paths.get(pathToFile);
        try (InputStream in = Files.newInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
        } catch (IOException x) {
            throw new AutotestError("Error read file \"" + pathToFile + "\"", x);
        }
        return stringBuilder.toString();
    }

    public static void setNewValueOfParameter(String groupName, String fileName, String paramName, String newValue) {
        String oldString = "\\[" + groupName + "\\]/" + paramName + "=(.*?)\r\n";
        String newString = "[" + groupName + "]/" + paramName + "=" + newValue + "\r\n";
        if ("Root".equals(groupName)) {
            oldString = "\\@" + paramName + "=(.*?)\r\n";
            newString = "@" + paramName + "=" + newValue + "\r\n";
        }
        try {
            File file = new File(fileName);
            String content = FileUtils.readFileToString(file, Charset.defaultCharset());
            content = content.replaceAll(oldString, newString);
            FileUtils.writeStringToFile(file, content, Charset.defaultCharset());
        } catch (IOException e) {
            LOGGER.warn("Error read or write file \"" + fileName +"\"", e);
        }
    }

    public static Map<String, String> getGroupParametersFromFile(String nodeName, String textFromFile) {
        Map<String, String> mapOfParametersFromFile = getAllParametersFromFile(textFromFile);
        Map<String, String> mapOfGroupParametersFromFile = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : mapOfParametersFromFile.entrySet()) {
            String key = entry.getKey();
            String matchingString = "[" + nodeName + "]";
            String valueOfProperty = entry.getValue();
            if(key.contains(matchingString)) {
                String correctedKey = StringUtils.substringAfterLast(key, "]/");
                mapOfGroupParametersFromFile.put(correctedKey, valueOfProperty);
            }
        }
        return mapOfGroupParametersFromFile;
    }

    public static Map<String, String> getRootParametersFromFile(String textFromFile) {
        Map<String, String> mapOfAllParametersFromFile = getAllParametersFromFile(textFromFile);
        Map<String, String> mapOfRootParametersFromFile = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : mapOfAllParametersFromFile.entrySet()) {
            String key = entry.getKey();
            String valueOfProperty = entry.getValue();
            if(key.matches(".*\\[.*\\].*")) continue;
            mapOfRootParametersFromFile.put(key, valueOfProperty);
        }
        return mapOfRootParametersFromFile;
    }

    public static Map<String, String> getAllParametersFromFile(String textFromFile) {
        Map<String, String> mapOfParametersFromFile = new LinkedHashMap<>();
        String[] arrayOfFileRaws = textFromFile.split("\n");
        for (int i = 1; i < arrayOfFileRaws.length; i++) {
            String curRaw = arrayOfFileRaws[i];
            String nameOfProperty = StringUtils.substringBetween(curRaw, "@", "=");
            String valueOfProperty = StringUtils.substringAfter(curRaw, "=");
            String correctedValue = StringEscapeUtils.unescapeJava(valueOfProperty);
            mapOfParametersFromFile.put(nameOfProperty, correctedValue);
        }
        return mapOfParametersFromFile;
    }

    public static Boolean elementIsFound(String xPath) {
        try {
            WebElement element = PageFactory.getWebDriver().findElement(By.xpath(xPath));
            LOGGER.info("Element is found", element);
        } catch (NoSuchElementException nsee) {
            LOGGER.info("Element not found", nsee);
            return false;
        }
        return true;
    }

    public static void uploadFile(WebElement element, String pathToXml) throws InterruptedException {
        Actions actions = new Actions(PageFactory.getWebDriver());
        int width = element.getSize().getWidth();
        int height = element.getSize().getHeight();
        actions.moveToElement(element, width - 5, height - 5).click().build().perform();
        String absolutePath = "";
        try {
            absolutePath = new File(pathToXml).getCanonicalPath();
        } catch (IOException e) {
            throw new AutotestError("File not found by path " + pathToXml);
        }
        CommonMethods.chooseFile(absolutePath);
    }

}
