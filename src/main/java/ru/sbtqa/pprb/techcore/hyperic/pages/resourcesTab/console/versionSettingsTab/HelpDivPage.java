package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Справка")
public class HelpDivPage extends PPRBContainersControlModule{

    final String helpDivPageXpath =  "//div[@class='helpDiv']";

    @ElementTitle(value = "Описание кнопок вкладки")
    @FindBy(xpath = helpDivPageXpath + "//table")
    HypericTable table;


    @ElementTitle(value = "Закрыть")
    @FindBy(xpath = "//div[@title='Описание']//span[@dojoattachpoint = 'closeButtonNode']")
    Button close;

    public HelpDivPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(helpDivPageXpath)));
    }
}
