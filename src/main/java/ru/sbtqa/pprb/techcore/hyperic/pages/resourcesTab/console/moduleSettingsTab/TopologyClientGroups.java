package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка модулей. Топология клиентских групп")
public class TopologyClientGroups extends ModuleSettingsTab {

    @ElementTitle(value = "Изменить порт для группы")
    @FindBy(xpath = "//*[@id='changePortForClientGroupButton']/span")
    Button changeClientPort;


    @ElementTitle(value = "Группы модулей размещаемые на клиентских узлах")
    @FindBy(id = "ClientGroupsTopologyTableContainer")
    HypericTable groupOfModulesOnClientCellsTab;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'OK' or text() = 'ОК']")
    Button Ok;

    @ElementTitle(value = "Очистить все фильтры")
    @FindBy(xpath ="//*[@id='Top-ClientGroupsTopologyTableContainer']/div[2]/div[3]/div[1]/img")
    Button reset;

    @ElementTitle(value = "Назад в меню Настройка модулей")
    @FindBy(xpath ="//*[@id='Top-ClientGroupsTopologyTableContainer']/div[5]/a/span")
    Button back;


    public TopologyClientGroups() {
        wait.until(ExpectedConditions.elementToBeClickable(back));
    }
}
