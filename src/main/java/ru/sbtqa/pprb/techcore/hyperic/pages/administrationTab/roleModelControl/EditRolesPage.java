package ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.roleModelControl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.AdministrationPage;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;

/**
 * Created by sbt-kolesnichenko-af on 24.11.2017.
 */
@PageEntry(title = "Управление пользователями>>Пользователи>>Пользователь>>Редактирование ролей")
public class EditRolesPage extends AdministrationPage {

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//div[@id='Top-UserRolesChangeContainer']//a[@title='Сохранить изменения']")
    public Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = "//div[@id='Top-UserRolesChangeContainer']//a[@title='Отменить изменения']")
    public Button cancel;

    @ElementTitle(value = "Переместить вправо")
    @FindBy(xpath = "//div[@id='Top-UserRolesChangeContainer']//img[contains(@src, 'arrow_select')]")
    public Button btnChooseNode;

    @ElementTitle(value = "Переместить влево")
    @FindBy(xpath = "//div[@id='Top-UserRolesChangeContainer']//img[contains(@src, 'arrow_deselect')]")
    public Button btnRemoveNode;

    @ElementTitle(value = "Доступные роли")
    @FindBy(xpath = "//select[child::optgroup[@label='Доступные роли']]")
    public Select selectNodeForChoose;

    @ElementTitle(value = "Назначенные роли")
    @FindBy(xpath = "//select[child::optgroup[@label='Назначенные роли']]")
    public Select selectNodesOfGroupModule;

    @ActionTitle(value = "переходит по хлебным крошкам на страницу")
    public void goByBreadTo(String title) {
        String xPath = "//div[@class='bread']//a[contains(., '" + title + "')]";
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath)));
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public EditRolesPage() {
        wait.until(ExpectedConditions.attributeToBe(By.id("Top-UserRolesChangeContainer"), "style", "display: block;"));
    }
}
