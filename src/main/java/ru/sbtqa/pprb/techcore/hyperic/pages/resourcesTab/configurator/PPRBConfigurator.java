package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Конфигуратор ППРБ")
public class PPRBConfigurator extends AnyPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListOfVersionsPage.class);

    @ElementTitle(value = "Список артефактов")
    @FindBy(xpath = "//div[@id='div_table_artifacts']")
    HypericTable artifacts;
    
    @ElementTitle(value = "Импорт модели")
    @FindBy(xpath = "//a[@id='importModel']")
    Button importModel;

    public PPRBConfigurator() {
        //        Коментирую, так как при отсутствии прав на страничку она отдаётся пустая и это корректное поведение
        //        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("importModel")));
        LOGGER.info("Page is initialized: " + this.getClass().getSimpleName());
    }


}

