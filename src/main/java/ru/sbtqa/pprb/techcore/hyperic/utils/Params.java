package ru.sbtqa.pprb.techcore.hyperic.utils;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Params {

    private static Logger LOGGER = Logger.getLogger(Params.class.getName());


    private static Map<String, String> parameters = new HashMap<>();

    private Params() {
        throw new IllegalAccessError("Utility class");
    }

    public static void setParam(String name, String value) {
        if (!name.trim().isEmpty()) {
            parameters.put(name.trim(), value);
        }
    }

    public static String getParam(String name) {
        if (parameters.containsKey(name.trim())) {
            return parameters.get(name.trim());
        }
        return name;
    }

    public static void resetParams() {
        parameters.clear();
    }


    public static void generateUnicalVariable(String var) {
        try {
            Params.setParam(var, var + "_" + String.valueOf((new Date()).getTime()));
            Thread.sleep(10);
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, ex.getMessage(), ex);
        }
    }

    public static void generateUnicalintVariable(String var) {
        try {
            Params.setParam(var, String.valueOf((new Date()).getTime()).substring((Math.max(String.valueOf((new Date()).getTime()).length(),4) -4)));
            Thread.sleep(10);
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, ex.getMessage(), ex);
        }
    }


    public static String inspect(String value) {

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            if (value.contains(entry.getKey())) {
                return entry.getValue();
            }
        }
        return value;
    }
}
