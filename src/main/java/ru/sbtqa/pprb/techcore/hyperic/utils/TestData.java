package ru.sbtqa.pprb.techcore.hyperic.utils;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import ru.sbtqa.tag.datajack.TestDataObject;
import ru.sbtqa.tag.datajack.adaptors.JsonDataObjectAdaptor;
import ru.sbtqa.tag.datajack.exceptions.DataException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class TestData {

    private TestData(){}

    public static TestDataObject getTestData(String jsonDataPath, String dataBlockName) throws DataException {
        TestDataObject testData = new JsonDataObjectAdaptor(jsonDataPath, dataBlockName);
        testData.applyGenerator(DataGensCallback.class);
        return testData;
    }

    public static String getNodeAttribute(String pathToXml, String attribute) {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        String attr = "";
        try {
            Element xml = builderFactory.newDocumentBuilder().parse(new File(pathToXml)).getDocumentElement();
            attr = xml.getAttribute(attribute);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        return attr;
    }

}