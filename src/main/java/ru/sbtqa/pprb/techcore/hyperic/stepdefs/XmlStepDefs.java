package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.DataTable;
import cucumber.api.java.ru.Когда;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper;
import ru.sbtqa.pprb.techcore.hyperic.utils.TestData;
import ru.sbtqa.tag.datajack.Stash;

import java.util.Map;

/**
 * Created by sbt-kolesnichenko-af on 16.10.2017.
 */
public class XmlStepDefs {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlStepDefs.class);

    @Когда("^анализ xml: информация о модели конфигурации$")
    public void analizeXmlNewModel(DataTable dataTable) {
        Map<String, String> xml = dataTable.asMap(String.class, String.class);
        String pathToXml = KeysHelper.MODEL_CONFIGURATIONS_PATH + xml.get("Имя файла");
        String artifactId = TestData.getNodeAttribute(pathToXml, "artifactId");
        Assert.assertTrue("Config xml parse error! ArtifactId is not found", !artifactId.isEmpty());
        String version = TestData.getNodeAttribute(pathToXml, "version");
        Assert.assertTrue("Config xml parse error! Version is not found", !version.isEmpty());
        Stash.put("artifactId", artifactId);
        Stash.put("version", version);
        Stash.put("uploadConfigurationModelFile", pathToXml);
    }


}
