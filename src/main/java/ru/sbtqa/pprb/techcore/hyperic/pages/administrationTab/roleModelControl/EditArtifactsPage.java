package ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.roleModelControl;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.AdministrationPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by sbt-kolesnichenko-af on 30.11.2017.
 */
@PageEntry(title = "Управление пользователями>>Назначение артефактов")
public class EditArtifactsPage extends AdministrationPage {

    @ElementTitle(value = "Поле фильтра доступных артефактов")
    @FindBy(xpath = "//input[@id = 'artifact_filter_0']")
    public TextInput inputArtifactFilter_0;

    @ElementTitle(value = "Поле фильтра назначенных артефактов")
    @FindBy(xpath = "//input[@id = 'artifact_filter_1']")
    public TextInput inputArtifactFilter_1;

    @ElementTitle(value = "Применить фильтр")
    @FindBy(xpath = "*//button[@id = 'searchArtifact']")
    public Button btnApplyArtifactFilter;

    @ElementTitle(value = "Сбросить фильтр")
    @FindBy(xpath = "*//button[or @id = 'clearFilterArtifact']")
    public Button btnResetArtifactFilter;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//div[@id='Top-UserArtifactsChangeContainer']//a/span[text()='Сохранить']")
    public Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = "//div[@id='Top-UserArtifactsChangeContainer']//a/span[text()='Отменить']")
    public Button cancel;

    @ElementTitle(value = "Переместить вправо")
    @FindBy(xpath = "//div[@id='Top-UserArtifactsChangeContainer']//img[contains(@src, 'arrow_select')]")
    public Button btnChooseNode;

    @ElementTitle(value = "Переместить влево")
    @FindBy(xpath = "//div[@id='Top-UserArtifactsChangeContainer']//img[contains(@src, 'arrow_deselect')]")
    public Button btnRemoveNode;

    @ElementTitle(value = "Доступные артефакты")
    @FindBy(xpath = "//select[child::optgroup[@label='Доступные артефакты']]")
    public Select selectAvailableArtifacts;

    @ElementTitle(value = "Назначенные артефакты")
    @FindBy(xpath = "//select[child::optgroup[@label='Назначенные артефакты']]")
    public Select selectAssignedArtifacts;

    public EditArtifactsPage() {
        wait.until(ExpectedConditions.attributeToBe(By.id("Top-UserArtifactsChangeContainer"), "style", "display: block;"));
    }
}
