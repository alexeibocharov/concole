package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.support.FindBy;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка инфраструктуры. Ячейки")
public class InfrastractureCellTab extends PPRBContainersControlModule {

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = "//*[@id='Top-WorkWithCellsSettingsTableContainer']/div[3]/div[1]/button[1]/span")
    Button add;

    @ElementTitle(value = "Изменить")
    @FindBy(id = "showEditCellPaneConfirmed")
    Button change;

    @ElementTitle(value = "Удалить")
    @FindBy(id = "deleteCellsConfirmed")
    Button delete;

    @ElementTitle(value = "Сформировать ячейки")
    @FindBy(xpath = "//*[@id='Top-WorkWithCellsSettingsTableContainer']/div[3]/div[3]/div[1]/button[2]/span")
    Button formCell;

    @ElementTitle("Ячейки")
    @FindBy(id = "CellsSettingsTableContainer")
    HypericTable cellsSettingsTableContainerTab;

    public InfrastractureCellTab() {
        assertTrue(tabList.getActiveTab().getText().equals("Настройка инфраструктуры"));
    }
}
