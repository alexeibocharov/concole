package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка модулей. Топология серверных групп. Список ячеек группы")
public class TopologyServiceGroupsForOneGroup extends ModuleSettingsTab {

    @ElementTitle(value = "Добавить/исключить ячейки")
    @FindBy(xpath = "//*[@id='ListCellsTableContainerAddRemoveBtn']/span")
    Button changeGroupPort;

    @ElementTitle(value = "Ячейки группы модулей")
    @FindBy(id = "ListCellsTableContainer")
    HypericTable listCellsTableContainerTab;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'OK' or text() = 'ОК']")
    Button Ok;

    @ElementTitle(value = "Очистить все фильтры")
    @FindBy(xpath ="//*[@id='Top-ListCellsTableContainer']/div[2]/div[3]/div[1]/img")
    Button reset;

    @ElementTitle(value = "Назад в Топологию серверных групп")
    @FindBy(xpath ="//*[@id='Top-ListCellsTableContainer']/div[5]/a/span")
    Button back;

    public TopologyServiceGroupsForOneGroup() {
        wait.until(ExpectedConditions.elementToBeClickable(back));
    }
}