package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.java.After;
import ru.sbtqa.pprb.techcore.hyperic.utils.CLI.CLIConsoleHelper;
import ru.sbtqa.tag.datajack.Stash;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sbt-kolesnichenko-af on 03.01.2018.
 */
public class ConsoleAfterStepDefs {

    @After("@8352")
    public void after8352() throws IOException {
        // TODO: 04.01.2018
        /*
        * Не работает сценарий deleteModulesGroup
        * Спросить у разрабов
        * */
        Map<String, String> scenarioData = Stash.getValue("groupModulesData");
        CLIConsoleHelper cliConsoleHelper = new CLIConsoleHelper("deleteModulesGroup");
        cliConsoleHelper.createScenario(scenarioData);
        cliConsoleHelper.runScenario();

    }

    @After("@8359")
    public void after8359() throws IOException {
        // TODO: 04.01.2018
        /*
        * Перед тем как удалить модули они должны быть исключены из группы, а такого сценария нет
        * Спросить у разрабов!
        *
        * */
        CLIConsoleHelper cliConsoleHelper = new CLIConsoleHelper("deleteModule");
        Map<String, String> scenarioData1 = new HashMap<String, String>(){{put("Код модуля", "Autotest8359ModuleCode1");}};
        cliConsoleHelper.createScenario(scenarioData1);
        cliConsoleHelper.runScenario();
        cliConsoleHelper.removeScenario();
        Map<String, String> scenarioData2 = new HashMap<String, String>(){{put("Код модуля", "Autotest8359ModuleCode2");}};
        cliConsoleHelper.createScenario(scenarioData2);
        cliConsoleHelper.runScenario();
        cliConsoleHelper.removeScenario();
        Map<String, String> scenarioData3 = new HashMap<String, String>(){{put("Код модуля", "Autotest8359ModuleCode3");}};
        cliConsoleHelper.createScenario(scenarioData3);
        cliConsoleHelper.runScenario();
        cliConsoleHelper.removeScenario();
    }
}
