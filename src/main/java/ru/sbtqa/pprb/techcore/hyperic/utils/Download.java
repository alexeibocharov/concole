package ru.sbtqa.pprb.techcore.hyperic.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.tag.allurehelper.AllureNonCriticalFailure;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.qautils.errors.AutotestError;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Download {
    private static final Logger LOGGER = LoggerFactory.getLogger(Download.class);

    private static RemoteWebDriver WINIUM_DRIVER;
    private static LocalTime TIMER;

    public static LocalTime getTimer() {
        return TIMER;
    }

    public static void setTimer(LocalTime timer) {
        Download.TIMER = timer;
    }

    public static void winiumDriverProcessStart() {
        try {
            while ((WINIUM_DRIVER == null || !checkWiniumProcess()) && LocalTime.now().isBefore(getTimer())) {
                if (!checkWiniumProcess()) {
                    File pathToWiniumFile = new File("src\\test\\resources\\webdrivers\\Winium.exe");
                    Runtime.getRuntime().exec(pathToWiniumFile.getAbsolutePath());
                }
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("debugConnectToRunningApp", true);
                String urlRemoteDriver = "http://localhost:9999";
                LOGGER.info("Winium драйвер еще не запущен");
                URL url = new URL(urlRemoteDriver);
                WINIUM_DRIVER = new RemoteWebDriver(url, capabilities);
            }
        } catch (IOException e) {
            LOGGER.info("Winium драйвер не найден", e.getClass());
        } catch (UnreachableBrowserException uBE) {
            LOGGER.info("Winium драйвер не успел запуститься или на машине не установлен .Net Framework.");
            if (LocalTime.now().isBefore(getTimer())) {
                LOGGER.info("Ошибка Winium драйвера. Пока есть еще время, инициализируем повторно.");
                winiumDriverProcessStart();
            }
        }
    }

    private static void winiumDriverProcessStop() {
        try {
            if (checkWiniumProcess())
                Runtime.getRuntime().exec("taskkill /F /IM Winium.exe");
        } catch (IOException e) {
            LOGGER.info("Не удалось остановить winium драйвер", e.getClass());
            AllureNonCriticalFailure.fire(new AutotestError("Не удалось остановить winium драйвер"));
        }
    }

    public static void setFolderSave(String pathForSave) {
        try {
            File pathToDirectory = new File(pathForSave);
            if (!pathToDirectory.exists()) {
                pathToDirectory.mkdir();
            }
            String pathToSaveFile = "REG ADD \"HKEY_CURRENT_USER\\Software\\Microsoft\\Internet Explorer\\Main\" /v " +
                    "\"Default Download Directory\" /t REG_SZ /d \"" + pathToDirectory.getCanonicalPath() + "\" /f";
            Runtime.getRuntime().exec(pathToSaveFile);
        } catch (IOException e) {
            LOGGER.info("Папка для сохранения файлов не установлена. Нет прав на изменение реестра.", e.getClass());
            throw new AutotestError("Папка для сохранения файлов не установлена. Нет прав на изменение реестра.");
        }
    }

    public static void waitForCreatingFile(String fileName) {
        int timeOut = PageFactory.getTimeOut();
        int startTime = LocalTime.now().getSecond();
        int endTime = LocalTime.now().getSecond() + timeOut;
        File file = new File(fileName);
        while (!file.exists() && startTime <= endTime) {
            file = new File(fileName);
            startTime++;
        }
    }

    private static void waitToOpenModalWindow() {
        setTimer(LocalTime.now().plusSeconds(PageFactory.getTimeOutInSeconds()));
        winiumDriverProcessStart();
        try {
            while (!WINIUM_DRIVER.findElement(By.id("1148")).isDisplayed() && LocalTime.now().isBefore(getTimer())) {
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            LOGGER.info("Ошибка во время ожидания модального окна");
        }
    }

    private static Boolean UiHWNDdoOpen() {
        try {
            if (WINIUM_DRIVER.findElementByClassName("DirectUIHWND").isDisplayed()) {
                Thread.sleep(5000);
                return true;
            }
        } catch (Exception eAll) {
            if (LocalTime.now().isBefore(getTimer())) {
                LOGGER.info("DirectUIHWND окна для скачивания пока нет");
                return false;
            }
        }
        return false;
    }

    public static void waitToOpenSaveWindow() {
        if (getTimer() == null) {
            setTimer(LocalTime.now().plusSeconds(PageFactory.getTimeOutInSeconds()));
        }
        try {
            while (!UiHWNDdoOpen() && LocalTime.now().isBefore(getTimer())) {
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            LOGGER.info("Ошибка во время ожидания модального окна");
        } catch (NoSuchElementException ex) {
            LOGGER.info("Скачивание пока не возможно. Нет окна");
            waitToOpenSaveWindow();
        } catch (UnreachableBrowserException ex) {
            if (LocalTime.now().isBefore(getTimer())) {
                LOGGER.info("Скачивание пока не возможно, возможно проблемы с драйвером. Перезапускаем драйвер");
                winiumDriverProcessStop();
                winiumDriverProcessStart();
                waitToOpenSaveWindow();
            }
        }
    }

    public static void pressButton(String button) {
        setTimer(LocalTime.now().plusSeconds(PageFactory.getTimeOutInSeconds()));
        winiumDriverProcessStart();
        setFolderSave(KeysHelper.TEMP_FOLDER_PATH);
        waitToOpenSaveWindow();
        clickOnButton("DirectUIHWND", button);
        winiumDriverProcessStop();
    }

    public static void clickOnButton(String classWindows, String nameElement) {
        RemoteWebDriver driver = WINIUM_DRIVER;
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElementByClassName(classWindows).findElement(By.name(nameElement))).click().build().perform();
    }

    public static void clickOnButton(String nameElement) {
        RemoteWebDriver driver = WINIUM_DRIVER;
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.name(nameElement))).click().build().perform();
    }

    private static Boolean checkWiniumProcess() {
        try {
            Process process = new ProcessBuilder("tasklist.exe", "/fo", "csv", "/nh").start();
            Scanner sc = new Scanner(process.getInputStream());
            List<String> ended = new ArrayList<>();
            while (sc.hasNextLine()) {
                ended.add(sc.nextLine());
            }
            for (String each : ended) {
                if (each.contains("Winium")) {
                    return true;
                }
            }
        } catch (IOException e) {
            LOGGER.info("Ошибка при просмотре списка запущенных процессов. Возможно нет прав.");
        }
        return false;
    }

    public static void uploadFileFromXmlFolder(String fileName) {
        File downloadFile = new File(KeysHelper.MODEL_CONFIGURATIONS_PATH);
        waitToOpenModalWindow();
        WINIUM_DRIVER.findElement(By.id("1148")).sendKeys(downloadFile.getAbsolutePath() + "\\" + fileName);
        clickOnButton("Открыть");
    }

}
