package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.NodesTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

/**
 * Created by sbt-kolesnichenko-af on 12.10.2017.
 */
@PageEntry(title = "Модуль управления контейнером ППРБ. Список узлов для остановки")
public class ListOfNodesToStop extends AnyPage {

    @ElementTitle(value = "Остановить")
    @FindBy(xpath = "//button[@id='stopNodesConfirmed']/span[text()='Остановить']")
    Button btnStopNodesConfirmed;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = "//button[@id='stopNodesCanceled']/span[text()='Отменить']")
    Button btnStartNodesCanceled;

    @ElementTitle("Узлы")
    @FindBy(xpath = "//div[@id='startNodesPane']/table")
    HypericTable nodes;

    public ListOfNodesToStop() {
        wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//div[@id='stopNodesPane']//div[@id='confirmStopNodesMsg']" +
                        "/p[contains(text(), 'Будет произведена остановка контейнера')]")
        ));
    }
}
