package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by sbt-kolesnichenko-af on 21.10.2017.
 */
@PageEntry(title = "Создание индивидуальных настроек")
public class IndividualSettingsPage extends AnyPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndividualSettingsPage.class);

    @ElementTitle(value = "Отмена")
    @FindBy(xpath = "//a[@id='cancelCustomLink']/span[@id='customFormBtnCancel']")
    public Button btnCancel;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//a[@id='saveCustomLink']/span[@id='customFormBtnSave']")
    public Button btnSave;

    @ElementTitle(value = "Модуль/группа")
    @FindBy(xpath = "//div[@id='addCustomForm']/input[@id='custom_module']")
    public TextInput inputModuleOrGroup;

    @ElementTitle(value = "Узел")
    @FindBy(xpath = "//div[@id='addCustomForm']/input[@id='custom_scope']")
    public TextInput inputNode;

    @ElementTitle(value = "Копировать значение настроек")
    @FindBy(xpath = "//div[@id='addCustomForm']/input[@id='custom_copy']")
    public CheckBox chkbCopyNodeSettings;

    public IndividualSettingsPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='cancelCustomLink']")));
        wait.until(ExpectedConditions.visibilityOf(inputModuleOrGroup));
        LOGGER.info("Page is initialized: " + this.getClass().getSimpleName());
    }
}
