package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка инфраструктуры")
public class InfrastructureSettingsTab extends PPRBContainersControlModule {


    @ElementTitle(value = "Инвентаризация ресурсов")
    @FindBy(xpath = "//*[@id='tab3_left_menu']/div[1]/button/span")
    public Button btnResourcesInventarization;

    @ElementTitle(value = "Размещение оборудования")
    @FindBy(xpath = "//*[@id='tab3_left_menu']/button[1]")
    public Button btnDeploymentEquipment;

    @ElementTitle(value = "Группы серверов")
    @FindBy(id = "showServerGroupFormBtn")
    public Button btnServiceGroup;

    @ElementTitle(value = "Серверы (платформы)")
    @FindBy(xpath = "//*[@id='tab3_left_menu']/div[2]/button/span")
    public Button btnServicePlatform;

    @ElementTitle(value = "Ячейки")
    @FindBy(xpath = "//*[@id='tab3_left_menu']/button[3]/span")
    public Button btnNodesAndCells;

    @ElementTitle(value = "Зоны обслуживания")
    @FindBy(xpath = "//*[@id='tab3_left_menu']/div[3]/button/span")
    public Button btnZones;

    @ElementTitle(value = "Обслуживание серверов")
    @FindBy(xpath = "//*[@id='tab3_left_menu']/button[4]/span")
    public Button btnServiceServers;

    @ElementTitle(value = "Справка")
    @FindBy(xpath = "//*[@id='tab3_left_menu']/button[5]/span")
    public Button btnHelp;

    public InfrastructureSettingsTab() {
        assertTrue(tabList.getActiveTab().getText().equals("Настройка инфраструктуры"));
    }
}
