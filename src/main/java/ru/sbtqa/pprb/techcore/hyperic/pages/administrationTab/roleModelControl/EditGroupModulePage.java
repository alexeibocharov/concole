package ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.roleModelControl;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.AdministrationPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by sbt-kolesnichenko-af on 30.11.2017.
 */
@PageEntry(title = "Управление пользователями>>Назначение групп модулей")
public class EditGroupModulePage extends AdministrationPage {

    @ElementTitle(value = "Поле для ввода названия группы")
    @FindBy(xpath = "//input[@id = 'module_filter_0']")
    public TextInput inputModuleFilter;

    @ElementTitle(value = "Поле для ввода артефакта")
    @FindBy(xpath = "//input[@id = 'artifact_filter']")
    public TextInput inputArtifactFilter;

    @ElementTitle(value = "Применить фильтр")
    @FindBy(xpath = "*//button[@id = 'searchModule']")
    public Button btnApplyFilter;

    @ElementTitle(value = "Сбросить фильтр")
    @FindBy(xpath = "*//button[@id = 'clearFilterModule']")
    public Button btnResetFilter;

    @ElementTitle(value = "Применить фильтр для поиска артефакта")
    @FindBy(xpath = "*//button[@id = 'searchArtifact']")
    public Button btnApplyArtifactFilter;

    @ElementTitle(value = "Сбросить фильтр для поиска артефакта")
    @FindBy(xpath = "*//button[@id = 'clearFilterArtifact']")
    public Button btnResetArtifactFilter;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//div[@id='Top-UserModulesChangeContainer']//a/span[text()='Сохранить']")
    public Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = "//div[@id='Top-UserModulesChangeContainer']//a/span[text()='Отменить']")
    public Button cancel;

    @ElementTitle(value = "Переместить вправо")
    @FindBy(xpath = "//div[@id='Top-UserModulesChangeContainer']//img[contains(@src, 'arrow_select')]")
    public Button btnChooseNode;

    @ElementTitle(value = "Переместить влево")
    @FindBy(xpath = "//div[@id='Top-UserModulesChangeContainer']//img[contains(@src, 'arrow_deselect')]")
    public Button btnRemoveNode;

    @ElementTitle(value = "Доступные группы модулей")
    @FindBy(xpath = "//select[child::optgroup[@label='Доступные группы модулей']]")
    public Select selectNodeForChoose;

    @ElementTitle(value = "Назначенные группы модулей")
    @FindBy(xpath = "//select[child::optgroup[@label='Назначенные группы модулей']]")
    public Select selectNodesOfGroupModule;

    @ElementTitle(value = "Доступные артефакты")
    @FindBy(xpath = "//select[child::optgroup[@label='Доступные артефакты']]")
    public Select selectAvailableArtifacts;

    @ElementTitle(value = "Назначенные артефакты")
    @FindBy(xpath = "//select[child::optgroup[@label='Назначенные артефакты']]")
    public Select selectAssignedArtifacts;

    public EditGroupModulePage() {
        wait.until(ExpectedConditions.attributeToBe(By.id("Top-UserModulesChangeContainer"), "style", "display: block;"));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='bread']//li[text()='Назначение групп модулей']")));
    }
}
