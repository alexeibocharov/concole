package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.TabList;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ")
public abstract class PPRBContainersControlModule extends AnyPage {

    @ElementTitle(value = "Вкладки")
    @FindBy(xpath = "//ul[@role = 'tablist']")
    public TabList tabList;

    public PPRBContainersControlModule() {
        wait.until(ExpectedConditions.elementToBeClickable(tabList));
        checkPageTitle("Модуль управления контейнером ППРБ");
    }

    @ActionTitle(value = "выбирает вкладку")
    public void selectTab(String tabName) {
        tabList.selectTab(tabName);
        assertTrue(tabList.getActiveTab().getText().equals(tabName));
    }
    @ActionTitle(value = "выбирает вкладку без проверки фокуса")
    public void selectTabWithoutCheckingFocus(String tabName) {
        tabList.selectTab(tabName);
    }
}

