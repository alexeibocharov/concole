package ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.roleModelControl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.AdministrationPage;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

/**
 * Created by sbt-kolesnichenko-af on 24.11.2017.
 */
@PageEntry(title = "Управление пользователями>>Пользователи>>Пользователь")
public class UserPage extends AdministrationPage {

    @ElementTitle(value = "Роли пользователя")
    @FindBy(id = "RolesUserTableContainer")
    protected HypericTable tblRolesOfUser;

    @ElementTitle(value = "Изменить")
    @FindBy(xpath = "//div[@id='Top-RolesUserTableContainer']//a/span[text()='Изменить']")
    public Button cancel;

    @ActionTitle(value = "переходит по хлебным крошкам на страницу")
    public void goByBreadTo(String title) {
        String xPath = "//div[@class='bread']//a[contains(., '" + title +"')]";
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath)));
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public UserPage() {
        wait.until(ExpectedConditions.attributeToBe(By.id("Top-RolesUserTableContainer"), "style", "display: block;"));
    }
}
