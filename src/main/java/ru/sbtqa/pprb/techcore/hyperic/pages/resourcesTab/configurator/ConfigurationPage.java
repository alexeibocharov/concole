package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sbt-kolesnichenko-af on 19.10.2017.
 */
@PageEntry(title = "Конфигурация")
public class ConfigurationPage extends AnyPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationPage.class);

    @ElementTitle(value = "Конфигурация")
    @FindBy(xpath = "//table[@id='treeConfiguration']")
    public HypericTable tableTreeConfiguration;

    @ElementTitle(value = "Свернуть все")
    @FindBy(xpath = "//a[@id='collapseAll']")
    public Button btnCollapseAll;

    @ElementTitle(value = "Развернуть все")
    @FindBy(xpath = "//a[@id='expandAll']")
    public Button btnExpandAll;

    @ElementTitle(value = "Сохранить все")
    @FindBy(xpath = "//a[@id='saveAll']")
    public Button btnSaveAll;

    @ElementTitle(value = "Версия")
    @FindBy(xpath = "//tr[@data-id-node='1']")
    public Button arrowVersion;

    @ElementTitle(value = "Root")
    @FindBy(xpath = "//tr[@data-id-node='1-0']")
    public Button arrowRoot;

    @ElementTitle(value = "Добавить узел")
    @FindBy(xpath = "//span[@class='GROUP_TYPE']/../span[@class='addNode']")
    public Button addNode;

    @ElementTitle(value = "Новый узел")
    @FindBy(xpath = "//span[@class='GROUP']/input")
    public TextInput inputNewNode;

    @ElementTitle(value = "Список версий")
    @FindBy(xpath = "//a[@class='backToListVersion']")
    public Link btnBackToListVersion;

    @ElementTitle(value = "Импорт")
    @FindBy(xpath = "//span[@class='importNode']")
    public Button btnImportNode;

    @ElementTitle(value = "Тип группы")
    @FindBy(xpath = "//div[@id='dialogSelect']//div[@id='boxSelect']/select[@class='selectChoice']")
    public Select chooseTypeOfGroup;

    // Кнопка Ок в Диалоге "Выбор элемента конфигурации" с выпадающим списком
    @ElementTitle(value = "Ok")
    @FindBy(xpath = "//div[@id='dialogSelect']//span[@class='ok']")
    public Button ok;

    public ConfigurationPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@id='expandAll']")));
        LOGGER.info("Page is initialized: " + this.getClass().getSimpleName());
    }

    @ActionTitle(value = "добавляет узел")
    public void addNode(String nodeName) {
        WebElement addNewNode = wait.until(ExpectedConditions.elementToBeClickable(addNode));
        addNewNode.click();
        WebElement inputNewGroup = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='GROUP']/input")));
        inputNewNode.clear();
        inputNewNode.sendKeys(nodeName);
        inputNewNode.sendKeys(Keys.ENTER);
    }

    @ActionTitle(value = "изменяет наименование созданного узла на")
    public void renameNode(String nodeName) {
        WebElement currentNode = PageFactory.getWebDriver().findElement(By.xpath("//tr/td/span[@class='GROUP']/input"));
        currentNode.click();
        currentNode.clear();
        currentNode.sendKeys(nodeName);
        currentNode.sendKeys(Keys.ENTER);
    }

    @ActionTitle(value = "удаляет узел")
    public void deleteNode(String nodeName) {
        String xpathOfDeleteNode = "//span[text()='" + nodeName + "']/../span[@class='deleteNode']";
        WebElement delNode = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathOfDeleteNode)));
        delNode.click();
        wait.until(ExpectedConditions.attributeContains(delNode, "class", "unDeleteNode"));
    }

    @ActionTitle(value = "отменяет удаление узла")
    public void unDeleteNode(String nodeName) {
        String xpathOfDeleteNode = "//span[text()='" + nodeName + "']/../span[@class='unDeleteNode']";
        WebElement unDelNode = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathOfDeleteNode)));
        unDelNode.click();
        wait.until(ExpectedConditions.attributeContains(unDelNode, "class", "deleteNode"));
    }

    @ActionTitle(value = "экспортирует узел")
    public void exportNode(String nodeName) {
        String xpathOfExportNode = "//span[text()='" + nodeName + "']/../span[@class='exportNode']";
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathOfExportNode)));
        WebDriver driver = PageFactory.getWebDriver();
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.xpath(xpathOfExportNode))).click().build().perform();
    }

    public Map<String, String> getAllParameters(String nodeName) {
        Map<String, String> mapParameters = new LinkedHashMap<>();
        String xpathOfDataIdNode = "//span[text()='" + nodeName + "']/../..";
        WebElement node = PageFactory.getWebDriver().findElement(By.xpath(xpathOfDataIdNode));
        String dataIdNode = node.getAttribute("data-id-node");
        List<WebElement> listOfNodeParameters = PageFactory.getWebDriver().findElements(
                By.xpath(xpathOfDataIdNode + "/../tr[@data-id-parent='" + dataIdNode + "']"));
        for (WebElement parameter : listOfNodeParameters) {
            try {
                String parameterName = parameter.findElement(By.xpath(".//td/span[contains(@class, 'PROPERTY')]")).getText();
                String parameterValue = parameter.findElement(By.xpath(".//td/span[@class='editable']")).getText();
                mapParameters.put(parameterName, parameterValue);
            } catch (NoSuchElementException nsee) {
                LOGGER.info("Probably element is not PROPERTY", nsee.getMessage());
            }
        }
        return mapParameters;
    }

    public Map<String, String> getNotEmptyParameters(String nodeName) {
        Map<String, String> mapOfAllParameters = this.getAllParameters(nodeName);
        Map<String, String> mapParameters = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : mapOfAllParameters.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue().trim();
            if (!value.isEmpty() && !value.contains("***")) mapParameters.put(key, value);
        }
        return mapParameters;
    }

    public WebElement getCellWithDefaultValue() {
        List<WebElement> listOfAllCells = PageFactory.getWebDriver().findElements(By.xpath("//tr/td/span[@class='isDefault']"));
        Assert.assertTrue("There are not cells with default value", !listOfAllCells.isEmpty());
        return listOfAllCells.get(0);
    }

    public WebElement getElementOfGroup(String elementName, String groupName) {
        String xPathOfGroup = "//span[text()='" + groupName + "']/../..";
        WebElement groupElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathOfGroup)));
        String dataIdNode = groupElement.getAttribute("data-id-node");
        String xPathOfElement = "//tr[@data-id-parent='" + dataIdNode + "']/td/span[text()='" + elementName + "']";
        return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xPathOfElement)));
    }

}
