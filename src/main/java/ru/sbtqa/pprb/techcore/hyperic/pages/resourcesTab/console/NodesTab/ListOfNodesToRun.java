package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.NodesTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

/**
 * Created by sbt-kolesnichenko-af on 12.10.2017.
 */
@PageEntry(title = "Модуль управления контейнером ППРБ. Список узлов для запуска")
public class ListOfNodesToRun extends AnyPage {

    @ElementTitle(value = "Запустить")
    @FindBy(xpath = "//button[@id='startNodesConfirmed']/span[text()='Запустить']")
    Button btnStartNodesConfirmed;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = "//button[@id='startNodesCanceled']/span[text()='Отменить']")
    Button btnStartNodesCanceled;

    @ElementTitle("Узлы")
    @FindBy(xpath = "//div[@id='startNodesPane']/table")
    HypericTable nodes;

    public ListOfNodesToRun() {
        wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//div[@id='startNodesPane']//div[@id='confirmStartNodesMsg']" +
                        "/p[contains(text(), 'Будет произведен запуск контейнера')]")
        ));
    }
}
