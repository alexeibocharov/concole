package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Настройка модулей -> Группы модулей -> Добавить")
public class TopologyAddGroupPage extends AnyPage {

    final String TopologyAddGroupPageXpath = "//*[@id='editGroupPane']";

    @ElementTitle(value = "Наименование группы")
    @FindBy(xpath = TopologyAddGroupPageXpath + "//*[@id='group_name_edit']")
    TextInput group_name_edit;

    @ElementTitle(value = "Код группы")
    @FindBy(xpath = TopologyAddGroupPageXpath + "//*[@id='group_code_edit']")
    TextInput cod_name_edit;

    @ElementTitle(value = "Порт")
    @FindBy(xpath = TopologyAddGroupPageXpath + "//*[@id='group_port_edit']")
    TextInput port_edit;

    @ElementTitle(value = "Тип")
    @FindBy(xpath = TopologyAddGroupPageXpath + "//*[@id='group_type_edit']")
    Select type;

    @ElementTitle(value = "Каталог для установки группы на серверах")
    @FindBy(xpath = TopologyAddGroupPageXpath + "//*[@id='group_folder_edit']")
    TextInput catalog_edit;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = TopologyAddGroupPageXpath + "//*[@id='editGroupFormBtn']/span")
    Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = TopologyAddGroupPageXpath + "//*[@id='cancelEditGroupFormBtn']/span")
    Button btnCancel;

    public TopologyAddGroupPage() {
        wait.until(ExpectedConditions.elementToBeClickable(save));
    }

}
