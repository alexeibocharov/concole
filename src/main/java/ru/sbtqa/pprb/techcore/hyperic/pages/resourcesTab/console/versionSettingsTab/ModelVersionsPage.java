package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Версии моделей данных")
public class ModelVersionsPage extends PPRBContainersControlModule {

    final String modelVersionsTablePageXpath = "//div[@id='Top-ModelVersionsTableContainer']";

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = modelVersionsTablePageXpath + "//button[descendant::text() = 'Добавить']")
    Button add;

    @ElementTitle(value = "Изменить")
    @FindBy(xpath = modelVersionsTablePageXpath + "//button[descendant::text() = 'Изменить']")
    Button edit;

    @ElementTitle(value = "Признак Устаревшая")
    @FindBy(xpath = modelVersionsTablePageXpath + "//button[descendant::text() = 'Признак \"Устаревшая\"']")
    Button deprecate;

    @ElementTitle(value = "Фильтр моделей данных")
    @FindBy(xpath = modelVersionsTablePageXpath + "//input[@id='ModelVersionsTableFilter']")
    TextInput filter;

    @ElementTitle(value = "Таблица Версии моделей данных")
    @FindBy(xpath = "//*[@id='ModelVersionsTableContainer']//table")
    HypericTable modelVersionsTableContainerTable;

    @ElementTitle(value = "Счетчик Версии моделей данных")
    @FindBy(id = "ModelVersionsTableContainer")
    HypericTable modelVersionsTableContainerCounter;

    public ModelVersionsPage() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(modelVersionsTablePageXpath)));
    }
}
