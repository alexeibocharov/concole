package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Версии моделей данных >> Изменить")
public class EditModelVersionPage extends AnyPage {

    final String editModelVersionPageXpath = "//div[@id='editModelVersion']";

    @ElementTitle(value = "Наименование версии модели")
    @FindBy(xpath = editModelVersionPageXpath + "//input[@id='modelVersion_name_edit']")
    TextInput modelVersion_name_edit;

    @ElementTitle(value = "Код версии модели")
    @FindBy(xpath = editModelVersionPageXpath + "//input[@id='modelVersion_code_edit']")
    TextInput modelVersion_code_edit;

    @ElementTitle(value = "Репозиторий")
    @FindBy(id = "modelVersion_repo_edit")
    Select modelVersion_repo_edit;

    @ElementTitle(value = "Путь к файлу в репозитории")
    @FindBy(xpath = editModelVersionPageXpath + "//input[@id='modelVersion_file_path_edit']")
    TextInput modelVersion_file_path_edit;

    @ElementTitle(value = "Файл")
    @FindBy(xpath = editModelVersionPageXpath + "//input[@id='modelVersion_file_edit']")
    TextInput modelVersion_file_edit;

    @ElementTitle(value = "Контрольная сумма")
    @FindBy(xpath = editModelVersionPageXpath + "//input[@id='check_sum_model']")
    TextInput check_sum_model;

    @ElementTitle(value = "Алгоритм")
    @FindBy(id = "check_sum_alg_model")
    Select check_sum_alg_model;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = editModelVersionPageXpath + "//button[descendant::text() = 'Сохранить']")
    Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = editModelVersionPageXpath + "//button[descendant::text() = 'Отменить']")
    Button cancel;

    public EditModelVersionPage() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(editModelVersionPageXpath)));
    }
}
