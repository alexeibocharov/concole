package ru.sbtqa.pprb.techcore.hyperic.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.List;

public class TabList extends TypifiedElement {

    public TabList(WebElement wrappedElement) {
        super(wrappedElement);
    }

    public boolean isThereTab(String tabName) {
        boolean isThere = false;
        List<WebElement> tabs =  this.getTabs();
        for (WebElement tab :  tabs) {
            if (tabName.equals(tab.getText())) isThere = true;
        }
        return isThere;
    }

    public List<WebElement> getTabs() {
        return this.getWrappedElement().findElements(By.xpath(".//li[@role = 'tab']"));
    }

    public WebElement getActiveTab() {
        return getTabs().stream().filter(tab -> tab.getAttribute("aria-selected").equals("true")).findFirst().get();
    }

    public void selectTab(String name) {
        getTabs().stream().filter(tab -> tab.getText().equals(name)).findFirst().get().click();
    }

}
