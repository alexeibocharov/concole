package ru.sbtqa.pprb.techcore.hyperic.utils;

import ru.sbtqa.tag.qautils.properties.Props;

public class KeysHelper {

    public static final String TESTDATA_PATH = Props.get("testdata.folder");
    public static final String TESTDATA_OBJECT = "testDataObject";
    public static final String VMOPTIONS_OBJECT = "testVMOptions";
    public static final String MODEL_CONFIGURATIONS_PATH = Props.get("modelConfigurations.folder");
    public static final String VMOPTIONS_PATH = Props.get("vmoptions.json.folder");
    public static final String TEMP_FOLDER_PATH = Props.get("temp.folder");
    public static final String UTILITES_FOLDER_PATH = Props.get("utilites.folder");
    public static final String CONFIG_FOLDER_PATH = Props.get("config.folder");
    public static final String CONSOLE_SCENARIOS_TEMPLATE_PATH = Props.get("console.scenarios.folder");
    public static final String CONFIGURATOR_UTILITE_VERSION = Props.get("configurator.utilite.version");
    public static final String CONSOLE_UTILITE_VERSION = Props.get("console.utilite.version");

    private KeysHelper() {
    }
}
