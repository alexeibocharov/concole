package ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.monte.media.gui.datatransfer.StringTransferable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;

/**
 * Created by sbt-kolesnichenko-af on 24.11.2017.
 */
@PageEntry(title = "Administration")
public class AdministrationPage extends AnyPage {

    public AdministrationPage() {
       wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='adminTab']//a[contains(text(), 'Administration')]")));
    }

    @ActionTitle(value = "переходит в плагин")
    public void goToPlugin(String pluginName) {
        String newPlaginName = "Управление пользователями";
        WebElement plugin = PageFactory.getWebDriver().findElement(By.xpath("//a[text()='" + newPlaginName + "']"));
        plugin.click();
    }

}
