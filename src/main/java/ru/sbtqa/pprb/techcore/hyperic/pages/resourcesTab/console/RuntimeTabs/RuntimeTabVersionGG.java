package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.RuntimeTabs;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Модуль управления контейнером ППРБ. Runtime. Версии GG на серверах")
public class RuntimeTabVersionGG extends PPRBContainersControlModule {

    @ElementTitle(value = "Установка версий")
    @FindBy(id = "gridVersionInstall")
    public Button installVersion;

    @ElementTitle(value = "Неиспользуемые версии")
    @FindBy(id = "unusedGridVersion")
    public Button notUsedVersions;

    @ElementTitle(value = "Статус обновления")
    @FindBy(id = "deleteNodeSpanRuntime")
    public Button statusOfRefresh;

    @ElementTitle(value = "Версии GG на серверах")
    @FindBy(id = "tabGridVersionNodeContainer")
    HypericTable versionGGOnServersTab;

    public RuntimeTabVersionGG() {
        assertTrue(tabList.getActiveTab().getText().equals("Runtime"));
        wait.until(ExpectedConditions.elementToBeClickable(installVersion));
    }
}
