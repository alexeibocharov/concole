package ru.sbtqa.pprb.techcore.hyperic.pages.dashboardTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;

import static org.junit.Assert.assertTrue;

@PageEntry(title = "Dashboard")
public class DashboardPage extends AnyPage {

    public DashboardPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dashTab")));
        wait.until(ExpectedConditions.elementToBeClickable(By.id("dashTab")));
        assertTrue(PageFactory.getWebDriver().getTitle().contains("Dashboard"));
    }
}
