package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

/**
 * Created by sbt-kolesnichenko-af on 23.01.2018.
 */
@PageEntry(title = "Настройка модулей -> Контейнер бизнес-логики")
public class KBLPage extends AnyPage {

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = "//button[@title = 'Добавление новой версии КБЛ']")
    Button addKBL;

    @ElementTitle(value = "Изменить")
    @FindBy(xpath = "//td[@id = 'showEditKBL']/button")
    Button btnEditKBL;

    @ElementTitle(value = "Удалить")
    @FindBy(xpath = "//td[@id = 'deleteKBL']/button")
    Button btnDeleteKBL;

    @ElementTitle(value = "Контейнер бизнес-логики")
    @FindBy(xpath = "//*[@id='KBLTableContainer']")
    HypericTable tableKBL;

    public KBLPage() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='Top-KBLTableContainer']")));
    }
}
