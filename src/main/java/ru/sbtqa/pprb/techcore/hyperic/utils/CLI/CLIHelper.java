package ru.sbtqa.pprb.techcore.hyperic.utils.CLI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.tag.allurehelper.ParamsHelper;
import ru.sbtqa.tag.allurehelper.Type;
import ru.sbtqa.tag.datajack.Stash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by sbt-kolesnichenko-af on 29.12.2017.
 */
public class CLIHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(CLIHelper.class);
    public static String standName;

    static {
        standName = Stash.getValue("standName");
    }

    public CLIHelper() {
    }

    protected void runProcess(String argLine, String type) throws IOException {
        ProcessBuilder pb = new ProcessBuilder(argLine.split(" "));
        pb.redirectErrorStream(true);
        Process p =  pb.start();
        String result = readResultOfProcess(p);
        LOGGER.info("Result of run process utilite - <" + result + ">");
        ParamsHelper.addAttachment( result.getBytes(),
                "Результат работы консольной утилиты. " + type, Type.TEXT);

    }

    private String readResultOfProcess(Process p) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while (null != (line = br.readLine())) {
            sb.append(line);
        }
        return new String(sb.toString().getBytes("UTF-8"));
    }

}
