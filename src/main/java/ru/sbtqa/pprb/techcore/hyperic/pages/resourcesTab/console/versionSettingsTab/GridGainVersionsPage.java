package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;


@PageEntry(title = "Управление версиями >> Версии GridGain")
public class GridGainVersionsPage extends PPRBContainersControlModule {

    final String gridGainVersionsTablePageXpath = "//div[@id = 'Top-GridGainVersionsTableContainer']";

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = gridGainVersionsTablePageXpath + "//button[descendant::text() = 'Добавить']")
    Button add;

    @ElementTitle(value = "Изменить")
    @FindBy(xpath = gridGainVersionsTablePageXpath + "//button[descendant::text() = 'Изменить']")
    Button edit;

    @ElementTitle(value = "Удалить")
    @FindBy(xpath = gridGainVersionsTablePageXpath + "//button[descendant::text() = 'Удалить']")
    Button delete;

    @ElementTitle(value = "По умолчанию")
    @FindBy(xpath = gridGainVersionsTablePageXpath + "//button[descendant::text() = 'По умолчанию']")
    Button getByDefault;

    @ElementTitle(value = "Признак Устаревшая")
    @FindBy(xpath = gridGainVersionsTablePageXpath + "//button[descendant::text() = 'Признак \"Устаревшая\"']")
    Button deprecate;

    @ElementTitle(value = "Фильтр версии GridGain")
    @FindBy(xpath = gridGainVersionsTablePageXpath + "//input[@id='filterGridGainVersions']")
    TextInput filter;

    @ElementTitle(value = "Счетчик Версии GridGain")
    @FindBy(id = "GridGainVersionsTableContainer")
    HypericTable GridGainVersionsTableContainerCounter;

    @ElementTitle(value = "Таблица Версии GridGain")
    @FindBy(xpath = "//*[@id='GridGainVersionsTableContainer']//table")
    HypericTable GridGainVersionsTableContainer;

    @ElementTitle(value = "ОК")
    @FindBy(xpath = "//div[@class='dijitDialog']//button[text()='OK']")
    Button ok;



    public GridGainVersionsPage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(gridGainVersionsTablePageXpath)));
    }
}
