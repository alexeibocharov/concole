package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.DataTable;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator.ConfigurationPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator.EditVersionPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator.ListOfVersionsPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.EditGroupPane;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab.TopologySettings;
import ru.sbtqa.pprb.techcore.hyperic.utils.CommonMethods;
import ru.sbtqa.pprb.techcore.hyperic.utils.Params;
import ru.sbtqa.tag.allurehelper.ParamsHelper;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.exceptions.PageException;
import ru.sbtqa.tag.pagefactory.exceptions.PageInitializationException;
import ru.sbtqa.tag.qautils.errors.AutotestError;
import ru.sbtqa.tag.qautils.strategies.MatchStrategy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static ru.sbtqa.pprb.techcore.hyperic.blocks.Dialog.ACTIVE_DIALOG_XPATH;

public class TableStepDefs {

    private static final Logger LOGGER = LoggerFactory.getLogger(TableStepDefs.class);
    public WebDriverWait wait = new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds());
    public static final String TABLE_CONTENT = "tableContent";
    public static final Map<String, Integer> HEADERS_OF_CONFIGURATION_TABLE = new LinkedHashMap<String, Integer>() {{
        put("Наименование элемента", 1);
        put("Тип элемента", 2);
        put("Значение в конфигураторе", 3);
    }};
    public static final Map<String, Integer> HEADERS_OF_LIST_VERSION_TABLE = new LinkedHashMap<String, Integer>() {
        {
            put("Наименование версии", 1);
            put("Наименование модуля/группы", 2);
            put("Наименование узла", 3);
            put("Опции VM", 4);
            put("Валидность", 5);
            put("Загрузил", 6);
            put("Дата загрузки", 7);
            put("Обновил", 8);
            put("Дата обновления", 9);
            put("Действие", 10);
        }
    };

    public List<Map<String, String>> listOfSelectedNodes = new ArrayList<>();

    public static final String MODULES_IS_NOT_RUNNED_ON_NODE = "icon_available_red";
    public static final String MODULES_RUNNED_ON_NODE = "icon_available_green";
    public static final String ENVELOPE_RUNNED_ON_NODE = "icon_available_yellow";

    @Когда("^(?:пользователь |он |)(находит|не находит) запись в таблице \"(.*?)\"$")
    public void findEntriesInTable(String search, String tableName, DataTable table) throws Throwable {
        Map<String, String> rowToFind = table.asMap(String.class, String.class);
        Map<String, String> inspectedMap = new HashMap<>();
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);

        for (String key : rowToFind.keySet()) {
            inspectedMap.put(key, Params.inspect(rowToFind.get(key)));
        }

        switch (search) {
            case "не находит":
                if (tbl.getRows().isEmpty()) {
                    ParamsHelper.addParam("Таблица пустая ", tableName);
                    return;
                }
                Assert.assertTrue("Row " + inspectedMap + " is found", tbl.findEntries(inspectedMap).size() == 0);
                ParamsHelper.addParam("Запись не найдена", rowToFind.toString());
                break;
            case "находит":
                if (tbl.getRows().isEmpty()) {
                    Assert.assertTrue("В таблице " + tableName + " нет записей.", tbl.getRows().isEmpty());
                    return;
                }
                Assert.assertTrue("Row " + inspectedMap + " not found", tbl.findEntries(inspectedMap).size() != 0);
                Stash.put("groupModulesData", inspectedMap);
                ParamsHelper.addParam("Запись найдена", inspectedMap.toString());
                break;
        }

        LOGGER.info("пользователь " + search + " запись " + inspectedMap.toString() + "в таблице " + tableName);
    }

    @Когда("^пользователь проверяет что таблица \"(.*?)\" пуста$")
    public void emptyTable(String tableName) throws Throwable {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);

                if (tbl.getRows().isEmpty()) {
                    ParamsHelper.addParam("Таблица пустая ", tableName);
                    return;
                }
                Assert.assertTrue("Таблица не пуста", tbl.getRows().isEmpty());
    }



    @Когда("^проверка для теста 6150$")
    public void forTest6150() throws Throwable {
        Page page = PageFactory.getInstance().getPage("Модуль управления контейнером ППРБ. Runtime. Версии GG на серверах. Неиспользуемые версии");
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle("Неиспользуемые версии");
        WebElement webElement1 = page.getElementByTitle("Переустановить на отмеченных серверах");
        WebElement webElement2 = page.getElementByTitle("Удалить с отмеченных серверов");
        if (tbl.getRows().isEmpty() && webElement1.isEnabled() && webElement2.isEnabled() ) {
            LOGGER.info("Проверка для теста 6150 прошла успешно");
            return;
        }
        if (tbl.getRows().isEmpty()) throw new AutotestError("Таблица 'Неиспользуемые версии' пустая");
        tbl.selectRow(0);
        webElement1.click();
        WebElement dialog = PageFactory.getWebDriver().findElement(By.xpath(ACTIVE_DIALOG_XPATH));
        wait.until(ExpectedConditions.elementToBeClickable(dialog));
        assertTrue(dialog.getText().contains("Нет прав на выполнение операции."));
        WebElement ok1 = PageFactory.getWebDriver().findElement(By.xpath(".//button[text() = 'OK' or text() = 'ОК']"));
        ok1.click();
        webElement2.click();
        WebElement dialog2 = PageFactory.getWebDriver().findElement(By.xpath(ACTIVE_DIALOG_XPATH));
        wait.until(ExpectedConditions.elementToBeClickable(dialog2));
        assertTrue(dialog2.getText().contains("Нет прав на выполнение операции."));
        WebElement ok2 = PageFactory.getWebDriver().findElement(By.xpath(".//button[text() = 'OK' or text() = 'ОК']"));
        ok2.click();
        LOGGER.info("Проверка для теста 6150 прошла успешно");
    }

    @Когда("^проверка, что запись в таблице \"(.*?)\" (с признаком|без признака) по умолчанию$")
    public void findEntriesInTableWithDefaultAttribute(String tableName, String defaultAttribute, DataTable table) throws Throwable {
        Map<String, String> rowToFind = table.asMap(String.class, String.class);
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);

        if (tbl.getRows().isEmpty()) {
            LOGGER.info("В таблице " + tableName + " нет записей.");
            return;
        }

        String styleAttribute = tbl.findEntries(rowToFind).get(0).getAttribute("style");

        switch (defaultAttribute) {
            case "с признаком":
                Assert.assertEquals("Запись " + rowToFind + " не содержит признак по умолчанию", "background-color: orange;", styleAttribute);
                ParamsHelper.addParam("Запись " + rowToFind + " содержит признак по умолчанию", rowToFind.toString());
                break;
            case "без признака":
                Assert.assertEquals("Запись " + rowToFind + " содержит признак по умолчанию", "", styleAttribute);
                ParamsHelper.addParam("Запись " + rowToFind + " не содержит признак по умолчанию", rowToFind.toString());
                break;
        }

        LOGGER.info("Проверили, что запись " + table + " в таблице " + tableName + " " + defaultAttribute + " по умолчанию");
    }

    @Когда("^анализ таблицы \"(.*?)\": артефакт (не загружен|загружен)$")
    public void selectEntriesInTable(String tableName, String status) throws Throwable {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        if (tbl.getRows().size() == 0) throw new AutotestError("Table is empty");

        wait.until(ExpectedConditions.elementToBeClickable(tbl));

        List<Map<String, String>> content = tbl.getRowsAsStringMappedToHeadings();
        Set<String> artifactSet = new LinkedHashSet<>();
        for (Map<String, String> map : content) {
            artifactSet.add(map.get("Оригинальное имя артефакта"));
        }
        String artifactId = Stash.getValue("artifactId");
        Assert.assertNotNull("ArtifactId is not put on Stash", artifactId);
        switch (status) {
            case "не загружен":
                Assert.assertFalse("Artifact already is loaded", artifactSet.contains(artifactId));
                return;
            case "загружен":
                Assert.assertTrue("Artifact is not loaded yet", artifactSet.contains(artifactId));
        }
    }

    @Когда("^проверяем что элементы \"(.*?)\" в таблице \"(.*?)\" подсвеченны$")
    public void checkIlluminatedElements (String element,String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<List<WebElement>> listVersions = tbl.getRows();
        for (List<WebElement> listVersion : listVersions) {
            for (WebElement el : listVersion) {
                if (el.getText().trim().contains(element)) {
                        Assert.assertTrue("Не все записи " + element + " в таблице " + tableName + " подсвеченны",el.getAttribute("innerHTML").contains("findStrColor"));
                    }
            }
            }
        LOGGER.info("Проверили, что все записи " + element + " в таблице " + tableName + " подсвеченны");
        }


    @Когда("^анализ таблицы \"(.*?)\": новая версия загружена$")
    public void checknewVersion(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        if (tbl.getRows().size() == 0) throw new AutotestError("Table is empty");

        // Проверяем, что артефакт с новой версией появился в таблице
        String expectedVersion = Stash.getValue("version");
        List<List<WebElement>> listVersions = tbl.getRows();
        Set<String> setStringVersion = new LinkedHashSet<>();
        for (List<WebElement> listVersion : listVersions) {
            String actualVersion = listVersion.get(1).getText().trim();
            if (!actualVersion.isEmpty()) setStringVersion.add(actualVersion);
        }
        Assert.assertTrue("Artifact of version \"" + expectedVersion + "\" is not found", setStringVersion.contains(expectedVersion));

        // Проверяем логин, дату, действие
        WebElement raw = ListOfVersionsPage.getRequestedRaw(tbl, expectedVersion);
        // Проверяем логин
        String expectedLogin = Stash.getValue("loginHyperic");
        String actualLogin = raw.findElement(By.xpath("./td[" + HEADERS_OF_LIST_VERSION_TABLE.get("Загрузил") + "]")).getText();
        Assert.assertTrue("Logins is different", expectedLogin.equals(actualLogin));

        //Проверяем Действие
        String actualAction = raw.findElement(By.xpath("./td[" + HEADERS_OF_LIST_VERSION_TABLE.get("Действие") + "]")).getText();
        Assert.assertTrue("Actions is different", "Загрузка модели".equals(actualAction));

//        // TODO: 20.10.2017 добавить проверку даты
    }

    @Когда("^переходит в список версий артефакта$")
    public void goToVersionOfArtifact() throws PageInitializationException {
        // проваливаемся в артефакт
        String artifactOriginalName = Stash.getValue("artifactId");
        String xpath = "//td[text()='" + artifactOriginalName + "']/../td[1]/a";
        WebDriverWait wait = new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds());
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        PageFactory.getInstance().getCurrentPage().clickWebElement(element);
    }

    @Когда("^в таблице \"(.*?)\" переходит в конфигурацию версии \"(.*?)\"$")
    public void createConfigurationForVersion(String tableName, String version) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        if (tbl.getRows().size() == 0) throw new AutotestError("Table is empty");
        List<List<WebElement>> listVersions = tbl.getRows();
        WebElement currentRaw;
        for (List<WebElement> listVersion : listVersions) {
            currentRaw = listVersion.get(1);
            String actualVersion = currentRaw.getText().trim();
            if (version.equals(actualVersion)) {
                WebElement linkVersion = currentRaw.findElement(By.tagName("a"));
                linkVersion.click();
                return;
            }
        }
    }

    @Когда("^в таблице \"(.*?)\" переходит в конфигурацию для автотеста$")
    public void goToConfigurationOfAutotestVersion(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        String testModuleAndNodeName = Stash.getValue("testModuleAndNodeName");
        String xpathOfAllModuleNames = ListOfVersionsPage.XPATH_OF_ALL_MODULE_NAMES;
        WebElement currentRaw = ListOfVersionsPage.searchRawOfTestVersion(xpathOfAllModuleNames, testModuleAndNodeName);
        String xPathVersionName = "./td[" + HEADERS_OF_LIST_VERSION_TABLE.get("Наименование версии") + "]/table/tbody/tr/td/a";
        WebElement versionName = currentRaw.findElement(By.xpath(xPathVersionName));
        versionName.click();
    }

    @Когда("^(?:пользователь |он |)выбирает запис(ь|и) в таблице \"(.*?)\"$")
    public void selectEntriesInTable(String plural, String tableName, DataTable table) throws Throwable {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        if (tbl.getRows().isEmpty()) throw new AutotestError("Таблица " + tableName + " пустая");
        listOfSelectedNodes = new ArrayList<>();

        table.asLists(String.class).forEach(strings -> {
                    Map<String, String> row = new HashMap<>();
                    row.put(strings.get(0), Params.inspect(strings.get(1)));
                    listOfSelectedNodes.add(row);
                }
        );

        for (Map<String, String> selectedNode : listOfSelectedNodes) {
            if (tbl.findEntries(selectedNode).isEmpty())
                throw new AutotestError("Запись " + selectedNode + " в таблице " + tableName + " не найдена");
            tbl.selectRows(selectedNode);
            ParamsHelper.addParam("Выбрана запись " + selectedNode + " в таблице " + tableName, selectedNode.toString());
            LOGGER.info("Выбрана запись " + selectedNode + " в таблице " + tableName);
        }
    }


    @Когда("^пользователь выбирает первую запись в таблице \"(.*?)\"$")
    public void selectFirstElementInTable(String tableName) throws Throwable {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        if (tbl.getRows().isEmpty()) throw new AutotestError("Таблица " + tableName + " пустая");
        tbl.selectRow(0);
        LOGGER.info("Выбрана запись " + 1 + " в таблице " + tableName);
    }

    @Когда("^пользователь проваливается в первую запись в таблице \"(.*?)\"$")
    public void intoFirstElementInTable(String tableName) throws Throwable {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        if (tbl.getRows().isEmpty()) throw new AutotestError("Таблица " + tableName + " пустая");
        tbl.findElement(By.xpath(".//a")).click();
        LOGGER.info("Провалились в " + 1 + " запись таблицы " + tableName);
    }

    @Когда("^(?:пользователь |он |)нажимает на запис(ь|и) в таблице \"(.*?)\"$")
    public void clickEntriesInTable(String plural, String tableName, DataTable table) throws Throwable {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        if (tbl.getRows().isEmpty()) throw new AutotestError("Таблица " + tableName + " пустая");
        listOfSelectedNodes = new ArrayList<>();

        table.asLists(String.class).forEach(strings -> {
                    Map<String, String> row = new HashMap<>();
                    row.put(strings.get(0), Params.inspect(strings.get(1)));
                    listOfSelectedNodes.add(row);
                }
        );

        for (Map<String, String> selectedNode : listOfSelectedNodes) {
            if (tbl.findEntries(selectedNode).isEmpty())
                throw new AutotestError("Запись " + selectedNode + " в таблице " + tableName + " не найдена");
            tbl.clickRows(selectedNode);
            ParamsHelper.addParam("Сделан клик по записи " + selectedNode + " в таблице " + tableName, selectedNode.toString());
            LOGGER.info("Сделан клик по записи " + selectedNode + " в таблице " + tableName);
        }
    }

    @Когда("^(?:пользователь |он |)в таблице \"(.*?)\" проваливается в ячейку \"(.*?)\" в записи, содержащей$")
    public void intoCellOfRowContains(String tableName, String cellHeader, DataTable table) throws Throwable {
        listOfSelectedNodes = new ArrayList<>();
        table.asLists(String.class).forEach(strings -> {
                    Map<String, String> row = new HashMap<>();
                    row.put(strings.get(0), Params.inspect(strings.get(1)));
                    listOfSelectedNodes.add(row);
                }
        );
        for (Map<String, String> rowsToFind : listOfSelectedNodes) {
            ParamsHelper.addParam("Переходим в строку", rowsToFind.toString());
            HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
            Map<String, WebElement> row = tbl.findEntriesMappedToHeadings(rowsToFind, MatchStrategy.CONTAINS).stream().findAny().get();
            row.get(cellHeader).findElement(By.xpath(".//a")).click();
        }

    }

    @Когда("^в таблице \"(.*?)\" проваливается в ячейку в поле Роль с параметрами$")
    public void goToRole(String tableName, DataTable dataTable) throws PageException {
        Map<String, String> rowsToFind = dataTable.asMap(String.class, String.class);
        ParamsHelper.addParam("Переходим в строку", rowsToFind.toString());
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        Map<String, WebElement> row = tbl.findEntriesMappedToHeadings(rowsToFind, MatchStrategy.CONTAINS).stream().findAny().get();
        try {
            //row.get("Роль").findElement(By.xpath(".//a[text()='" + rowsToFind.get("Роль") + "']")).click();
            row.get("Роль").findElement(By.xpath(".//a[contains(text(),'" + rowsToFind.get("Роль") + "')]")).click();
        } catch (NoSuchElementException nsee) {
            LOGGER.info("Role <<" + row.get("Роль").getText() + ">> is not editable");
        }
    }

    @Когда("^в таблице \"(.*?)\" пытается провалиться в любую редактируемую роль$")
    public void goToAnyRole(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<Map<String, WebElement>> allRowsOfRole = tbl.getRowsMappedToHeadings(new ArrayList<>(Arrays.asList("Роль")));
        boolean roleIsFound = false;
        for (Map<String, WebElement> map : allRowsOfRole) {
            WebElement cellOfRole = map.get("Роль");
            try {
                List<WebElement> listRoles = cellOfRole.findElements(By.xpath(".//a"));
                if (!listRoles.isEmpty()) {
                    WebElement role = listRoles.get(0);
                    role.click();
                    ParamsHelper.addParam("Проваливаемся в роль", role.getText());
                    roleIsFound = true;
                    break;
                }
            } catch (NoSuchElementException nsee) {
                ParamsHelper.addParam("Role is not editable", cellOfRole.getText());
            }
        }
        Assert.assertTrue("Not founded editable role. Test can't passed", roleIsFound);
    }


    @Когда("^(?:пользователь |он |)в таблице \"(.*?)\" устанавливает количество строк \"(.*?)\"$")
    public void setPageSizeInTable(String tableName, String cellCount) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        tbl.getPageSizeSelector().selectByValue(cellCount);
    }

    @Когда("^(?:пользователь |он |)запоминает содержимое таблицы \"(.*?)\"$")
    public void saveTableContent(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<Map<String, String>> content = tbl.getRowsAsStringMappedToHeadings();
        Stash.put(TABLE_CONTENT, content);
    }

    @Когда("^(?:пользователь |он |)проверяет что таблица \"(.*?)\" не изменилась$")
    public void checkIfTableChanged(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<Map<String, String>> actual = tbl.getRowsAsStringMappedToHeadings();
        List<Map<String, String>> expected = Stash.getValue(TABLE_CONTENT);
        assertEquals(expected, actual);
    }

    @Когда("^(?:пользователь |он |)проверяет что выбрана запись в таблице \"(.*?)\"$")
    public void checkIfRowSelected(String tableName, DataTable table) throws PageException {
        Map<String, String> rowsToFind = table.asMap(String.class, String.class);
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        assertTrue(tbl.isRowSelected(rowsToFind));
    }

    @Тогда("^количество узлов в поле \"(.*?)\" соответствует количеству в таблице \"(.*?)\"$")
    public void countNodesEquals(String field, String tableName) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(field);
        List<WebElement> listOfNodes = select.findElements(By.tagName("option"));
        int actualCountOfNodes = listOfNodes.size();
        List<Map<String, String>> listOfExpectedNodes = Stash.getValue(TABLE_CONTENT);
        int expectedCountOfNodes = listOfExpectedNodes.size();
        Assert.assertEquals("", expectedCountOfNodes, actualCountOfNodes);
    }

    @Тогда("^(?:пользователь |он |)проверяет что в таблице \"(.*?)\" выбранные узлы (запущены|остановлены)$")
    public void checkStatus(String tableName, String status) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<String> listOfStatuses = getListOfStatuses(tbl);
        switch (status) {
            case "запущены":
                listOfStatuses.stream().forEach(typeOfPic ->
                        Assert.assertTrue("Node is not runned", typeOfPic.equals(MODULES_RUNNED_ON_NODE) |
                                typeOfPic.equals(ENVELOPE_RUNNED_ON_NODE))
                );
                return;
            case "остановлены":
                listOfStatuses.stream().forEach(typeOfPic ->
                        Assert.assertTrue("Nodes are runned", typeOfPic.equals(MODULES_IS_NOT_RUNNED_ON_NODE))
                );
                return;
            default:
                throw new AutotestError("The status is not defined");
        }
    }

    private List<String> getListOfStatuses(HypericTable tbl) {
        List<String> listOfStatuses = new LinkedList<>();
        for (Map<String, String> rowsToFind : listOfSelectedNodes) {
            try {
                String src = tbl.findEntries(rowsToFind).get(0)
                        .findElement(By.xpath(".//span/img[contains(@src, '/images/')]"))
                        .getAttribute("src");
                String typeOfPic = src.substring(src.indexOf("icon"), src.lastIndexOf(".gif"));
                listOfStatuses.add(typeOfPic);
            } catch (Exception e) {
                LOGGER.info("Status is not defined");
            }
        }
        return listOfStatuses;
    }

    @Тогда("^таблица \"(.*?)\": дерево сворачивается$")
    public void treeCollapse(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        Button version = (Button) PageFactory.getInstance().getCurrentPage().getElementByTitle("Версия");
        wait.until(ExpectedConditions.attributeContains(version, "class", "collapsed"));
        List<WebElement> listBranches = tbl.findElements(By.xpath(".//tr[contains(@class, 'branch')]"));
        listBranches.stream().forEach(branch ->
                Assert.assertTrue("Branch is not collapsed", branch.getAttribute("class").contains("collapsed"))
        );
    }

    @Тогда("^таблица \"(.*?)\": дерево разворачивается$")
    public void treeExpand(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        Button version = (Button) PageFactory.getInstance().getCurrentPage().getElementByTitle("Версия");
        wait.until(ExpectedConditions.attributeContains(version, "class", "expanded"));
        List<WebElement> listBranches = tbl.findElements(By.xpath(".//tr[contains(@class, 'branch')]"));
        listBranches.stream().forEach(branch ->
                Assert.assertTrue("Branch is not expanded", branch.getAttribute("class").contains("expanded"))
        );
    }

    @Тогда("^таблица \"(.*?)\": сворачивает дерево с помощью стрелок$")
    public void treeCollapseWithArrows(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        ArrayList<WebElement> listBranches = (ArrayList<WebElement>) tbl.findElements(By.xpath(".//tr[contains(@class, 'branch')]"));
        Collections.reverse(listBranches);
        listBranches.stream().forEach(branch -> {
            branch.findElement(By.className("treegrid-indent")).click();
            wait.until(ExpectedConditions.attributeContains(branch, "class", "collapsed"));
            Assert.assertTrue("Branch is not collapsed", branch.getAttribute("class").contains("collapsed"));
        });
    }

    @Тогда("^таблица \"(.*?)\": разворачивает дерево с помощью стрелок$")
    public void treeExpandWithArrows(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        ArrayList<WebElement> listBranches = (ArrayList<WebElement>) tbl.findElements(By.xpath(".//tr[contains(@class, 'branch')]"));
        listBranches.stream().forEach(branch -> {
            branch.findElement(By.className("treegrid-indent")).click();
            wait.until(ExpectedConditions.attributeContains(branch, "class", "expanded"));
            Assert.assertTrue("Branch is not collapsed", branch.getAttribute("class").contains("expanded"));
        });
    }

    @Тогда("^кнопка удаления узла \"(.*?)\" заменена на кнопку отмены удаления$")
    public void unDeleteNode(String nodeName) {
        String xpathOfDeleteNode = "//span[text()='" + nodeName + "']/../span[@class='unDeleteNode']";
        WebElement unDeleteButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathOfDeleteNode)));
        Assert.assertTrue("The button for delete is not changed", unDeleteButton.getCssValue("background-image").contains("undelete.png"));
    }

    @Тогда("^содержимое узла \"(.*?)\" пригасает$")
    public void contentFades(String nodeName) {
        String xpathOfGroupRaw = "//span[text()='" + nodeName + "']/../..";
        String dataIdNode = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathOfGroupRaw))).getAttribute("data-id-node");
        List<WebElement> elementsInGroup = PageFactory.getWebDriver()
                .findElements(By.xpath("//tr[@data-id-parent='" + dataIdNode + "']"));
        // Проверяем что поля не редактируются
        for (WebElement raw : elementsInGroup) {
            String classOfEditableField = raw.findElement(By.xpath(".//td[3]")).getAttribute("class");
            Assert.assertTrue("Field is editable", "deletedNode".equals(classOfEditableField));
        }
        // Проверяем что все содержимое пригаснуло
        for (WebElement raw : elementsInGroup) {
            List<WebElement> allSpan = raw.findElements(By.tagName("span"));
            for (WebElement element : allSpan) {
                String colorOfSpan = element.getCssValue("color");
                Assert.assertTrue("Color is not changed", "rgba(192, 192, 192, 1)".equals(colorOfSpan));
            }
        }
    }

    @Тогда("^содержимое узла \"(.*?)\" принимает стандартное состояние$")
    public void contentIsStandartState(String nodeName) {
        String xpathOfGroupRaw = "//span[text()='" + nodeName + "']/../..";
        String dataIdNode = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathOfGroupRaw))).getAttribute("data-id-node");
        List<WebElement> elementsInGroup = PageFactory.getWebDriver()
                .findElements(By.xpath("//tr[@data-id-parent='" + dataIdNode + "']"));
        // Проверяем что поля редактируются
        for (WebElement raw : elementsInGroup) {
            String classOfEditableField = raw.findElement(By.xpath(".//td[3]")).getAttribute("class");
            Assert.assertTrue("Field is not editable", "onDblClickCellElement".equals(classOfEditableField));
        }
        // Проверяем что все содержимое не пригаснуто
        for (WebElement raw : elementsInGroup) {
            List<WebElement> allSpan = raw.findElements(By.tagName("span"));
            for (WebElement element : allSpan) {
                String colorOfSpan = element.getCssValue("color");
                Assert.assertTrue("Color is not changed", !"rgba(192, 192, 192, 1)".equals(colorOfSpan));
            }
        }
    }

    @Тогда("^узел \"(.*?)\" удален$")
    public void nodeIsDeleted(String nodeName) {
        try {
            PageFactory.getWebDriver().findElement(By.xpath("//span[text()='" + nodeName + "']"));
            throw new AutotestError("Node " + nodeName + " exists in DOM");
        } catch (org.openqa.selenium.NoSuchElementException nsee) {
            LOGGER.info("Node " + nodeName + " is removed!");
        }
    }

    @Тогда("^заполняет все обязательные текстовые поля$")
    public void fillAllRequiredField() {
        String xPathOfRequiredField = "//tr/td[" + HEADERS_OF_CONFIGURATION_TABLE.get("Наименование элемента") + "]/span[text()='*']/../..";
        List<WebElement> listOfRequiredFields = PageFactory.getWebDriver().findElements(By.xpath(xPathOfRequiredField));
        for (int i = 0; i < listOfRequiredFields.size(); i++) {
            String xPathOfTypeElementField = "child::td[" + HEADERS_OF_CONFIGURATION_TABLE.get("Тип элемента") + "]";
            String xPathOfValue = "./td[" + HEADERS_OF_CONFIGURATION_TABLE.get("Значение в конфигураторе") + "]";
            WebElement typeOfElement = listOfRequiredFields.get(i).findElement(By.xpath(xPathOfTypeElementField));
            WebElement webElementValue = listOfRequiredFields.get(i).findElement(By.xpath(xPathOfValue));
            // Проверяем что поле текстовое и не заполнено
            if ("STRING".equals(typeOfElement.getText()) && webElementValue.findElement(By.xpath("./span[@class='editable']")).getText().isEmpty()) {
                CommonMethods.doubleClickOnWebelement(webElementValue);
                WebElement input = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='editable']/input[@type]")));
                input.sendKeys("AnyText");
            }
        }
        // Убираем фокус с поля
        listOfRequiredFields.get(0).findElement(By.xpath("./..")).click();
    }

    @Тогда("^удаляет записи в обязательных полях$")
    public void removedDataFromRequiredField() {
        WebElement elementWithRequiredField = PageFactory.getWebDriver()
                .findElements(By.xpath("//tr/td[1]/span[text()='*']/../../td[3]")).get(0);
        CommonMethods.doubleClickOnWebelement(elementWithRequiredField);
        WebElement input = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span/input")));
        input.clear();
        // Убираем фокус с поля
        elementWithRequiredField.findElement(By.xpath("./..")).click();
    }

    @Тогда("^анализ таблицы \"(.*?)\": проверяет (валидность|невалидность) версии \"(.*?)\"$")
    public void checkValidationOfVersion(String tableName, String validation, String version) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        if (tbl.getRows().size() == 0) throw new AutotestError("Table is empty");
        // Проверяем валидность
        WebElement raw = ListOfVersionsPage.getRequestedRaw(tbl, version);
        String actualSign = raw.findElement(By.xpath("./td[" + HEADERS_OF_LIST_VERSION_TABLE.get("Валидность") + "]"))
                .findElement(By.xpath("./span/img")).getAttribute("src");
        switch (validation) {
            case "валидность":
                Assert.assertTrue("The version is not valid", actualSign.contains("check.gif"));
                return;
            case "невалидность":
                Assert.assertTrue("The version is valid", actualSign.contains("cross.gif"));
                return;
            default:
                throw new AutotestError("The error from the feature");
        }
    }

    @Тогда("^проверяет (валидность|невалидность) конфигурации$")
    public void checkValidationVersion(String validation, DataTable dataTable) throws PageException {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        ListOfVersionsPage listVersionPage = new ListOfVersionsPage();
        WebElement currRaw = listVersionPage.getRequestedRaw(data);
        String actualSign = currRaw.findElement(By.xpath("./td[" + HEADERS_OF_LIST_VERSION_TABLE.get("Валидность") + "]"))
                .findElement(By.xpath("./span/img")).getAttribute("src");
        switch (validation) {
            case "валидность":
                Assert.assertTrue("The version is not valid", actualSign.contains("check.gif"));
                return;
            case "невалидность":
                Assert.assertTrue("The version is valid", actualSign.contains("cross.gif"));
                return;
            default:
                throw new AutotestError("The error from the feature");
        }
    }

    @Тогда("^ищет/создает индивидуальные настройки конфигурации для автотеста$")
    public void searchOrCreateConfigurationForAutotest() throws PageException {
        String xpathOfAllModuleNames = ListOfVersionsPage.XPATH_OF_ALL_MODULE_NAMES;
        String testModuleAndNodeName = "autotest_version";
        Stash.put("testModuleAndNodeName", testModuleAndNodeName);
        WebElement currentRaw = ListOfVersionsPage.searchRawOfTestVersion(xpathOfAllModuleNames, testModuleAndNodeName);
        if (null == currentRaw) {
            // Если не нашли тестовую конфигурацию, то создаем индивидуальные настройка для версии, которая первая в списке
            currentRaw = PageFactory.getWebDriver().findElement(By.xpath("//table[@class='jtable']/tbody/tr[1]"));
            ListOfVersionsPage.createIndividualSettings(currentRaw, testModuleAndNodeName, true);
        }
    }

    @Тогда("^переходит в индивидуальные настройки конфигурации для автотеста$")
    public void goToConfigurationForAutotest() throws PageException {
        String xpathOfAllModuleNames = ListOfVersionsPage.XPATH_OF_ALL_MODULE_NAMES;
        String testModuleAndNodeName = Stash.getValue("testModuleAndNodeName");
        WebElement currentRaw = ListOfVersionsPage.searchRawOfTestVersion(xpathOfAllModuleNames, testModuleAndNodeName);
        ListOfVersionsPage.clickOnEditVMOptions(currentRaw);
    }

    @Тогда("^создает индивидуальные настройки для конфигурации$")
    public void createConfigurationForVersion(DataTable dataTable) throws PageInitializationException {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        ListOfVersionsPage listVersionPage = new ListOfVersionsPage();
        WebElement currRaw = listVersionPage.getRequestedRaw(data);
        WebElement createSettings = currRaw.findElement(By.xpath(ListOfVersionsPage.XPATH_OF_BUTTON_CREATE_SETTINGS));
        PageFactory.getInstance().getCurrentPage().clickWebElement(createSettings);
    }

    @Тогда("^удаляет конфигурацию$")
    public void deleteConfigurationForVersion(DataTable dataTable) throws PageInitializationException {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        ListOfVersionsPage listVersionPage = new ListOfVersionsPage();
        WebElement currRaw = listVersionPage.getRequestedRaw(data);
        Assert.assertNotNull("Element " + data + " is not found!", currRaw);
        WebElement deleteSettings = currRaw.findElement(By.xpath(ListOfVersionsPage.XPATH_OF_BUTTON_DELETE_SETTINGS));
        PageFactory.getInstance().getCurrentPage().clickWebElement(deleteSettings);
    }

    @Тогда("^в списке версий (присутсвует|отсутствует) конфигурация с параметрами$")
    public void thereIsConfiguration(String state, DataTable dataTable) {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        ListOfVersionsPage listVersionPage = new ListOfVersionsPage();
        WebElement currRaw = listVersionPage.getRequestedRaw(data);
        switch (state) {
            case "присутсвует":
                Assert.assertNotNull("The configuration " + data.toString() + " is not found in list of version", currRaw);
                ParamsHelper.addParam("Существует конфигурация с параметрами", data.toString());
                break;
            case "отсутствует":
                Assert.assertNull("The configuration " + data.toString() + " is found in list of version", currRaw);
                ParamsHelper.addParam("Отсутствует конфигурация с параметрами", data.toString());
                break;
        }

    }

    @Тогда("^переходит в индивидуальные настройки конфигурации с параметрами$")
    public void goToConfiguration(DataTable dataTable) throws PageException {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        ListOfVersionsPage listVersionPage = new ListOfVersionsPage();
        WebElement currRaw = listVersionPage.getRequestedRaw(data);
        String xPathOfVersion = ListOfVersionsPage.XPATH_OF_CELLS_OF_LIST_VERSION_TABLE.get("Наименование версии");
        WebElement webElement = currRaw.findElement(By.xpath(xPathOfVersion));
        ParamsHelper.addParam("Переходим в конфигурацию с параметрами", data.toString());
        PageFactory.getInstance().getCurrentPage().clickWebElement(webElement);
    }

    @Когда("^переходит в настройки точки взаимодействия \"(.*?)\"$")
    public void goToConfigurationOfPoint(String pointName) {
        PageFactory.getWebDriver().findElement(By.xpath("//a[text()='" + pointName + "']")).click();
    }

    @Тогда("^открывается страница с настройками точки взаимодействия \"(.*?)\"$")
    public void openPageWithConfigurationOfPoint(String pointName) {
        // название полностью выглядит примерно так - new-transport (esb-transport, 7.1.0.rc5-AutotestVersion)
        // нам нужно только часть - new-transport
        String point = pointName.split(" ")[0];
        Pattern pattern = Pattern.compile(" \\| " + point);
        wait.until(ExpectedConditions.textMatches(By.xpath("//span[@id='backToListVersionName']"), pattern));
    }

    @Тогда("^запоминает конфигурацию настроек родительской версии$")
    public void rememberConfiguration() {
        Stash.put("parentConfiguration", getMapConfiguration());
    }

    @Тогда("^запоминает конфигурацию настроек точки взаимодействия$")
    public void rememberConfigurationOfPoint() {
        Stash.put("pointConfiguration", getMapConfiguration());
    }

    public Map<String, String> getMapConfiguration() {
        WebElement pointConfiguration = PageFactory.getWebDriver().findElement(By.xpath("//table/tbody[@id='configuration']"));
        Map<String, String> map = new HashMap<>();
        List<WebElement> listRows = pointConfiguration.findElements(By.xpath("./tr"));
        for (int i = 1; i <= listRows.size(); i++) {
            String param = pointConfiguration
                    .findElement(By.xpath("./tr[" + i + "]/td[" +
                            HEADERS_OF_CONFIGURATION_TABLE.get("Наименование элемента") + "]")).getText();
            String value = pointConfiguration
                    .findElement(By.xpath("./tr[" + i + "]/td[" +
                            HEADERS_OF_CONFIGURATION_TABLE.get("Значение в конфигураторе") + "]")).getText();
            map.put(param, value);
        }
        Assert.assertTrue("Map is empty. Check xPathes of fields", !map.isEmpty());
        return map;
    }

    @Когда("^пауза$")
    public void pause() throws InterruptedException {
        Thread.sleep(1000);
    }

    @Тогда("^конфигурация настроек совпадает с конфигурацией родительской версии$")
    public void equalsConfigurations() {
        Map<String, String> currentConfiguration = getMapConfiguration();
        Map<String, String> parentConfiguration = Stash.getValue("parentConfiguration");
        Assert.assertEquals("Configurations is not equals", parentConfiguration, currentConfiguration);
    }

    @Тогда("^конфигурации точек взаимодействия совпадают$")
    public void equalsConfigurationsOfPoints() {
        Map<String, String> currentConfiguration = getMapConfiguration();
        Map<String, String> pointConfiguration = Stash.getValue("pointConfiguration");
        Assert.assertEquals("Configurations is not equals", pointConfiguration, currentConfiguration);
    }

    @Тогда("^на странице \"(.*?)\" присутствует элемент \"(.*?)\"$")
    public void elementIsDisplayed(String pageTitle, String element) throws PageException {
        Page page = PageFactory.getInstance().getPage(pageTitle);
        try {
            WebElement webElement = page.getElementByTitle(element);
            Assert.assertTrue("Element '" + element + "' is not displayed!", webElement.isDisplayed());
        } catch (NoSuchElementException nsee) {
            throw new AutotestError("Element <" + element + "> is not found on page <" + pageTitle + ">!", nsee);
        }


    }

    @Тогда("^анализ таблицы \"(.*?)\": проверяет заполненность поля Опции VM данными$")
    public void checkVMOptions(String tableName, DataTable table) throws PageException {
        Map<String, String> option = table.asMap(String.class, String.class);
        String expectedVMOption = EditVersionPage.getVMOption(option.get("Опции VM"));
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        String xpathOfAllModuleNames = ListOfVersionsPage.XPATH_OF_ALL_MODULE_NAMES;
        String testModuleAndNodeName = Stash.getValue("testModuleAndNodeName");
        WebElement currentRaw = ListOfVersionsPage.searchRawOfTestVersion(xpathOfAllModuleNames, testModuleAndNodeName);
        String actualVMOptions = currentRaw.findElement(By.xpath("./td[" + HEADERS_OF_LIST_VERSION_TABLE.get("Опции VM") + "]")).getText();
        Assert.assertTrue("VM Options are different", expectedVMOption.equals(actualVMOptions));
    }

    @Тогда("^изменяет значение параметра с пометкой default на \"(.*?)\"$")
    public void changeDefaultValue(String newValue) throws PageInitializationException {
        WebElement field = ((ConfigurationPage) PageFactory.getInstance().getCurrentPage()).getCellWithDefaultValue();
        WebElement input = field.findElement(By.xpath("./../span[@class='editable']"));
        Stash.put("defaultValue", input.getText());
        Stash.put("newValue", newValue);
        CommonMethods.doubleClickOnWebelement(input);
        input = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span/input")));
        PageFactory.getInstance().getCurrentPage().fillField(input, newValue);
        input.sendKeys(Keys.ENTER);
        boolean disappear = wait.until(ExpectedConditions
                .invisibilityOfElementLocated(By.xpath("//span[text()='" + newValue + "']/../span[@class='isDefault']")));
        Assert.assertTrue("Element is not disappear", disappear);
    }

    @Тогда("^возвращает прежнее значение и пометка default появляется$")
    public void revertDefaultValue() throws PageInitializationException {
        String newValue = Stash.getValue("newValue");
        WebElement input = PageFactory.getWebDriver().findElement(By.xpath("//td/span[@class='editable' and text()='" + newValue + "']"));
        CommonMethods.doubleClickOnWebelement(input);
        input = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span/input")));
        String defaultValue = Stash.getValue("defaultValue");
        PageFactory.getInstance().getCurrentPage().fillField(input, defaultValue);
        input.sendKeys(Keys.ENTER);
        WebElement defaultMark = wait.until(ExpectedConditions.presenceOfElementLocated(By
                .xpath("//span[text()='" + defaultValue + "']/../span[@class='isDefault']")));
        Assert.assertNotNull("Element is not appear", defaultMark);
    }

    @Тогда("^для числового параметра \"(.*?)\" устанавливает значение \"(.*?)\"$")
    public void setValue(String paramName, String value) throws PageInitializationException {
        String xPath = "//tr/td/span[contains(@class, 'PROPERTY') and text()='" + paramName + "']/../../td/span[@class='editable']";
        WebElement input = PageFactory.getWebDriver().findElement(By.xpath(xPath));
        CommonMethods.doubleClickOnWebelement(input);
        input = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span/input")));
        PageFactory.getInstance().getCurrentPage().fillField(input, value);
        input.sendKeys(Keys.ENTER);
    }

    @Тогда("^для параметра \"(.*?)\" в группе \"(.*?)\" устанавливает значение \"(.*?)\"$")
    public void setParameterValueOnGroup(String paramName, String groupName, String value) throws PageInitializationException {
        String xPathOfGroup = "//span[text()='" + groupName + "']/../..";
        WebDriverWait wait = new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds());
        WebElement groupElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathOfGroup)));
        String dataIdNode = groupElement.getAttribute("data-id-node");
        String xPath = "//tr[@data-id-parent='" + dataIdNode + "']/td/span[contains(@class, 'PROPERTY') and text()='" +
                paramName + "']/../../td/span[@class='editable']/..";
        WebElement input = PageFactory.getWebDriver().findElement(By.xpath(xPath));
        CommonMethods.doubleClickOnWebelement(input);
        input = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='editable']/input")));
        PageFactory.getInstance().getCurrentPage().fillField(input, value);
        input.sendKeys(Keys.ENTER);
    }

    @Тогда("^значение параметра \"(.*?)\" в группе \"(.*?)\" (равно|не равно) \"(.*?)\"$")
    public void checkParameterValueOnGroup(String paramName, String groupName, String equality, String expectedValue) throws PageInitializationException {
        String xPathOfGroup = "//span[text()='" + groupName + "']/../..";
        WebDriverWait wait = new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds());
        WebElement groupElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathOfGroup)));
        String dataIdNode = groupElement.getAttribute("data-id-node");
        String xPath = "//tr[@data-id-parent='" + dataIdNode + "']/td/span[contains(@class, 'PROPERTY') and text()='" +
                paramName + "']/../../td/span[@class='editable']";
        String actualValue = PageFactory.getWebDriver().findElement(By.xpath(xPath)).getText();
        switch (equality) {
            case "равно":
                String errMessageForEquals = "The values are different!";
                Assert.assertEquals(errMessageForEquals, expectedValue, actualValue);
                break;
            case "не равно":
                String errMessageForNotEquals = "The values are not different!";
                Assert.assertNotEquals(errMessageForNotEquals, expectedValue, actualValue);
                break;
            default:
                throw new AutotestError("Please, check this stepdef. Parameter \"equality\"");
        }
        ParamsHelper.addParam("Expected value: $1%s; actual value: $2%s", new String[]{expectedValue, actualValue});
    }

    @Тогда("^для параметра \"(.*?)\" в группе \"(.*?)\" удаляет значение$")
    public void removedValue(String paramName, String groupName) throws PageInitializationException {
        String xPathOfGroup = "//span[text()='" + groupName + "']/../..";
        WebElement groupElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathOfGroup)));
        String dataIdNode = groupElement.getAttribute("data-id-node");
        String xPath = "//tr[@data-id-parent='" + dataIdNode + "']/td/span[contains(@class, 'PROPERTY') and text()='" +
                paramName + "']/../../td/span[@class='editable']";
        WebElement input = PageFactory.getWebDriver().findElement(By.xpath(xPath));
        CommonMethods.doubleClickOnWebelement(input);
        input = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='editable']/input")));
        input.clear();
        input.sendKeys(Keys.ENTER);
    }

    @Тогда("^поле значения параметра \"(.*?)\" пустое$")
    public void checkValueIsNull(String paramName) {
        String xPath = "//tr/td/span[contains(@class, 'PROPERTY') and text()='" + paramName + "']/../../td/span[@class='editable']";
        String actualValue = PageFactory.getWebDriver().findElement(By.xpath(xPath)).getText();
        String errMessage = "The value is not empty! Actual value: " + actualValue;
        Assert.assertTrue(errMessage, actualValue.isEmpty());
    }

    @Тогда("^элемент с типом \"(.*?)\" (подлежит|не подлежит) редактированию$")
    public void checkEditableOfElement(String groupOfElement, String possibility) {
        String xPath = "//span[@class='" + groupOfElement + "']";
        // Берем первый элемент группы
        WebElement element = PageFactory.getWebDriver().findElements(By.xpath(xPath)).get(0);
        CommonMethods.doubleClickOnWebelement(element);
        boolean isExist = element.findElements(By.tagName("input")).isEmpty();
        switch (possibility) {
            case "подлежит":
                Assert.assertFalse("Element is not editable", isExist);
                break;
            case "не подлежит":
                Assert.assertTrue("Element is editable", isExist);
                break;
        }
    }

    @Тогда("^переименовывает элемент с типом \"(.*?)\" на \"(.*?)\"$")
    public void renameElement(String groupType, String newName) {
        String xPath = "//span[@class='" + groupType + "']";
        // Берем первый элемент группы
        WebElement element = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xPath))).get(0);
        CommonMethods.doubleClickOnWebelement(element);
        WebElement input = wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(element, By.tagName("input")));
        input.clear();
        input.sendKeys(newName);
        input.sendKeys(Keys.ENTER);
    }

    @Тогда("^в дереве присутствует элемент с типом \"(.*?)\" с именем \"(.*?)\"$")
    public void thereIsElementAndGroupInDom(String groupType, String name) {
        String xPath = "//span[@class='" + groupType + "' and text()='" + name + "']";
        boolean elementIsFound = CommonMethods.elementIsFound(xPath);
        Assert.assertTrue("Element is not found in DOM. XPath of element: " + xPath, elementIsFound);
    }

    @Тогда("^в дереве присутствует элемент с именем \"(.*?)\"$")
    public void thereIsElementAndGroupInDom(String name) {
        String xPath = "//*[text()='" + name + "']";
        boolean elementIsFound = CommonMethods.elementIsFound(xPath);
        Assert.assertTrue("Element is not found in DOM. XPath of element: " + xPath, elementIsFound);
    }

    @Тогда("^добавляет параметр группы \"(.*?)\"$")
    public void addParameterOfGroup(String groupName) {
        String xPathOfNodeId = "//span[text()='ipAddresses']/../..";
        String nodeId = PageFactory.getWebDriver().findElement(By.xpath(xPathOfNodeId)).getAttribute("data-id-node");
        String xPathOfAddedParams = "//tr[@data-id-parent='" + nodeId + "']";
        int countOfParamsBefore = PageFactory.getWebDriver().findElements(By.xpath(xPathOfAddedParams)).size();
        String xPath = "//span[text()='" + groupName + "']/../span[@class='addNode']";
        WebElement btnAddParam = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xPath)));
        btnAddParam.click();
        int countOfParamsAfter = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
                By.xpath(xPathOfAddedParams))).size();
        String errMessage = "Element is not to be added. Count before added: " + countOfParamsBefore + ", count after"
                + countOfParamsAfter;
        Assert.assertTrue(errMessage, countOfParamsAfter > countOfParamsBefore);
    }

    @Тогда("^добавляет группу \"(.*?)\" на узле \"(.*?)\"$")
    public void addGroupOnNode(String groupName, String nodeName) {
        String xPath = "//span[@class='GROUP_TYPE' and contains(text(),'" + nodeName + "')]/../span[@class='addNode']";
        WebElement addNewNode = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xPath)));
        addNewNode.click();
        WebElement inputNewGroup = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='GROUP']/input")));
        inputNewGroup.clear();
        inputNewGroup.sendKeys(groupName);
        inputNewGroup.sendKeys(Keys.ENTER);
    }

    @Тогда("^нажимает Добавить на узле \"(.*?)\" в группе \"(.*?)\"$")
    public void pressAddGroupOnNode(String nodeName, String groupName) {
        String xPathOfGroup = "//span[text()='" + groupName + "']/../..";
        WebElement groupElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathOfGroup)));
        String dataIdNode = groupElement.getAttribute("data-id-node");
        String xPathOfNode = "//tr[@data-id-parent='" + dataIdNode + "']//span[@class='GROUP_TYPE' and contains(text(),'" + nodeName + "')]/..";
        String xPathOfAdd = "/span[@class='addNode']";
        WebElement addNewNode = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xPathOfNode + xPathOfAdd)));
        addNewNode.click();
    }

    @Тогда("^появляется список опций на узле \"(.*?)\"$")
    public void thereIsListOfOptionOnNode(String nodeName) {
        String xPathOfNode = "//span[contains(text(),'" + nodeName + "')]/../..";
        WebElement groupElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathOfNode)));
        String dataIdNode = groupElement.getAttribute("data-id-node");
        String xPathOfNestedElements = "//tr[contains(@data-id-parent, '" + dataIdNode + "')]";
        List<WebElement> listOptions = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xPathOfNestedElements)));
        Assert.assertTrue("There is not options on node \"" + nodeName + "\"", !listOptions.isEmpty());
    }

    @Тогда("^переименовывает группу \"(.*?)\" на \"(.*?)\"$")
    public void renameGroup(String groupName, String newName) throws InterruptedException {
        String xPath = "//span[@class='GROUP' and text()='" + groupName + "']";
        // Берем первый элемент группы
        WebElement element = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xPath))).get(0);
        element = wait.until(ExpectedConditions.elementToBeClickable(element));
        CommonMethods.doubleClickOnWebelement(element);
        WebElement input = wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(element, By.tagName("input")));
        input.clear();
        input.sendKeys(newName);
        input.sendKeys(Keys.ENTER);
    }

    @Тогда("^(появляется|исчезает) тултип с текстом-пояснением для параметра \"(.*?)\"$")
    public void tooltipIsAppear(String state, String elementName) {
        String tooltip = Stash.getValue("tooltip");
        switch (state) {
            case "появляется":
                Assert.assertTrue("Tooltip for element '" + elementName + "' is not visible", tooltip.contains("block"));
                break;
            case "исчезает":
                Assert.assertTrue("Tooltip for element '" + elementName + "' is visible", tooltip.contains("none"));
                break;
        }
    }

    @Когда("^наводит курсор на параметр \"(.*?)\" в группе \"(.*?)\"$")
    public void hoverElement(String elementName, String groupName) throws PageInitializationException {
        WebElement param = ((ConfigurationPage) PageFactory.getInstance().getCurrentPage())
                .getElementOfGroup(elementName, groupName);
        Actions hover = new Actions(PageFactory.getWebDriver());
        hover.moveToElement(param, 5, 5).build().perform();
        WebElement tooltip = new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.presenceOfNestedElementLocatedBy(param, By.xpath("div[@class='description']")));
        new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.attributeContains(tooltip, "style", "block"));
        Stash.put("tooltip", tooltip.getAttribute("style"));
    }

    @Когда("^убирает курсор с параметра \"(.*?)\" в группе \"(.*?)\"$")
    public void hoverOnAnotherElement(String elementName, String groupName) throws PageInitializationException {
        WebElement param = ((ConfigurationPage) PageFactory.getInstance().getCurrentPage())
                .getElementOfGroup(elementName, groupName);
        Actions hover = new Actions(PageFactory.getWebDriver());
        hover.moveToElement(param, -5, -5).build().perform();
        WebElement tooltip = new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.presenceOfNestedElementLocatedBy(param, By.xpath("div[@class='description']")));
        new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.attributeContains(tooltip, "style", "none"));
        Stash.put("tooltip", tooltip.getAttribute("style"));
    }

    @Тогда("^создает при отсутствии группы с параметрами в таблице \"(.*?)\"$")
    public void createGroupsIfNot(String tableName, DataTable dataTable) throws PageException {
        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        for (Map<String, String> map : list) {
            if (tbl.findEntries(map).size() == 0) {
                PageFactory.getInstance().getCurrentPage().clickElementByTitle("Добавить");
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='group_name_edit']")));
                //------------ Заполняет информацию по группе модулей -----------------
                PageFactory.getInstance().getPage(EditGroupPane.class).fillField("Наименование группы", map.get("Наименование"));
                PageFactory.getInstance().getPage(EditGroupPane.class).fillField("Код группы", map.get("Код"));
                PageFactory.getInstance().getPage(EditGroupPane.class).fillField("Порт", map.get("Порт"));
                PageFactory.getInstance().getPage(EditGroupPane.class).select("Тип", map.get("Тип"));
                //----------------------------------------------------------------------
                PageFactory.getInstance().getCurrentPage().clickElementByTitle("Сохранить");
                wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@id='Top-TopologySettingsTableContainer']")));
                PageFactory.getInstance().getPage(TopologySettings.class);
            }
        }
    }

    @Тогда("^выбирает первый элемент списка в поле \"(.*?)\"$")
    public void chooseFirstNode(String field) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(field);
        select.getOptions().get(0).click();
    }

    @Тогда("^выбирает элемент списка \"(.*?)\" в поле \"(.*?)\"$")
    public void chooseFirstNode(String item, String field) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(field);
        select.selectByVisibleText(item);
    }

    @Тогда("^выбирает все элементы списка в поле \"(.*?)\"$")
    public void chooseAllNodes(String field) throws PageException, InterruptedException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(field);
        ParamsHelper.addParam("Список элементов в поле \"" + field + "\"", select.getText());
        for (WebElement option : select.getOptions()) {
            option.click();
            Thread.sleep(500);
        }
    }

    @Тогда("^поле \"(.*?)\" содержит перемещенные элементы$")
    public void thereIsNode(String field) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(field);
        Assert.assertTrue("Filed " + field + " is empty", !select.getOptions().isEmpty());
    }

    @Тогда("^в поле \"(.*?)\" появились пользователи у которых логин начинается на \"(.*?)\"$")
    public void userIsAppearInTextbox(String fieldName, String partOflogin) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(fieldName);
        for (WebElement option : select.getOptions()) {
            String login = StringUtils.substringBetween(option.getText(), "(", ")");
            Assert.assertTrue(StringUtils.startsWith(login, partOflogin));
        }
    }

    @Тогда("^в поле \"(.*?)\" появилсь пользователи с логинами \"(.*?)\"$")
    public void usersAreAppearInTextbox(String fieldName, String logins) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(fieldName);
        LinkedList<String> listLogins = new LinkedList<>(Arrays.asList(logins.split(",")));
        for (WebElement option : select.getOptions()) {
            String login = StringUtils.substringBetween(option.getText(), "(", ")");
            Assert.assertTrue(listLogins.remove(login.trim()));
        }
        Assert.assertTrue(listLogins.isEmpty());
    }

    @Когда("^очищает поле \"(.*?)\"$")
    public void clearField(String field) throws PageException {
        WebElement login = PageFactory.getInstance().getCurrentPage().getElementByTitle(field);
        login.clear();
    }

    @Тогда("^в поле \"(.*?)\" пользователь с логином \"(.*?)\" недоступен для добавления$")
    public void userIsNotAccessForAdding(String fieldName, String login) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(fieldName);
        List<WebElement> listOfUsers = select.getOptions();
        Assert.assertTrue(listOfUsers.size() == 1);
        Assert.assertTrue("User " + login + " is access for adding", "true".equals(listOfUsers.get(0).getAttribute("disabled")));
    }

    @Тогда("^поле \"(.*?)\" пустое$")
    public void thereIsNotNodesInField(String field) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(field);
        List<WebElement> listOfNodes = select.getOptions();
        Assert.assertTrue("Filed " + field + " is not empty! List of nodes " + listOfNodes.toString(), listOfNodes.isEmpty());
    }

    @Тогда("^запоминает узлы в поле \"(.*?)\"$")
    public void rememberNodes(String field) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(field);
        List<WebElement> listOfNodes = select.getOptions();
        List<Map<String, String>> listOfMapsOfParams = new LinkedList<>();
        for (WebElement node : listOfNodes) {
            String[] masOfAllParams = node.getText().split(";");
            Map<String, String> mapOfNodes = new LinkedHashMap<>();
            for (String strOfParam : masOfAllParams) {
                String[] masOfParam = strOfParam.split(":");
                mapOfNodes.put(masOfParam[0].trim(), masOfParam[1].trim());
            }
            listOfMapsOfParams.add(mapOfNodes);
        }
        Stash.put("MapOfNodes", listOfMapsOfParams);
    }

    @Тогда("^проверяет что таблица \"(.*?)\" содержит добавленные узлы$")
    public void tableContainsNodes(String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<Map<String, String>> actualListOfMapsOfParamsFromTable =
                tbl.getRowsAsStringMappedToHeadings(new LinkedList<>(Arrays.asList("Узел", "IP", "Дата-центр", "Зал", "Стойка")));
        List<Map<String, String>> expectedListOfMapsOfParams = Stash.getValue("MapOfNodes");
        String errMessage = "Actual list " + actualListOfMapsOfParamsFromTable +
                ", expected list " + expectedListOfMapsOfParams;
        Assert.assertEquals(errMessage, expectedListOfMapsOfParams.size(), actualListOfMapsOfParamsFromTable.size());
        List<Collection<String>> actualListOfValues = new LinkedList<>();
        List<Collection<String>> expectedListOfValues = new LinkedList<>();
        for (int i = 0; i < expectedListOfMapsOfParams.size(); i++) {
            actualListOfValues.add(actualListOfMapsOfParamsFromTable.get(i).values());
            expectedListOfValues.add(expectedListOfMapsOfParams.get(i).values());
        }
        for (int i = 0; i < expectedListOfValues.size(); i++) {
            Assert.assertTrue(errMessage, actualListOfValues.get(i).removeAll(expectedListOfValues.get(i)));
        }
    }

    @Тогда("^читает все значения поля \"(.*?)\" в таблице \"(.*?)\"$")
    public void readAllValuesInField(String fieldName, String tableName) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<Map<String, String>> listOfRoles = tbl.getRowsAsStringMappedToHeadings(new ArrayList<>(Arrays.asList(fieldName)));
        for (Map<String, String> map : listOfRoles) {
            System.out.println("\n<<<" + map.get(fieldName) + ">>>\n");
        }
    }

    @Тогда("^в таблице \"(.*?)\" значение поля \"(.*?)\" содержит список значений$")
    public void checkAllValuesInField(String tableName, String fieldName, DataTable dataTable) throws PageException {
        List<String> expectedListOfRoles = dataTable.asList(String.class).stream().collect(Collectors.toList());
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<Map<String, String>> listOfRows = tbl.getRowsAsStringMappedToHeadings(new ArrayList<>(Arrays.asList(fieldName)));
        List<String> actualListOfRoles = new ArrayList<>();
        for (Map<String, String> map : listOfRows) {
            actualListOfRoles.add(map.get(fieldName));
        }
        expectedListOfRoles.removeAll(actualListOfRoles);
        Assert.assertTrue("User is not contains roles " + expectedListOfRoles.toString(), expectedListOfRoles.isEmpty());
    }

    @Тогда("^в таблице \"(.*?)\" значение поля \"(.*?)\" не содержит запись \"(.*?)\"$")
    public void checkValueInField(String tableName, String fieldName, String checkedElement) throws PageException {
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<Map<String, String>> listOfRows = tbl.getRowsAsStringMappedToHeadings(new ArrayList<>(Arrays.asList(fieldName)));
        List<String> actualListOfElements = new ArrayList<>();
        for (Map<String, String> map : listOfRows) {
            actualListOfElements.add(map.get(fieldName));
        }
        Assert.assertFalse("The element is found in " + fieldName, actualListOfElements.contains(checkedElement));
    }

    @Тогда("^в таблице \"(.*?)\" присутствует запись$")
    public void readValueInField(String tableName, DataTable dataTable) throws PageException {
        Map<String, String> expectedRow = dataTable.asMap(String.class, String.class);
        HypericTable tbl = (HypericTable) PageFactory.getInstance().getCurrentPage().getElementByTitle(tableName);
        List<Map<String, String>> listOfRows = tbl.getRowsAsStringMappedToHeadings();
        boolean isThere = false;
        for (Map<String, String> map : listOfRows) {
            if (map.values().containsAll(expectedRow.values())) {
                isThere = true;
                break;
            }
        }
        Assert.assertTrue("Row " + expectedRow + " there is not in table " + tableName, isThere);
        ParamsHelper.addParam("В таблице \"" + tableName + "\" присутствует запись", expectedRow.toString());
    }

    @Тогда("^проверяет порядок следования параметров$")
    public void checkOrderOfParameters(DataTable dataTable) {
        List<String> listOrderedParams = dataTable.asList(String.class);
        WebElement body = PageFactory.getWebDriver().findElement(By.xpath("//tbody[@id='configuration']"));
        String[] masParams = body.getText().split("\n");
        List<String> listAllParams = new LinkedList<>();
        for (String param : masParams) {
            String[] mas = param.split(" ");
            listAllParams.add(mas[0]);
        }
        String strAllParams = StringUtils.substringBetween(listAllParams.toString(), "[", "]");
        String strOrderedParams = StringUtils.substringBetween(listOrderedParams.toString(), "[", "]");
        String errMessage = "Params " + listOrderedParams + " is not ordered! List of all params " + listAllParams;
        Assert.assertTrue(errMessage, strAllParams.contains(strOrderedParams));
    }

    @Тогда("^в поле \"(.*?)\" появился список содержащий слово \"(.*?)\"$")
    public void thereIsListWhichContainsWord(String fieldName, String expectedWord) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(fieldName);
        for (WebElement option : select.getOptions()) {
            String artifact = StringUtils.substringBetween(option.getText(), "[", "]");
            Assert.assertTrue(artifact.contains(expectedWord));
        }
    }

    @Тогда("^в поле \"(.*?)\" (не содержится|содержится) элемент \"(.*?)\"$")
    public void thereIsElementInField(String fieldName, String contain, String expectedElement) throws PageException {
        Select select = (Select) PageFactory.getInstance().getCurrentPage().getElementByTitle(fieldName);
        List<String> listOfValues = select.getOptions().stream().map(WebElement::getText).collect(Collectors.toList());
        switch (contain) {
            case "содержится":
                Assert.assertTrue("Element " + expectedElement + " is not found in " + listOfValues,
                        listOfValues.contains(expectedElement));
                break;
            case "не содержится":
                Assert.assertTrue("Element " + expectedElement + " is found in " + listOfValues,
                        !listOfValues.contains(expectedElement));
                break;
        }
    }

}
