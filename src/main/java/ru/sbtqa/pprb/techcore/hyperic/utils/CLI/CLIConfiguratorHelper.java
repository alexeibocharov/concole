package ru.sbtqa.pprb.techcore.hyperic.utils.CLI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper;
import ru.sbtqa.tag.allurehelper.ParamsHelper;

import java.io.IOException;

/**
 * Created by sbt-kolesnichenko-af on 29.12.2017.
 */
public class CLIConfiguratorHelper extends CLIHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(CLIConfiguratorHelper.class);
    public static String utilite;
    public static String configFile;

    static {
        utilite = KeysHelper.UTILITES_FOLDER_PATH + KeysHelper.CONFIGURATOR_UTILITE_VERSION + ".jar";
        configFile = KeysHelper.CONFIG_FOLDER_PATH + "configurator_" + standName.toLowerCase() + ".properties";
    }

    public CLIConfiguratorHelper() {
    }

    /**
     * Метод для удаления версии артефакта.
     * Пример команды для удаления версии:
     *      java -jar loader-version.jar -mode d
     *          -cf <имя файла настроек>
     *          -a <артефакт>
     *          -v <версия>
     *          [-n <узел>]
     *          [-m <модуль>]
     *          [-path <путь к группе>]
     *
     */
    public void removeArtifactVersion(String artifact, String version, String module, String node)
            throws IOException, InterruptedException {
        String moduleValue = module.isEmpty() ? "" : "-m " + module;
        String nodeValue = node.isEmpty() ? "" : "-n " + node;
        String argLine = String.format("java -jar %1$s -mode d -cf %2$s -a %3$s -v %4$s %5$s %6$s",
                utilite, configFile, artifact, version, moduleValue, nodeValue);
        LOGGER.info("ArgLine - <" + argLine + ">");
        ParamsHelper.addParam("ArgLine", argLine);
        runProcess(argLine, "Удаление версии конфигурации артефакта");
    }

    public void loadModelConfiguration(String configModel) throws IOException, InterruptedException {
        String argLine = String.format("java -jar %1$s -mode s -cf %2$s -sf %3$s", utilite, configFile, configModel);
        LOGGER.info("ArgLine - <" + argLine + ">");
        ParamsHelper.addParam("ArgLine", argLine);
        runProcess(argLine, "Загрузка модели конфигурации артефакта");
    }

    public void createModelConfigurationWithOverlap(String artifact, String version, String module, String node)
            throws IOException, InterruptedException {
        String moduleValue = module.isEmpty() ? "" : "-m " + module;
        String nodeValue = node.isEmpty() ? "" : "-n " + node;
        String argLine = String.format("java -jar %1$s -mode p -cf %2$s -a %3$s -v %4$s %5$s %6$s",
                utilite, configFile, artifact, version, moduleValue, nodeValue);
        LOGGER.info("ArgLine - <" + argLine + ">");
        ParamsHelper.addParam("ArgLine", argLine);
        runProcess(argLine, "Загрузка конфигурации артефакта");
    }

}
