package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Список модулей")
public class ModulesSettingsPage extends PPRBContainersControlModule {

    final String modulesSettingsPageXpath = "//div[@id='Top-ModulesSettingsTableContainer']";

    @ElementTitle(value = "Строка поиска")
    @FindBy(id = "ModulesSettingsTableFilter")
    TextInput search;

    @ElementTitle(value = "Добавить")
    @FindBy(xpath = modulesSettingsPageXpath + "//button[descendant::text() = 'Добавить']")
    Button add;

    @ElementTitle(value = "Изменить")
    @FindBy(xpath = modulesSettingsPageXpath + "//button[descendant::text() = 'Изменить']")
    Button edit;

    @ElementTitle(value = "Удалить")
    @FindBy(xpath = modulesSettingsPageXpath + "//button[descendant::text() = 'Удалить']")
    Button delete;

    @ElementTitle(value = "Фильтр модулей")
    @FindBy(xpath = modulesSettingsPageXpath + "//input[@id='ModulesSettingsTableFilter']")
    TextInput filter;

    @ElementTitle(value = "Модули")
    @FindBy(id = "ModulesSettingsTableContainer")
    HypericTable modelVersionsTableContainer;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'ОК']")
    Button Ok;

    @ElementTitle(value = "Табличный фильтр модулей")
    @FindBy(xpath = "//*[@id='ModulesSettingsTableContainer']/div/table/thead/tr/th[2]/div/div[2]")
    Button filterModule;

    @ElementTitle(value = "Значение фильтра модуль")
    @FindBy(xpath = "//input[@class='filterVal']")
    HypericTable filterVersionVal;

    @ElementTitle(value = "Применить")
    @FindBy(xpath =".//button[text() = 'Применить']")
    Button apply;

    @ElementTitle(value = "Сбросить")
    @FindBy(xpath =".//button[text() = 'Сбросить']")
    Button cancel;

    @ElementTitle(value = "Закрыть")
    @FindBy(xpath =".//button[text() = 'Закрыть']")
    Button close;


    public ModulesSettingsPage() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(modulesSettingsPageXpath)));
    }
}
