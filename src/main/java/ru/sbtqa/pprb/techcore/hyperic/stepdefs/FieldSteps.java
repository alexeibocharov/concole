package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.java.ru.Тогда;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.tag.pagefactory.exceptions.PageInitializationException;
import ru.sbtqa.tag.stepdefs.GenericStepDefs;

import static ru.sbtqa.pprb.techcore.hyperic.utils.Params.inspect;

public class FieldSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(FieldSteps.class);

    GenericStepDefs genericStepDefs = new GenericStepDefs();

    @Тогда("пользователь заполняет поле \"(.*?)\" значением \"(.*?)\"")
    public void fillField(String element, String value) throws NoSuchMethodException, PageInitializationException {
        String inspectedValue = inspect(value);
        genericStepDefs.userActionTwoParams("заполняет поле", element, inspectedValue);
        LOGGER.info("пользователь заполняет поле " +  element + " значением " + inspectedValue);
    }
}
