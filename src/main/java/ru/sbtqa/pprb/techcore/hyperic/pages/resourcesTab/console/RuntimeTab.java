package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

import static org.junit.Assert.assertTrue;

/**
 * Created by sbt-kolesnichenko-af on 24.01.2018.
 */
@PageEntry(title = "Модуль управления контейнером ППРБ. Runtime")
public class RuntimeTab extends PPRBContainersControlModule {

    @ElementTitle(value = "Версии GG на серверах")
    @FindBy(id = "GGOnServersBtn")
    public Button btnGGVersions;

    @ElementTitle(value = "Обновить GG")
    @FindBy(id = "addNodeSpanRuntime")
    public Button refreshGG;

    @ElementTitle(value = "Статус обновления")
    @FindBy(id = "deleteNodeSpanRuntime")
    public Button statusOfRefresh;

    @ElementTitle(value = "Синхронизировать")
    @FindBy(xpath = "//div[@id='tabRuntimeButtons']//span[@id='changeNodeSpan']")
    public Button synchronize;

    public RuntimeTab() {
        assertTrue(tabList.getActiveTab().getText().equals("Runtime"));
        wait.until(ExpectedConditions.elementToBeClickable(btnGGVersions));
    }
}
