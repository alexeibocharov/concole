package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.versionSettingsTab;


import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

@PageEntry(title = "Управление версиями >> Версии GridGain >> Изменить")
public class EditGridGainVersionsPage extends AnyPage {

    final String editGridGainVersionsPageXpath = "//div[@id='editVersionGridGain']";

    @ElementTitle(value = "Наименование версии GridGain")
    @FindBy(xpath = editGridGainVersionsPageXpath + "//input[@id='gridGainVersion_name_edit']")
    TextInput gridGainVersion_name_edit;

    @ElementTitle(value = "Код версии GridGain")
    @FindBy(xpath = editGridGainVersionsPageXpath + "//input[@id='gridGainVersion_code_edit']")
    TextInput gridGainVersion_code_edit;

    @ElementTitle(value = "Репозиторий")
    @FindBy(id = "gridGain_repo_edit")
    Select gridGain_repo_edit;

    @ElementTitle(value = "Путь к файлу в репозитории")
    @FindBy(xpath = editGridGainVersionsPageXpath + "//input[@id='gridGainVersion_file_path_edit']")
    TextInput gridGainVersion_file_path_edit;

    @ElementTitle(value = "zip-файл GridGain")
    @FindBy(xpath = editGridGainVersionsPageXpath + "//input[@id='gridGainVersion_zipFile_edit']")
    TextInput gridGainVersion_zipFile_edit;

    @ElementTitle(value = "Контрольная сумма")
    @FindBy(xpath = editGridGainVersionsPageXpath + "//input[@id='check_sum_gg']")
    TextInput check_sum_gg;

    @ElementTitle(value = "Алгоритм")
    @FindBy(id = "check_sum_alg_gg")
    Select check_sum_alg_gg;

    @ElementTitle(value = "Версия лицензии GridGain")
    @FindBy(id = "gridGainVersion_license_edit")
    Select gridGainVersion_license_edit;

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = editGridGainVersionsPageXpath + "//button[descendant::text() = 'Сохранить']")
    Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = editGridGainVersionsPageXpath + "//button[descendant::text() = 'Отменить']")
    Button cancel;

    public EditGridGainVersionsPage() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(editGridGainVersionsPageXpath)));
    }
}
