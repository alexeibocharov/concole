package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.exceptions.PageException;
import ru.yandex.qatools.htmlelements.element.Link;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by sbt-kolesnichenko-af on 17.10.2017.
 */
@PageEntry(title = "Список версий артефакта")
public class ListOfVersionsPage extends AnyPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListOfVersionsPage.class);

    public static final Map<String, String> XPATH_OF_CELLS_OF_LIST_VERSION_TABLE = new LinkedHashMap<String, String>(){{
        put("Наименование версии", ".//tr/td/a");
        put("Наименование модуля/группы", "./td[2]");
        put("Наименование узла", "./td[3]");
        put("Опции VM", "./td[4]");
        put("Валидность", "./td[5]");
        put("Загрузил", "./td[6]");
        put("Дата загрузки", "./td[7]");
        put("Обновил", "./td[8]");
        put("Дата обновления", "./td[9]");
        put("Действие", "./td[10]");
    }
    };

    public static final String XPATH_OF_ALL_MODULE_NAMES = "//table[@class='jtable']/tbody/tr/td[2]";

    // Внимание!!! Данный xpath относительно текущей строки!
    public static final String XPATH_OF_BUTTON_EDIT_VM_OPTIONS = "./td[1]//tr/td[2]/a[@title='Редактировать']";

    // Внимание!!! Данный xpath относительно текущей строки!
    public static final String XPATH_OF_BUTTON_CREATE_SETTINGS = "./td[1]//tr/td[2]/a[@title='Создать индивидуальные настройки']";

    // Внимание!!! Данный xpath относительно текущей строки!
    public static final String XPATH_OF_BUTTON_DELETE_SETTINGS = "./td[1]//tr/td[2]/a[@title='Удалить']";

    @ElementTitle(value = "Список версий")
    @FindBy(xpath = "//div[@id='div_table_artifact_versions']")
    public HypericTable tableArtifactVersions;

    @ElementTitle(value = "Список артефактов")
    @FindBy(xpath = "//a[@class='backToListArtifact']")
    public Link btnBackToListArtifact;

    public ListOfVersionsPage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                        By.xpath("//span[@id='versionCaption']/strong[text()='Список версий']")));
        LOGGER.info("Page is initialized: " + this.getClass().getSimpleName());
    }

    public static WebElement getRequestedRaw(HypericTable table, String version) {
        WebElement raw = null;
        List<List<WebElement>> raws = table.getRows();
        for (int i = 0; i < raws.size(); i++) {
            String versionName = raws.get(i).get(1).getText();
            if (version.equals(versionName)) {
                raw = raws.get(i).get(1).findElement(By.xpath("./../../../../.."));
            }
        }
        return raw;
    }

    public List<WebElement> getListOfRows(String version) {
        List<List<WebElement>> allRows = tableArtifactVersions.getRows();
        List<WebElement> rowsOfVersion = new LinkedList<>();
        for (int i = 0; i < allRows.size(); i++) {
            String versionName = allRows.get(i).get(1).getText();
            if (version.equals(versionName)) {
                rowsOfVersion.add(allRows.get(i).get(1).findElement(By.xpath("ancestor::tr[contains(@class, 'jtable-data-row')]")));
            }
        }
        return tableArtifactVersions.findElements(By.xpath(".//tbody/tr[contains(@class, 'jtable-data-row')]"));
    }

    public WebElement getRequestedRaw(Map<String, String> map) {
        WebElement raw = null;
        List<WebElement> raws = tableArtifactVersions.findElements(By.xpath(".//tr[contains(@class, 'jtable-data-row')]"));
        for (WebElement currRaw : raws) {
            if (isRawCorrect(map, currRaw)) {
                raw = currRaw;
                break;
            }
        }
        return raw;
    }

    public boolean isRawCorrect(Map<String, String> map, WebElement currentRaw) {
        boolean isFound = false;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String xPathOfCheckingElement = XPATH_OF_CELLS_OF_LIST_VERSION_TABLE.get(entry.getKey());
            String expectedValue = entry.getValue();
            String actualValue = currentRaw.findElement(By.xpath(xPathOfCheckingElement)).getText();
            if (!expectedValue.equals(actualValue)) {
                isFound = false;
                break;
            } else {
                isFound = true;
            }
        }
        return isFound;
    }

    public static WebElement searchRawOfTestVersion(String xpathOfAllModuleNames, String testModuleAndNodeName) {
        List<WebElement> allModuleNames = PageFactory.getWebDriver().findElements(By.xpath(xpathOfAllModuleNames));
        WebElement currentRaw = null;
        for (WebElement element : allModuleNames) {
            String moduleName = element.getText();
            if (testModuleAndNodeName.equals(moduleName)) {
                currentRaw = element.findElement(By.xpath("./.."));
                LOGGER.info("The configuration for autotest is found!");
                return currentRaw;
            }
        }
        return currentRaw;
    }

    public static void clickOnEditVMOptions(WebElement currentRaw) {
        WebElement btnEditVMOptions = currentRaw.findElement(By.xpath(ListOfVersionsPage.XPATH_OF_BUTTON_EDIT_VM_OPTIONS));
        btnEditVMOptions.click();
    }

    private static void clickOnCreateVMOptions(WebElement currentraw) {
        WebElement btnAddNewSettings = currentraw.findElement(By.xpath(ListOfVersionsPage.XPATH_OF_BUTTON_CREATE_SETTINGS));
        btnAddNewSettings.click();
    }

    public static void createIndividualSettings(WebElement currentraw, String nameOfModuleAndNode, boolean copy) throws PageException {
        clickOnCreateVMOptions(currentraw);
        Page individualSettingsPage = PageFactory.getInstance().getPage(IndividualSettingsPage.class);
        individualSettingsPage.fillField("Модуль/группа", nameOfModuleAndNode);
        individualSettingsPage.fillField("Узел", nameOfModuleAndNode);
        WebElement chkbCopyValues = individualSettingsPage.getElementByTitle("Копировать значение настроек");
        if (copy) individualSettingsPage.setCheckBoxState(chkbCopyValues, copy);
        individualSettingsPage.clickWebElement(individualSettingsPage.getElementByTitle("Сохранить"));
        PageFactory.getInstance().getPage(ListOfVersionsPage.class);
    }

}
