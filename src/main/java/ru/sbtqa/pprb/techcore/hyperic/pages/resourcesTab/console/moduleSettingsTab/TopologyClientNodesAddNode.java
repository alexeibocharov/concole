package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.AnyPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;

/**
 * Created by sbt-kolesnichenko-af on 23.11.2017.
 */
@PageEntry(title = "Настройка модулей -> Топология клиентских узлов -> Топология клиентских узлов группа -> Добавить узел")
public class TopologyClientNodesAddNode extends AnyPage {

    @ElementTitle(value = "Сохранить")
    @FindBy(xpath = "//div[@id='Top-ModifyClientNodesContainer']//button[@title='Сохранить']")
    public Button save;

    @ElementTitle(value = "Отменить")
    @FindBy(xpath = "//div[@id='Top-ModifyClientNodesContainer']//button[@title='Отменить']")
    public Button cancel;

    @ElementTitle(value = "Добавить узлы")
    @FindBy(xpath = "//a[@title = 'Выбрать узлы']")
    public Button btnChooseNode;

    @ElementTitle(value = "Исключить узлы")
    @FindBy(xpath = "//a[@title = 'Отменить выбор']")
    public Button btnRemoveNode;

    @ElementTitle(value = "Узлы для выбора")
    @FindBy(id = "allAvailableNodesSelect")
    public Select selectNodeForChoose;

    @ElementTitle(value = "Узлы группы модулей")
    @FindBy(id = "currentNodesSelect")
    public Select selectNodesOfGroupModule;

    public TopologyClientNodesAddNode() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Top-ModifyClientNodesContainer")));
    }
}
