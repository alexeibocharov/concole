package ru.sbtqa.pprb.techcore.hyperic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.qatools.properties.Required;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

import java.util.List;

import static ru.sbtqa.tag.pagefactory.PageFactory.getWebDriver;
import static ru.sbtqa.tag.pagefactory.extensions.WebExtension.waitForPageToLoad;

@PageEntry(title = "Страница авторизации")
public class AuthorizationPage extends AnyPage {

    private static final Logger LOG = LoggerFactory.getLogger(AuthorizationPage.class);

    @ElementTitle("Username")
    @FindBy(id = "usernameInput")
    TextInput username;

    @ElementTitle("Password")
    @FindBy(id = "passwordInput")
    TextInput password;

    @Required
    @ElementTitle("Sign in")
    @FindBy(id = "submit")
    Button login;

    public AuthorizationPage() {
        acceptSslWarning();
        wait.until(ExpectedConditions.and(
                ExpectedConditions.elementToBeClickable(login),
                ExpectedConditions.elementToBeClickable(username),
                ExpectedConditions.elementToBeClickable(password))
        );
    }

    /**
     * Авторизация
     *
     * @param propLogin    - логин
     * @param propPassword - пароль
     */
    public void authorization(String propLogin, String propPassword) {
        clearAndSendKeys(username, propLogin);
        clearAndSendKeys(password, propPassword);
        login.click();
    }

    private void clearAndSendKeys(TextInput textInput, String text) {
        textInput.clear();
        textInput.sendKeys(text);
    }

    private void acceptSslWarning() {
        List sslWarnings = getWebDriver().findElements(By.id("overridelink"));
        if (sslWarnings.size() != 0) {
            getWebDriver().navigate().to("javascript:document.getElementById(\'overridelink\').click();");
            waitForPageToLoad(true);
        }
    }

}
