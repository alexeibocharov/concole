package ru.sbtqa.pprb.techcore.hyperic.pages;

import cucumber.api.DataTable;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.sbtqa.pprb.techcore.hyperic.blocks.Dialog;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.ValidationRule;
import ru.sbtqa.tag.pagefactory.exceptions.PageException;
import ru.sbtqa.tag.qautils.errors.AutotestError;
import ru.yandex.qatools.htmlelements.element.*;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static ru.sbtqa.pprb.techcore.hyperic.blocks.Dialog.ACTIVE_DIALOG_XPATH;

public class AnyPage extends Page {

    public WebDriverWait wait = new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds());

    @ElementTitle(value = "Заголовок страницы")
    @FindBy(xpath = "//*[@class = 'PageTitleBar']")
    protected TextBlock pageTitle;

    @ElementTitle(value = "Диалог")
    @FindBy(xpath = ACTIVE_DIALOG_XPATH)
    protected Dialog dialog;

    @ElementTitle(value = "Импорт модели конфигурации")
    @FindBy(xpath = "//div[@id='dialogImportModel']")
    public Dialog importModelDialog;

    @ElementTitle(value = "Выход")
    @FindBy(xpath = "//span/a[text() = 'Sign Out']")
    protected Link btnSignOut;

    @ElementTitle(value = "Обновить")
    @FindBy(xpath = "//div[@class='refreshButton']/img[contains(@title, 'Обновление')]")
    protected Image imgRefresh;

    @ElementTitle(value = "OK")
    @FindBy(xpath = ".//button[text() = 'OK' or text() = 'ОК']")
    public Button ok;

    public AnyPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getWebDriver())), this);
    }

    public void checkPageTitle(String expected) {
        if (pageTitle.getText().isEmpty()) {
            throw new AutotestError("Page has no title!");
        }
        assertEquals(expected, pageTitle.getText());
    }

    @ActionTitle(value = "переходит во вкладку")
    public void goToTab(String tabName) {
        WebElement tab = PageFactory.getWebDriver().findElement(By.xpath("//div[contains(@class, 'tab')]/a[contains(text(), '" + tabName + "')]"));
        tab.click();
    }

    @ActionTitle(value = "переходит по меню")
    public void moveToMenu(String menuPath) {
        List<String> items = Arrays.asList(menuPath.split("->"));
        if (items.size() < 2) {
            throw new AutotestError("Navigation supports \"->\" navigation rule");
        } else {
            try {
                ((JavascriptExecutor) PageFactory.getWebDriver()).executeScript("document.getElementById('resTab').className= 'tab over';");
                for (int i = 1; i < items.size(); i++) {
                    String xpathOfNestedElement = "//li/a[contains(text(), '" + items.get(i) + "')]";
                    WebElement currItem = PageFactory.getWebDriver().findElement(By.xpath(xpathOfNestedElement));
                    wait.until(ExpectedConditions.elementToBeClickable(currItem));
                    ((JavascriptExecutor) PageFactory.getWebDriver()).executeScript("arguments[0].click();", currItem);
                }
            } catch (TimeoutException | NoSuchElementException e) {
                throw new AutotestError("Failed to access menu item: " + menuPath, e);
            }
        }
    }

    @ActionTitle(value = "заполняет поля")
    public void fillFields(DataTable table) throws PageException {
        Map<String, String> values = table.asMap(String.class, String.class);

        for (Map.Entry<String, String> entry : values.entrySet()) {
            if (getElementByTitle(entry.getKey()) instanceof Select) {
                select(entry.getKey(), entry.getValue());
            } else {
                fillField(entry.getKey(), entry.getValue());
            }
        }
    }

    @ActionTitle(value = "переходит по хлебным крошкам на страницу")
    public void goByBreadTo(String title) {
        String xPath = "//div[contains(@style, ': block')]/child::div[@class='bread']//span[text()='" + title
                + "']/parent::a";
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath))).click();
    }

    @ActionTitle(value = "появляется диалог с сообщением")
    public void dialogAppears(String expectedMessage) {
        wait.until(ExpectedConditions.elementToBeClickable(dialog));
        assertTrue(dialog.isContainsMessage(expectedMessage));
    }

    @ActionTitle(value = "появляется диалог Импорт модели с сообщением")
    public void dialogImportModelAppears(String expectedMessage) {
        wait.until(ExpectedConditions.elementToBeClickable(importModelDialog));
        assertTrue(importModelDialog.isContainsMessage(expectedMessage));
    }

    @ValidationRule(title = "присутствуют элементы")
    public void elementsExist(List<String> elements) throws PageException {
        for (String element : elements) {
            assertTrue(PageFactory.getInstance().getCurrentPage().getElementByTitle(element).isDisplayed());
        }
    }

    @ValidationRule(title = "кнопка не активна")
    public void btnIsNotActive(String element) throws PageException {
        String errMessage = "Element " + element + " is active";
        assertTrue(errMessage, PageFactory.getInstance().getCurrentPage().getElementByTitle(element).getAttribute("class").contains("grayed"));
    }

    @ActionTitle(value = "выходит из системы")
    public void signOut() {
        btnSignOut.click();
    }


    @ActionTitle(value = "проверяет выбранное значение")
    public void checkSelectedValue(String elementTitle, String text) throws PageException {
        checkSelectedValue(text, getElementByTitle(elementTitle));
    }

    public void checkSelectedValue(String text, WebElement webElement) {
        String valueSelect = webElement.getAttribute("value");
        Map<String, String > options = new LinkedHashMap<>();
        List<WebElement> listOfOptions = webElement.findElements(By.tagName("option"));
        for (WebElement option : listOfOptions) {
            options.put(option.getAttribute("value"), option.getText());
        }
        String value = options.get(valueSelect);
        String errMessage = "The actual value '" + value + "' of WebElement '" + webElement +
                "' are not equal to expected text '" + text + "'";
        Assert.assertEquals(errMessage, text.replaceAll("\\s+", ""), value.replaceAll("\\s+", ""));
    }
}
