package ru.sbtqa.pprb.techcore.hyperic.blocks;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextBlock;

import static ru.sbtqa.pprb.techcore.hyperic.utils.CommonMethods.uploadFile;

public class Dialog extends HtmlElement {

    public static final String ACTIVE_DIALOG_XPATH = "//*[@role = 'dialog' and not(contains(@style, 'display: none'))]";

    @FindBy(xpath = ".//*[contains(@class, 'DialogPaneContent')]")
    public TextBlock content;

    @ElementTitle(value = "OK")
    @FindBy(xpath = ".//button[text() = 'OK' or text() = 'ОК']")
    public Button ok;

    @ElementTitle(value = "Отмена")
    @FindBy(xpath = ".//button[text() = 'Отмена']")
    public Button cancel;

    @ElementTitle(value = "Выберите файл")
    @FindBy(xpath = "//div[@id='dialogImportModel']//input[@id='selectFile']")
    public WebElement btnChooseFile;

    @ElementTitle(value = "Импорт модели")
    @FindBy(xpath = "//div[@id='dialogImportValue']//input[@id='importValueSelectFile']")
    public WebElement inputImportModel;

    @ElementTitle(value = "Отправить/Подача запроса")
    @FindBy(xpath = "//input[@id='btnUploadModel']")
    public WebElement btnUploadFile;

    @ElementTitle(value = "Импорт модели/Подача запроса")
    @FindBy(xpath = "//div[@id='dialogImportValue']//input[@id='btnUploadValue']")
    public WebElement btnImportModelUploadFile;

    public String getContent() {
        return content.getText();
    }

    public Boolean isContainsMessage(String message) {
        return this.getContent().toLowerCase().contains(message.toLowerCase());
    }

    @ActionTitle(value = "принимает его")
    public void accept() {
        ok.click();
        waitForDialogDisappear();
    }

    @ActionTitle(value = "принимает диалог")
    public void acceptDialog() {
        ok.click();
    }

    @ActionTitle(value = "не принимает его")
    public void cancel() {
        cancel.click();
        waitForDialogDisappear();
    }

    @ActionTitle(value = "загружает модель конфигурации")
    public void loadModelConfiguration() throws InterruptedException {
        // Вводим путь до файла, выбираем его
        String pathToXml = Stash.getValue("uploadConfigurationModelFile");
        uploadFile(btnChooseFile, pathToXml);
        btnUploadFile.click();
    }

    @ActionTitle(value = "загружает модель без выбора файла")
    public void pressDownload() {
        // Нажимаем кнопку Подача запроса без выбора файла
        btnUploadFile.click();
    }

    @ActionTitle(value = "загружает файл")
    public void loadFile(String fileName) throws InterruptedException {
        // Вводим путь до файла, выбираем его
        String pathToXml = KeysHelper.MODEL_CONFIGURATIONS_PATH + fileName;
        uploadFile(btnChooseFile, pathToXml);
        btnUploadFile.click();
    }

    @ActionTitle(value = "импортирует модель")
    public void importModel(String fileName) throws InterruptedException {
        // Вводим путь до файла, выбираем его
        String pathToXml = KeysHelper.MODEL_CONFIGURATIONS_PATH + fileName;
        uploadFile(inputImportModel, pathToXml);
        btnImportModelUploadFile.click();
    }

    @ActionTitle(value = "импортирует модель из временной папки")
    public void importModelFromTempFolder(String fileName) throws InterruptedException {
        // Вводим путь до файла, выбираем его
        String pathToXml = KeysHelper.TEMP_FOLDER_PATH + fileName;
        uploadFile(inputImportModel, pathToXml);
        btnImportModelUploadFile.click();
    }

    public void waitForDialogDisappear() {
        new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.not(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(ACTIVE_DIALOG_XPATH))));
    }
}
