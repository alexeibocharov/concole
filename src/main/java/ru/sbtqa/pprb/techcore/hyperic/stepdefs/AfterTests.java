package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.java.After;
import ru.sbtqa.pprb.techcore.hyperic.utils.CLI.CLIConfiguratorHelper;
import ru.sbtqa.tag.datajack.Stash;

import java.io.IOException;

/**
 * Created by sbt-kolesnichenko-af on 02.01.2018.
 */
public class AfterTests {

    @After("@10608, @2806, @2856")
    public void after10608() throws IOException, InterruptedException {
        CLIConfiguratorHelper cliConfiguratorHelper = new CLIConfiguratorHelper();
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), Stash.getValue("version"), "Autotest", "");
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), Stash.getValue("version"), "", "Autotest");
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), Stash.getValue("version"), "Autotest", "Autotest");
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), Stash.getValue("version"), "", "");
    }

    @After("@2184, @2186, @2187, @2191, @2193, @2194, @2196, @2197, @2199, @2201, @2202, @2892, @2908, @7192, @7557, @11614")
    public void after2184() throws IOException, InterruptedException {
        CLIConfiguratorHelper cliConfiguratorHelper = new CLIConfiguratorHelper();
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), Stash.getValue("version"), "", "");
    }

    @After("@3384")
    public void after3384() throws IOException, InterruptedException {
        CLIConfiguratorHelper cliConfiguratorHelper = new CLIConfiguratorHelper();
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), "1.3384-ValidVersion", "Autotest-version-1", "Autotest-version-1");
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), "1.3384-ValidVersion", "Autotest-version-2", "Autotest-version-2");
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), "1.3384-ValidVersion", "", "");
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), "1.3384-NotValidVersion", "Autotest-version-1", "Autotest-version-1");
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), "1.3384-NotValidVersion", "Autotest-version-2", "Autotest-version-2");
        cliConfiguratorHelper.removeArtifactVersion(Stash.getValue("artifactId"), "1.3384-NotValidVersion", "", "");
    }
}
