package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.NodesTab;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.PPRBContainersControlModule;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

/**
 * Created by sbt-kolesnichenko-af on 12.10.2017.
 */
@PageEntry(title = "Модуль управления контейнером ППРБ. Узлы")
public class NodeTab extends PPRBContainersControlModule {

    @ElementTitle(value = "Установить на узел")
    @FindBy(xpath = "//button[@id='addNode']/span[text()='Установить на узел']")
    Button btnAddOnNode;

    @ElementTitle(value = "Запустить")
    @FindBy(xpath = "//button[@id='startNode']/span[text()='Запустить']")
    Button btnStartNode;

    @ElementTitle(value = "Остановить")
    @FindBy(xpath = "//button[@id='stopNode']/span[text()='Остановить']")
    Button btnStopNode;

    @ElementTitle("Узлы")
    @FindBy(id = "tabNodesTableContainer")
    HypericTable nodes;

    public NodeTab() {
        wait.until(ExpectedConditions.elementToBeClickable(btnAddOnNode));
    }
}
