package ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.roleModelControl;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.pages.administrationTab.AdministrationPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by sbt-kolesnichenko-af on 24.11.2017.
 */
@PageEntry(title = "Управление ролевой моделью -> Добавить пользователя")
public class RoleModelControlAddUser extends AdministrationPage {

    @ElementTitle(value = "Список найденных пользователей")
    @FindBy(id = "ldapUsersSelect")
    public Select selectldapUsersSelect;

    @ElementTitle(value = "Логин")
    @FindBy(id = "search_login")
    public TextInput inputLogin;

    @ElementTitle(value = "Найти")
    @FindBy(xpath = "//div[@id='Top-LdapUserSearchContainer']//a[child::span[text()='Найти']]")
    public Button btnFind;

    @ElementTitle(value = "Закрыть")
    @FindBy(xpath = "//div[@id='Top-LdapUserSearchContainer']//a[child::span[text()='Закрыть']]")
    public Button btnClose;

    public RoleModelControlAddUser() {
        wait.until(ExpectedConditions.attributeToBe(By.id("Top-LdapUserSearchContainer"), "style", "DISPLAY: block"));
    }
}
