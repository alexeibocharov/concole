package ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.console.moduleSettingsTab;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.sbtqa.pprb.techcore.hyperic.elements.HypericTable;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;

@PageEntry(title = "Модуль управления контейнером ППРБ. Настройка модулей. Топология серверных групп")
public class TopologyServiceGroups extends ModuleSettingsTab {

    @ElementTitle(value = "Изменить порт для группы")
    @FindBy(xpath = "//*[@id='changePortForServerGroupButton']/span")
    Button changeGroupPort;


    @ElementTitle(value = "Группы модулей размещаемые на серверных узлах")
    @FindBy(id = "ServerGroupsTopologyTableContainer")
    HypericTable groupOfModulesOnServerCellsTab;

    @ElementTitle(value = "OK")
    @FindBy(xpath =".//button[text() = 'OK' or text() = 'ОК']")
    Button Ok;

    @ElementTitle(value = "Очистить все фильтры")
    @FindBy(xpath ="//*[@id='Top-ServerGroupsTopologyTableContainer']/div[2]/div[3]/div[1]/img")
    Button reset;

    @ElementTitle(value = "Назад в Настройка модулей")
    @FindBy(xpath ="//*[@id='Top-ServerGroupsTopologyTableContainer']/div[5]/a/span")
    Button back;

    public TopologyServiceGroups() {
        wait.until(ExpectedConditions.elementToBeClickable(back));
    }
}
