package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Тогда;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.pprb.techcore.hyperic.pages.AuthorizationPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.dashboardTab.DashboardPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator.ConfigurationPage;
import ru.sbtqa.pprb.techcore.hyperic.pages.resourcesTab.configurator.EditVersionPage;
import ru.sbtqa.pprb.techcore.hyperic.utils.CommonMethods;
import ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.datajack.TestDataObject;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.exceptions.PageException;
import ru.sbtqa.tag.pagefactory.exceptions.PageInitializationException;
import ru.sbtqa.tag.qautils.errors.AutotestError;
import java.util.List;
import java.util.Map;

import static ru.sbtqa.pprb.techcore.hyperic.utils.Download.*;
import static ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper.TESTDATA_OBJECT;

public class HypericStepDefs {

    private static final Logger LOGGER = LoggerFactory.getLogger(HypericStepDefs.class);
    public WebDriverWait wait = new WebDriverWait(PageFactory.getWebDriver(), PageFactory.getTimeOutInSeconds());
    private String TEXT_FROM_FILE = "";

    @Дано("^пользователь авторизован$")
    public void logIn() throws PageInitializationException {
        String url = Stash.getValue("urlHyperic");
        String standName = Stash.getValue("standName");
        String login = Stash.getValue("loginHyperic");
        String password = Stash.getValue("passwordHyperic");
        PageFactory.getWebDriver().get(url);
        LOGGER.info("Stand Name: " + standName);
        LOGGER.info("Url: " + url);
        LOGGER.info("Параметры авторизаци: login= " + login + ", pass= " + password);

        try {
            AuthorizationPage authPage = (AuthorizationPage) PageFactory.getInstance().getPage(AuthorizationPage.class);
            authPage.authorization(login, password);
        } catch (AutotestError e) {
            if (e.getCause() instanceof NoSuchElementException) {
                LOGGER.info("Не найдена форма авторизации, либо пользователь уже авторизован", e);
            } else {
                throw e;
            }
        }

    }

    @Дано("^пользователь авторизован под ролью \"(.*?)\"$")
    public void logInWithRole(String role) throws Throwable {
        TestDataObject tdo = Stash.getValue(TESTDATA_OBJECT);
        String url = tdo.get("application.url").getValue();
        String login = tdo.get("users." + role + ".login").getValue();
        Stash.put("login", login);
        String password = tdo.get("users." + role + ".password").getValue();
        PageFactory.getWebDriver().get(url);
        LOGGER.info("Stand Name: " + tdo.get("application.standName").getValue());
        LOGGER.info("Url: " + url);
        LOGGER.info("Параметры авторизаци: login= " + login + ", pass= " + password);
        try {
            AuthorizationPage authPage = (AuthorizationPage) PageFactory.getInstance().getPage(AuthorizationPage.class);
            authPage.authorization(login, password);
        } catch (AutotestError e) {
            if (e.getCause() instanceof NoSuchElementException) {
                LOGGER.info("Не найдена форма авторизации, либо пользователь уже авторизован", e);
            } else {
                throw e;
            }
        }

    }

    @Дано("^пользователь авторизован с учетной записью \"(.*?)\"$")
    public void logInWithUser(String user) throws Throwable {
        TestDataObject tdo = Stash.getValue(TESTDATA_OBJECT);
        String url = tdo.get("application.url").getValue();
        String login = tdo.get("users." + user + ".login").getValue();
        Stash.put("login", login);
        String password = tdo.get("users." + user + ".password").getValue();
        PageFactory.getWebDriver().get(url);
        LOGGER.info("Stand Name: " + tdo.get("application.standName").getValue());
        LOGGER.info("Url: " + url);
        LOGGER.info("Параметры авторизаци: login= " + login + ", pass= " + password);
        try {
            AuthorizationPage authPage = (AuthorizationPage) PageFactory.getInstance().getPage(AuthorizationPage.class);
            authPage.authorization(login, password);
        } catch (AutotestError e) {
            if (e.getCause() instanceof NoSuchElementException) {
                LOGGER.info("Не найдена форма авторизации, либо пользователь уже авторизован", e);
            } else {
                throw e;
            }
        }
    }

    @Дано("^(?:пользователь |он |)проверяет(?: что|) \"(.*?)\" \"(.*?)\"$")
    public void validationRuleOneParam(String rule, String param) throws Throwable{
        PageFactory.getInstance().getCurrentPage().fireValidationRule(rule, param);
    }

    @Дано("^(?:пользователь |он |)проверяет что на странице \"(.*?)\"$")
    public void validationRuleListParam(String rule, DataTable dataTable) throws Throwable{
        List<String> param = dataTable.asList(String.class);
        PageFactory.getInstance().getCurrentPage().fireValidationRule(rule, param);
    }

    @И("^пользователь выходит из системы$")
    public void signOut() {
        try {
            DashboardPage dashboardPage = (DashboardPage) PageFactory.getInstance().getPage(DashboardPage.class);
            dashboardPage.signOut();
        } catch (PageInitializationException e) {
            LOGGER.info("Страница не загрузилась", e);
        }
    }

    @Тогда("^появляется диалоговое окно браузера: \"(.*?)\"$")
    public void checkBrowserMessage(String alertMessage) throws PageInitializationException {
        wait.until(ExpectedConditions.alertIsPresent());
        Alert alert = PageFactory.getWebDriver().switchTo().alert();
        String actualText = alert.getText();
        String errorMessage = "The alert text is wrong. Expected: " + alertMessage + ", actual: " + actualText;
        Assert.assertTrue(errorMessage, actualText.contains(alertMessage));
        alert.accept();
    }

    @Тогда("^в диалоговом окне нажимает Покинуть страницу$")
    public void clickOnToLeaveInBrowserMessage() throws PageInitializationException {
        wait.until(ExpectedConditions.alertIsPresent());
        PageFactory.getWebDriver().switchTo().alert().accept();
    }

    @Тогда("^в диалоговом окне нажимает Остаться на странице$")
    public void clickOnToStayInBrowserMessage() throws PageInitializationException {
        wait.until(ExpectedConditions.alertIsPresent());
        PageFactory.getWebDriver().switchTo().alert().dismiss();
        wait.until(ExpectedConditions.not(ExpectedConditions.alertIsPresent()));
    }

    @Тогда("^работает с артефактом \"(.*?)\"$")
    public void workWithArtifact(String artifact) {
        Assert.assertNotNull("Artifact is not transferred from the feature", artifact);
        Stash.put("artifactId", artifact);
    }

    @Тогда("^заполняет поле Опции VM данными$")
    public void fillVMOptions(DataTable table) {
        Map<String, String> option = table.asMap(String.class, String.class);
        new EditVersionPage().fillVMOptions(option.get("Опции VM"));
    }

    @Тогда("^обновляет страницу$")
    public void refreshPage() {
        PageFactory.getWebDriver().navigate().refresh();
    }

    @Тогда("^закрывает браузер$")
    public void closeBrowser() {
        new ru.sbtqa.tag.stepdefs.ru.StepDefs().tearDown();
    }

    @Тогда("^открывает браузер$")
    public void openBrowser(Scenario scenario) {
        new ru.sbtqa.tag.stepdefs.ru.StepDefs().setUp(scenario);
    }

    @Тогда("^нажимает \"(.*?)\" на всплывающем окне$")
    public void pressCancelOnPopupWindow(String buttonName) {
        pressButton(buttonName);
    }

    @Тогда("^читает файл$")
    public void readFileUsingAutoIt() {
        TEXT_FROM_FILE = CommonMethods.getTextUsingAutoIt();
    }

    @Тогда("^ожидает сохранения файла \"(.*?)\"$")
    public void waitingSaveFile(String fileName) {
        setFolderSave(KeysHelper.TEMP_FOLDER_PATH);
        String pathToFile = KeysHelper.TEMP_FOLDER_PATH + fileName;
        waitForCreatingFile(pathToFile);
        TEXT_FROM_FILE = CommonMethods.getTextUsingJava(pathToFile);
    }


    @Тогда("в файле отображаются все измененные параметры узла \"(.*?)\"")
    public void checkParametersInDownloadedFile(String nodeName) throws PageInitializationException {
        Map<String, String> mapOfParametersFromPage = ((ConfigurationPage) PageFactory.getInstance().getCurrentPage())
                .getNotEmptyParameters(nodeName);
        Map<String, String> mapOfParametersFromFile = CommonMethods.getGroupParametersFromFile(nodeName, TEXT_FROM_FILE);
        String errMessage = "Maps are different! From page - " + mapOfParametersFromPage + ", from file - " +
                mapOfParametersFromFile;
        Assert.assertTrue(errMessage, mapOfParametersFromPage.equals(mapOfParametersFromFile));
    }

    @Тогда("в файле отображаются все измененные параметры корневого узла")
    public void checkRootParametersInDownloadedFile() throws PageInitializationException {
        Map<String, String> mapOfParametersFromPage = ((ConfigurationPage) PageFactory.getInstance().getCurrentPage())
                .getNotEmptyParameters("Root");
        Map<String, String> mapOfParametersFromFile = CommonMethods.getRootParametersFromFile(TEXT_FROM_FILE);
        String errMessage = "Count parameters from page = " + mapOfParametersFromPage.size() +
                ", count parameters from file = " + mapOfParametersFromFile.size();
        Assert.assertTrue(errMessage, mapOfParametersFromPage.equals(mapOfParametersFromFile));
    }

    @Тогда("^закрывает файл$")
    public void closeFile() {
        CommonMethods.closeFile("[CLASS:Notepad]");
    }

    @Тогда("в тестовом файле \"(.*?)\" изменяет значение параметра \"(.*?)\" в группе \"(.*?)\" на \"(.*?)\"")
    public void changeParameterValueInFile(String fileName, String paramName, String groupName, String newValue) {
        String pathToFile = KeysHelper.MODEL_CONFIGURATIONS_PATH + fileName;
        CommonMethods.setNewValueOfParameter(groupName, pathToFile, paramName, newValue);
    }

    @Тогда("^присутствует надпись \"(.*?)\" между панелью с вкладками и окнами с плагинами$")
    public void thereIsText(String expectedTitle) {
        String actualTitle = PageFactory.getWebDriver().findElement(By.className("PageTitleBar")).getText();
        Assert.assertEquals(expectedTitle, actualTitle);
    }

    @Тогда("^на странице \"(.*?)\" присутствует доступный пользователю элемент \"(.*?)\"$")
    public void elementIsEnabled(String pageTitle, String element) throws PageException {
        Page page = PageFactory.getInstance().getPage(pageTitle);
        try {
            WebElement webElement = page.getElementByTitle(element);
            Assert.assertTrue("Element '" + element + "' is not enable!", webElement.isEnabled());
        } catch (NoSuchElementException nsee) {
            throw new AutotestError("Element <" + element + "> is not enable for this user role. <" + pageTitle + ">!", nsee);
        }
    }
}
