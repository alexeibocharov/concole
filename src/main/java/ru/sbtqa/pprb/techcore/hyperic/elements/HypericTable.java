package ru.sbtqa.pprb.techcore.hyperic.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.sbtqa.tag.qautils.strategies.MatchStrategy;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HypericTable extends Table {

    private final String SELECTING_COL_XPATH = ".//*[contains(@class, 'selecting-column')]//input";
    private final String SIZE_SELECTOR_XPATH = ".//*[contains(@class, 'page-size-change')]//select";

    private final String DATA_IS_NOT_PRESENTED = "Данные отсутствуют";

    public HypericTable(WebElement wrappedElement) {
        super(wrappedElement);
    }

    @Override
    public List<List<WebElement>> getRows() {
        List<List<WebElement>> rows = new ArrayList();

        if (!getWrappedElement().findElements(By.xpath(".//tr[contains(@class, 'no-data-row')]")).isEmpty()) {
            return rows;
        } else {
            return super.getRows();1
        }

    }

    public Select getPageSizeSelector() {
        return new Select(findElement(By.xpath(SIZE_SELECTOR_XPATH)));
    }

    public void selectRow(Integer rowNumber) {
        selectRow((WebElement) getRows().get(rowNumber).get(0).findElement(By.xpath(".//ancestor::tr[contains(@class, 'data-row')]")));
    }

    public void selectRows(Map<String, String> rowData) {
        List<WebElement> found = findEntries(rowData);
        for (WebElement row : found) {
            selectRow(row);
        }
    }

    public void selectRow(Map<String, String> rowData) {
        selectRow(findEntries(rowData).stream().findAny().get());
    }

    public void selectRow(WebElement row) {
        WebElement checkBox = row.findElement(By.xpath(SELECTING_COL_XPATH));
        if (!checkBox.isSelected())  checkBox.click();
    }

    public void clickRow(Map<String, String> rowData) {
        clickRow(findEntries(rowData).stream().findAny().get());
    }

    public void clickRow(WebElement row) {
        WebElement checkBox = row.findElement(By.xpath(SELECTING_COL_XPATH));
        checkBox.click();
    }

    public void clickRows(Map<String, String> rowData) {
        List<WebElement> found = findEntries(rowData);
        for (WebElement row : found) {
            clickRow(row);
        }
    }

    public Boolean isRowSelected(Map<String, String> rowData) {
        return findEntries(rowData).get(0).getAttribute("class").contains("row-selected");
    }

    public List<WebElement> findEntries(Map<String, String> rowData) {
        return findEntries(rowData, MatchStrategy.EXACT);
    }

    public List<WebElement> findEntries(Map<String, String> rowData, MatchStrategy strategy) {
        List<WebElement> result = new ArrayList<>();
        List<Map<String, WebElement>> mappedEntries = findEntriesMappedToHeadings(rowData, strategy);

        for (Map<String, WebElement> rowOfCells : mappedEntries) {
            result.add(rowOfCells.values().stream().findAny().get()
                    .findElement(By.xpath(".//ancestor::tr[contains(@class, 'jtable-data-row')]")));
        }

        return result;
    }

    public List<Map<String, WebElement>> findEntriesMappedToHeadings(Map<String, String> rowData, MatchStrategy strategy) {
        boolean found;
        List<Map<String, WebElement>> foundRows = new ArrayList<>();

        List<Map<String, WebElement>> webRows = getRowsMappedToHeadings();
        for (Map<String, WebElement> webRow : webRows) {
            found = true;
            WebElement cell = null;
            for (Map.Entry<String, WebElement> entry : webRow.entrySet()) {
                cell = entry.getValue();

                if (rowData.containsKey(entry.getKey())) {
                    switch (strategy) {
                        case EXACT:
                            found &= cell.getText().trim().equals(rowData.get(entry.getKey()).trim());
                            break;
                        case CONTAINS:
                            found &= cell.getText().trim().contains(rowData.get(entry.getKey()).trim());
                    }
                }
            }
            if (found) {
                foundRows.add(webRow);
            }
        }

        return foundRows;
    }

}
