package ru.sbtqa.pprb.techcore.hyperic.utils;

//import ru.sbt.qa.datajack.callback.CallbackData;
//import ru.sbt.qa.datajack.callback.GeneratorCallback;

import ru.sbtqa.tag.datajack.callback.CallbackData;
import ru.sbtqa.tag.datajack.callback.GeneratorCallback;

public class DataGensCallback implements GeneratorCallback {

    @Override
    public Object call(CallbackData callbackData) {
        return callbackData.getResult();
    }
}