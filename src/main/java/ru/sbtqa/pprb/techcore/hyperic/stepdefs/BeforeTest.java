package ru.sbtqa.pprb.techcore.hyperic.stepdefs;

import cucumber.api.java.Before;
import cucumber.api.java.ru.Дано;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.datajack.TestDataObject;
import ru.sbtqa.tag.datajack.exceptions.DataException;

import java.io.File;
import java.io.IOException;

import static ru.sbtqa.pprb.techcore.hyperic.utils.KeysHelper.*;
import static ru.sbtqa.pprb.techcore.hyperic.utils.TestData.getTestData;

public class BeforeTest {

    private static final Logger LOG = LogManager.getLogger(BeforeTest.class);

    @Before
    public void setData() throws DataException {
        TestDataObject tdo = getTestData(TESTDATA_PATH + "/" + getEnvName(), "data");
        Stash.put(TESTDATA_OBJECT, tdo);
        String pathToTemp = KeysHelper.TEMP_FOLDER_PATH;
        try {
            FileUtils.forceDelete(new File(pathToTemp));
            LOG.info("Temp folder " + pathToTemp + " is removed");
        } catch (IOException e) {
            LOG.warn("Path <" + pathToTemp + "> is not exists!");
        }
        String url = tdo.get("application.url").getValue();
        Stash.put("urlHyperic", url);
        String login = tdo.get("users.hyperic.login").getValue();
        Stash.put("loginHyperic", login);
        String password = tdo.get("users.hyperic.password").getValue();
        Stash.put("passwordHyperic", password);
        String standName = tdo.get("application.standName").getValue();
        Stash.put("standName", standName);
    }

    @Дано("^(?:пользователь |он |)загружает файл с опциями VM$")
    public void downloadFileVMOptions() throws DataException {
        TestDataObject tdo = getTestData(VMOPTIONS_PATH, "2189_VMOptions");
        Stash.put(VMOPTIONS_OBJECT, tdo);
    }

    private static String getEnvName() {
        String fileName;
        try {
            fileName = System.getenv("stand").toLowerCase();
        } catch (NullPointerException e) {
            LOG.info("Params for Stand \"" + System.getenv("stand") + "\" not found" + String.valueOf(e));
            fileName = "focal";

            LOG.info("Picking up params for \"" + fileName + "\"");
        }

        LOG.info("Picked up params for \"" + fileName + "\"");
        return fileName;
    }
}
