rmdir target\allure-report /s /q
call lib/allure-commandline/bin/allure.bat generate -o target/allure-report -- target/allure-results/
call lib/allure-commandline/bin/allure.bat report open -o target/allure-report
